=======================
DICOM PRINTING HOW TO
=======================

Printing a study from directory
====================================

To print a study in a DICOM printer using `tcpprt` you can use the django wrapper command.

::
    python manage.py printscu dcmfile_in --config /etc/dcmtk/tcpprt.cfg --auto 1 --verbose --debug


Run help command to know more about the available options

::
    python manage.py printscu --help


Print a study with some options
===================================

To print a study in DICOM printer providing some custom options, you can use `print_study` django command for this.

::
    python manage.py print_study _study_guid_ --row 2 --col 3

Run help command to know more about the available options

::
    python manage.py print_study --help


For more information, please check the source code.