from rest_framework import serializers
from .models import Order, DicomFile, OrderMeta, ReportHeader
from alemhealthhub.serializers import HostSerializer


class OrderMetaSerializer(serializers.ModelSerializer):
    key = serializers.CharField(read_only=True)
    value = serializers.CharField(read_only=True)

    class Meta:
        model = OrderMeta
        fields = ('key', 'value',)


class DicomFileSerializer(serializers.ModelSerializer):
    guid = serializers.CharField(read_only=True)
    file = serializers.FileField(read_only=True)

    class Meta:
        model = DicomFile
        fields = ('guid', 'file',)


class ReportHeaderSerializer(serializers.ModelSerializer):
    header_file = serializers.FileField(read_only=True)

    class Meta:
        model = ReportHeader
        fields = '__all__'


class OrderListSerializer(serializers.ModelSerializer):
    # read as string
    guid = serializers.CharField(read_only=True)
    provider_guid = serializers.CharField(read_only=True)
    metas = OrderMetaSerializer(many=True, read_only=True)
    dicoms = DicomFileSerializer(many=True, read_only=True)

    class Meta:
        model = Order
        fields = ('guid', 'id', 'metas', 'priority', 'patient_name', 'patient_gender', 'meta_status', 'created_at',
                  'host', 'patient_id', 'patient_age', 'dicoms', 'modality', 'patient_bodypartexamined',
                  'report_filename', 'iso_file_name', 'normal_report_filename', 'hospital_name', 'provider_guid', 'provider_name')
        depth = 1


class OrderDescriptionSerializer(serializers.ModelSerializer):
    guid = serializers.CharField(read_only=True)
    provider_guid = serializers.CharField(read_only=True)
    dicoms = DicomFileSerializer(many=True, read_only=True)
    metas = OrderMetaSerializer(many=True, read_only=True)

    class Meta:
        model = Order
        fields = ('guid', 'id', 'accession_id', 'priority', 'patient_name', 'patient_age', 'patient_gender',
                  'patient_id', 'patient_bodypartexamined', 'patient_studydescription', 'patient_phone',
                  'patient_date_of_birth', 'metas', 'patient_additionalhistory', 'modality', 'meta_status',
                  'created_at', 'dicoms', 'report_filename', 'normal_report_filename', 'iso_file_name', 'provider_guid', 'provider_name')


class OrderReportSerializer(serializers.ModelSerializer):
    guid = serializers.CharField(read_only=True)

    class Meta:
        model = Order
        fields = ('guid', 'report', 'report_filename',)


class CompletionListSerializer(serializers.Serializer):
    def to_representation(self, obj):
        return {
            "progress": obj.get_meta_status_display(),
            "num_of_occurrence": str(obj.num_of_occurrence)
        }
