import datetime
import logging
import shutil
import string
import random
import uuid

import xhtml2pdf.pisa as pisa
from django.core.files import File
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.template.loader import get_template
from django.utils.text import slugify
from rest_framework.response import Response
from rest_framework.status import HTTP_404_NOT_FOUND
from rest_framework.views import APIView

import alemhealthhub.settings as settings
from alemhealthhub.models import Host, UserProfile, Config
from alemhealthhub.utils import *
from order.tasks import (
    send_dicom_to_localserver,
    send_dicom_to_host,
    send_studies_to_proxy,
    dicom_meta_write,
    Notification)
from printing.tasks.print_study import print_study
from .serializers import *
from order.serializers import CompletionListSerializer
from rest_framework import status
from django.http.response import HttpResponseRedirect

logger = logging.getLogger('alemhealth')

alemhealthhub_tmp_dir = settings.CACHE_DIR
if not os.path.isdir(alemhealthhub_tmp_dir):
    os.mkdir(alemhealthhub_tmp_dir)


class OrderList(APIView):
    def get(self, request):
        data = {}
        page = request.GET.get('page')
        per_page = request.GET.get('per_page')
        orders = Order.objects.exclude(meta_status=0).order_by('-id')
        search_key = request.GET.get('search')

        if not per_page:
            post_per_page = 25
        else:
            post_per_page = int(per_page)

        if not page:
            page = 1
        else:
            page = int(page)

        offset = page * post_per_page - post_per_page
        pagesize = page * post_per_page

        if len(orders.values()) > 0:

            """    searching     """

            if not search_key is None:

                if search_key.strip() != '':
                    orders = orders.filter(Q(patient_name__icontains=search_key) | Q(guid__icontains=search_key) | Q(
                        id__icontains=search_key))

            """     searching      """
            count = orders.count()
            orders = orders[offset:pagesize]
            data["pagination"] = {
                "offset": offset,
                "pagesize": pagesize,
                "count": count
            }
            print str(data["pagination"])

            serializers = OrderListSerializer(orders, many=True)

            data['orders'] = serializers.data

        return Response(data)

    def put(self, request):
        data = {}
        body = json.loads(request.body)
        assign_data = body["assigned"]
        for assign in assign_data:
            guid = str(assign["guid"])
            hostid = int(assign["host_id"])
            try:
                host = Host.objects.get(pk=hostid)
                ae_title = host.ae_title
                port = host.port
                ip_address = host.ip_address
                check_host = os.system('echoscu %s %s -aec %s' % (ip_address, port, ae_title))
                if check_host is 0:
                    try:
                        order = Order.objects.get(guid=guid)
                        order.host = host
                        order.save()
                        try:
                            flag = send_dicom_to_host.delay(order).get()
                            if flag == 3:
                                data["success"] = "host information updated"
                            elif flag == 2:
                                data["error"] = "some error occurred"
                        except:
                            data["error"] = "cannot send data"
                    except Order.DoesNotExist as e:
                        data["error"] = str(e)
                else:
                    data["error"] = "The host is not available"
            except Exception as err:
                logger.log("Can't find any host with provided id : " + str(hostid))
                data["error"] = str("Can't find any host with provided id : " + str(hostid) + " The error is: " + err)

        return Response(data)


class OrderDescription(APIView):
    def get(self, request, guid):
        data = {}
        try:
            order_guid = str(guid)
            try:
                order = Order.objects.get(guid=order_guid)
                order_description_serializers = OrderDescriptionSerializer(order)
                data["success"] = order_description_serializers.data
            except Order.DoesNotExist:
                data["error"] = "Order Doesn't Exists"
        except Exception as e:
            logger.info(str(e))
            data["error"] = "Invalid Request"
        return Response(data)

    def put(self, request, guid):
        body = json.loads(request.body)
        study_description = body["study_description"]
        additional_description = body["additional_description"]
        data = {}

        try:
            orderid = str(guid)
            try:
                order = Order.objects.filter(guid=orderid).update(patient_studydescription=study_description,
                                                                  patient_additionalhistory=additional_description)
                data["success"] = "Order information updated successfully"
            except Exception as e:
                logger.info(str(e))
                data["error"] = "Some error happens"

        except Exception as e:
            logger.info(str(e))
            data["error"] = "Invalid request"

        return Response(data)


class DicomPrint(APIView):
    def post(self, request, guid):
        data = {}
        body = json.loads(request.body)
        session_options = body.get('session_options', {})
        page_options = body.get('page_options', {})
        annotations = body.get('annotations', [])
        order_guid = uuid.UUID(guid)
        logger.info('Printing order {} with session options {} and page options {}'.format(order_guid,
                                                                                           session_options,
                                                                                           page_options))
        try:
            Order.objects.get(guid=str(order_guid))
            print_study(order_guid, session_options, page_options, annotations)
            data["success"] = 'Order {} is in printing'.format(order_guid)
        except ObjectDoesNotExist as e:
            logger.error('Order {} does not exists. Exception: {}'.format(order_guid, e.message))
            data["error"] = 'Order {} does not exist'.format(order_guid)
            return Response(data, status=HTTP_404_NOT_FOUND)

        except Exception as e:
            logger.error('Error while printing order {}. Exception {}'.format(order_guid, e.message))
            data["error"] = 'Error while printing order {}. Exception {}'.format(order_guid, e.message)
            return Response(data)


class ReportView(APIView):
    def get(self, request, guid):
        data = {}

        try:
            order_guid = str(guid)

            try:
                order = Order.objects.get(guid=order_guid)

                serializers = OrderReportSerializer(order)

                data["success"] = serializers.data

            except Order.DoesNotExist:
                data["error"] = "Order Doesn't Exists"

        except Exception as e:
            logger.info(str(e))
            data["error"] = "Invalid Request"

        return Response(data)

    def put(self, request):
        data = {}
        packet = json.loads(request.body)
        report = packet["report"]
        order_guid = packet["order_guid"]
        user_id = packet["user_id"]

        logger.info('PUT request to save order report for {} : {}'.format(order_guid, report))

        try:
            report_data = {}
            order = Order.objects.get(guid=order_guid)
            order.report = report

            # if order.meta_status != 5:
            #     order.meta_status = 2
            # else:
            #     order.meta_status = 4

            order.meta_status = 4  # Report Created
            order.save()

            logger.info('Notify reporting for order: {}  dict: {}'.format(order, Order.model_to_dict(order)))
            Notification.notify_order_reported(Order.model_to_dict(order))

            report_data["order"] = order
            report_data["settings"] = settings
            report_data["header_file"] = self.get_report_header()
            report_data["created_date"] = str(datetime.date.today().strftime("%a, %d %b %Y"))
            report_data["signature"] = self.get_user_signature(user_id)

            template = get_template('report/order.html')
            html = template.render(report_data)

            filename = str(order.pk) + '_' + slugify(order.accession_id) + '__' + slugify(
                str(datetime.date.today()))
            filename = filename[:120] + '.pdf'

            temp_path = '/tmp/alemhealthhub/'

            if not os.path.exists(temp_path):
                os.makedirs(temp_path)

            tmp_filename = '/tmp/alemhealthhub/' + filename
            tmp_file = open(tmp_filename, "w+b")
            pisa_status = pisa.CreatePDF(html, dest=tmp_file)

            if pisa_status:
                order.report_filename.save(filename, File(tmp_file))
                if os.path.isfile(tmp_filename):
                    os.unlink(tmp_filename)
            tmp_file.close()

            report_data["header_file"] = ''  # report without header
            no_header_html = template.render(report_data)

            no_header_filename = str(order.pk) + '_' + slugify(order.accession_id) + '__' + slugify(
                str(datetime.date.today()))
            no_header_filename = no_header_filename[:120] + '.pdf'

            tmp_filename = '/tmp/alemhealthhub/' + no_header_filename
            tmp_file = open(tmp_filename, "w+b")
            pisa_status = pisa.CreatePDF(no_header_html, dest=tmp_file)

            if pisa_status:
                order.normal_report_filename.save(no_header_filename, File(tmp_file))
                if os.path.isfile(tmp_filename):
                    os.unlink(tmp_filename)
            tmp_file.close()

            data["success"] = "Report Successfully Submitted"
        except Order.DoesNotExist:
            data["error"] = "Order " + str(order_guid) + ": not found (From Report API)"
        except Exception as e:
            raise

        return Response(data)

    @classmethod
    def get_report_header(cls):
        header = ReportHeader.objects.get(pk=1)
        file_path = os.path.join(settings.MEDIA_ROOT, unicode(header.header_file))
        if not os.path.exists(file_path):
            raise Exception("Header image not found")
        return file_path

    @classmethod
    def get_user_signature(cls, user_id):
        try:
            user_profile = UserProfile.objects.get(user_id=user_id)
            signature = os.path.join(settings.MEDIA_ROOT, unicode(user_profile.signature_file))
        except UserProfile.DoesNotExist:
            signature = None
        return signature


class SendDicomLocal(APIView):
    def get(self, request):

        logger.info("Sending dicom to local server")

        data = {}

        try:
            order_guid = request.GET.get('guid')

            try:
                flag = send_dicom_to_localserver.delay(order_guid).get()

                if (flag == 1):
                    data['error'] = "CD Burner Server Not Setup Yet"
                elif (flag == 2):
                    data['error'] = "Invalid Cridential/Dicom Server is Off Currently"
                elif (flag == 3):
                    data['success'] = "Successfully Send to CD Burner Server"

                return Response(data)
            except Exception as e:
                data['error'] = "Celery is not running"

        except ValueError:
            return api_error_message('ParameterMissing', 'Invalid Request')


class BoxAppIntegration(APIView):
    def post(self, request):
        data = {}
        data["studies"] = []
        try:
            for folder in os.listdir(settings.DICOM_DIR + "/."):
                data["studies"].append(os.path.join(settings.DICOM_DIR, folder))
            flag = send_studies_to_proxy.delay(data["studies"])
            if flag == 1:
                data["success"] = "Dicoms added"
            else:
                data["error"] = "Cannot send dicoms"
        except Exception as e:
            data["error"] = "The exception is: " + str(e)

        return Response(data)


class HospitalStudy(APIView):
    def put(self, request, guid):
        data = {}
        try:
            json_dict = json.loads(request.body)
            logger.info("box request: " + str(json_dict))
            try:
                order = Order.objects.get(guid=guid)
                order.patient_bodypartexamined = json_dict.get('patient_bodypartexamined')
                order.patient_id = json_dict.get('patient_id')
                order.patient_name = json_dict.get('patient_name')
                order.patient_age = json_dict.get('patient_age')
                order.patient_gender = json_dict.get('patient_gender')
                order.modality = json_dict.get('modality')
                order.priority = json_dict.get('priority')
                order.patient_phone = json_dict.get('patient_phone')
                order.patient_date_of_birth = json_dict.get('patient_date_of_birth')
                order.status = 1
                provider_guid = json_dict.get('provider_guid')

                if provider_guid:
                    order.provider_guid = provider_guid
                    order.provider_name = self.get_provider_name(provider_guid)

                order.save()

                try:
                    if json_dict.get("meta_data"):
                        logger.info("Enter into meta_data condition")
                        for meta in json_dict.get('meta_data'):
                            logger.info("Inside meta for loop")
                            try:
                                logger.info("Getting order_meta object")
                                order_meta = OrderMeta.objects.get(order=order, key=meta)
                                order_meta.value = json_dict.get('meta_data').get(meta)
                                order_meta.save()
                                logger.info("order_meta saved successfully")
                            except OrderMeta.DoesNotExist:
                                logger.info("Meta data " + meta + " does not exist")
                                pass
                        if json_dict.get('write_meta'):
                            logger.info("Enter into write_meta condition")
                            order.is_storage = json_dict.get('is_storage')
                            order.save()
                            logger.info("order is_storage save successfully")
                            dicom_meta_write.delay(guid=order.guid)
                        else:
                            logger.info("Write meta does not exist")
                            data["error"] = "Write meta does not exist"
                    else:
                        logger.error("Meta data does not exist")
                        data["error"] = "Meta data does not exist"
                except Exception as e:
                    logger.error("Cannot save meta data. " + str(e))
                    data["error"] = "Cannot save meta data. " + str(e)
            except Order.DoesNotExist:
                logger.error("Order does not exist")
                data["error"] = "Order does not exist"
        except Exception as e:
            logger.error("Cannot get requested data. " + str(e))
            data["error"] = "Cannot get requested data. " + str(e)
        return Response(data)

    def delete(self, request, guid):
        data = {}
        try:
            order = Order.objects.get(guid=guid)
            order.delete()
            data["success"] = "Order deleted"
        except Exception as e:
            data["error"] = "Cannot delete order. Reason: " + str(e)
        return Response(data)

    def get_provider_name(self, guid):
        provider_name = None
        facility_key = "facility"
        try:
            config_facility_data = json.loads(Config.objects.get(key=facility_key).value)
            if "providers" in config_facility_data:
                providers = config_facility_data['providers']
                for provider in providers:
                    if uuid.UUID(guid).hex == uuid.UUID(provider["guid"]).hex:
                        return provider['name']
        except ObjectDoesNotExist:
            message = "{} key does not exist".format(facility_key)
            logger.error(message)
        except Exception as err:
            message = "exception occurs in get provider list. reason: {}".format(err.message)
            logger.error(message)
        return provider_name


class ISOFileView(APIView):
    def put(self, request):
        data = {}
        try:
            # import pdb; pdb.set_trace()
            order_guid = json.loads(request.body)
            order = Order.objects.get(guid=order_guid["guid"])

            # get dicoms list
            media_root = settings.MEDIA_ROOT
            dicom_dir = os.path.join(media_root, 'dicoms', order_guid["guid"])

            # create temp directory
            temp_folder = "/tmp/iso_file/" + order_guid["guid"]

            # copy from order folder
            shutil.copytree(dicom_dir, temp_folder)

            # rename all the files (only 8 numeric values)
            for dicom_file in os.listdir(temp_folder + "/."):
                os.rename(os.path.join(temp_folder, dicom_file),
                          os.path.join(temp_folder, ''.join(random.choice(string.digits)
                                                            for _ in range(8))))

            # creating DICOMDIR file
            os.system("dcmmkdir --input-directory %s +r --output-file %s/DICOMDIR" % (
                temp_folder, temp_folder
            ))

            # stiing up the file name
            filename = str(order.pk) + '_' + slugify(order.accession_id) + '__' + slugify(
                str(datetime.date.today()))
            filename = filename[:120] + ".iso"

            iso_filename = order_guid["guid"] + ".iso"
            viewer_folder = os.path.join(settings.OPT_DIR, "viewer")
            os.system("mkisofs -o %s/%s %s %s" % (temp_folder, iso_filename, temp_folder, viewer_folder))

            # save the file to media dir and database
            temp_iso_file = temp_folder + "/" + iso_filename
            open_temp_iso_file = open(temp_iso_file)
            order.iso_file_name.save(filename, File(open_temp_iso_file))
            order.save()

            # deleting the temp files
            open_temp_iso_file.close()
            shutil.rmtree(temp_folder)

            # sending the serializer
            serializer = OrderDescriptionSerializer(order)
            data["order"] = serializer.data
            data["success"] = "success"

        except Exception as e:
            data["error"] = "Exception: " + str(e)

        return Response(data)


class ReportHeaderView(APIView):
    def post(self, request):
        data = {}
        header_file = request.FILES.get('file')
        try:
            header = ReportHeader.objects.get(pk=1)
            media_root = settings.MEDIA_ROOT
            file_path = os.path.join(media_root, "header_file")
            if os.path.exists(file_path):
                shutil.rmtree(file_path)
            header.header_file = header_file
            header.save()
            data = self.format(header)
        except ReportHeader.DoesNotExist:
            header = ReportHeader()
            header.header_file = header_file
            header.save()
            serializer = ReportHeaderSerializer(header)
            data["header_file"] = serializer.data
            data["success"] = "Successfully upload file"
        except Exception as err:
            data["error"] = "Cannot upload file"
        return Response(data)

    def dict_to_json(data, status):
        return HttpResponse(json.dumps(data), mimetype="application/json", status=status)

    def format(self, header):
        return {
            'id': header.pk,
            'signature': str(header.header_file)
        }

    def get(self, request):
        data = {}
        try:
            header = ReportHeader.objects.get(pk=1)
            serializer = ReportHeaderSerializer(header)
            data["header_file"] = serializer.data
            data["success"] = "Successfully get file"
        except Exception as e:
            data["error"] = "Error occured: " + str(e)

        return Response(data)


class Completion(APIView):
    def get(self, request):
        data = {}
        query = "SELECT id, meta_status, count(id) as num_of_occurrence FROM orders GROUP BY meta_status"

        try:
            orders = Order.objects.raw(query)
            serializer = CompletionListSerializer(orders, many=True)
            data = serializer.data
        except Exception as e:
            data["error"] = True
            data["message"] = "Could not get completion statistics."
            logger.exception(
                "box dashboard >>> get completion statistics >>> exception occurs in get completion statistics. reason: " + str(
                    e))

        return Response(data, status=status.HTTP_200_OK)


class KnowledgeBase(APIView):
    def get(self, request):
        article_list = [
            {
                "title": "20 oblique projection",
                "content": "20 oblique projection is a troubleshooting projection used in mammography, especially in young women and in follow-up patients."
            },
            {
                "title": "Down syndrome",
                "content": "Down syndrome (or trisomy 21) is the most common trisomy and also the commonest chromosomal disorder. It is a major cause of intellectual disability, and also has numerous multi-system manifestations."
            },
            {
                "title": "18q syndrome",
                "content": "18q syndrome is a rare chromosomal anomaly where there is a deletion of part of the long arm of chromosome 18. Associated symptoms and findings vary widely, as does their severity. Characteristic features include short stature, mental retardation and hypotonia, facial and distal skeletal abnormalities.  Chromosome 18q syndrome appears to result from a spontaneous, sporadic chromosomal error during very early embryonic development."
            }
        ]
        article = random.choice(article_list)
        return Response(article, status=status.HTTP_200_OK)


class DicomImage(APIView):
    def get(self, request, accession_id):
        try:
            order = Order.objects.get(accession_id=accession_id)
            logger.info("order: {0}".format(order))
            viewer_url = "/alemhubviewer/index.php?file=dicoms/{guid}&htmlMode=on".format(
                guid=order.guid.hex
            )
            logger.info("viewer_url: {0}".format(viewer_url))
            return HttpResponseRedirect(redirect_to=viewer_url)
        except ObjectDoesNotExist:
            err_msg = "Order doesn't exist with accession_id {0}".format(accession_id)
            data = {
                "error": {
                    "code": status.HTTP_404_NOT_FOUND,
                    "message": err_msg
                }
            }
            logger.error(err_msg)
            return Response(data, status=status.HTTP_404_NOT_FOUND)
