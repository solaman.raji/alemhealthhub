import datetime
import json
import logging
import os
import uuid

import requests
from celery import task
from django.core.exceptions import ObjectDoesNotExist
from django.core.files import File
from django.template.loader import get_template
from django.utils.text import slugify
from xhtml2pdf import pisa as pisa

from alemhealthhub import settings
from alemhealthhub.models import Config, ReportModel as ReportTemplate, ScheduleTask
from order.models import Order, ReportHeader
from order.tasks.utils import check_service_status

logger = logging.getLogger('alemhealth')


@task(name='report_template_upload_to_platform', acks_late=True)
def report_template_upload_to_platform(template_id):
    logger.info("Uploading report template to platform")

    # Check if provider_guid exists in configs table
    try:
        provider_guid = json.loads(Config.objects.get(key='hospital').value)['guid']
        logger.info("report template upload >>> provider_guid: {0}".format(provider_guid))

        # Check provider_guid validity
        try:
            uuid.UUID(provider_guid)
        except ValueError:
            err_msg = "report template upload >>> provider_guid is not valid"
            logger.error(err_msg)
            raise ValueError(err_msg)
        except Exception as e:
            err_msg = "report template upload >>> unexpected error: {0}".format(e)
            logger.error(err_msg)
            raise Exception(err_msg)
    except ObjectDoesNotExist:
        err_msg = "report template upload >>> provider_guid does not exist"
        logger.error(err_msg)
        raise ObjectDoesNotExist(err_msg)
    except Exception as e:
        err_msg = "report template upload >>> unexpected error: {0}".format(e)
        logger.error(err_msg)
        raise Exception(err_msg)

    # Get report template object
    try:
        report_template = ReportTemplate.objects.get(id=template_id)

        # Report template upload api url and data
        url = settings.AH_SERVER + "/api/reporttemplate/"
        logger.info("report template upload >>> url: {0}".format(url))

        data = {
            "provider_guid": provider_guid,
            "template_name": report_template.report_name,
            "procedure": report_template.procedure,
            "clinical_information": report_template.clinical_information,
            "comparison": report_template.comparison,
            "findings": report_template.findings,
            "impression": report_template.impression
        }
        logger.info("report template upload >>> request data: {0}".format(data))
    except ObjectDoesNotExist:
        logger.exception("report template upload >>> template does not exist")
        raise Exception("report template upload >>> template does not exist")
    except Exception as e:
        logger.exception("report template upload >>> exception occurs. Reason: " + str(e))
        raise Exception("report template upload >>> exception occurs. Reason: " + str(e))

    try:
        response = requests.post(url, data=json.dumps(data))
        logger.info("report template upload >>> response: {0}".format(response))

        response_content = json.loads(response.content)
        logger.info("report template upload >>> response_content: {0}".format(response_content))

        if response_content["status"] == 200:
            logger.info("report template upload >>> template id {0} uploaded to platform".format(template_id))
            logger.info("report template upload >>> platform template id: {0}".format(
                response_content["report"]["id"]
            ))
        else:
            logger.error("report template upload >>> template id {0} cannot be uploaded to platform".format(
                template_id
            ))
            logger.error("report template upload >>> response status_code: {0}".format(response_content["status"]))

            if "error" in response_content:
                logger.error("report template upload >>> response error: {0}".format(response_content["error"]))
    except Exception as e:
        err_msg = "report template upload >>> unexpected error in request to platform. reason: {0}".format(e)
        logger.error(err_msg)
        raise Exception(err_msg)


@task(name='report_download_from_platform', acks_late=True, ignore_result=True)
def report_download_from_platform():
    if not check_service_status("report_download"):
        return

    logger.info("Downloading report from platform")

    try:
        # Get report_download object from schedule_task
        task_type = "report_download"
        schedule_task = ScheduleTask.objects.get(task_type=task_type)
        last_download_time = schedule_task.last_download_time

        try:
            if last_download_time:
                # Convert last_download_time to datetime object with format
                last_download_time = schedule_task.last_download_time.strftime("%Y-%m-%d %H:%M:%S")
            else:
                last_download_time = ""

            logger.info("report download >>> last_download_time: {0}".format(last_download_time))
        except ValueError:
            err_msg = "report download >>> last_download_time is not a valid datetime object"
            logger.error(err_msg)
            raise ValueError(err_msg)
        except Exception as e:
            err_msg = "report download >>> exception occurs. reason: {0}".format(e)
            logger.error(err_msg)
            raise Exception(err_msg)
    except ObjectDoesNotExist:
        err_msg = "report download >>> task type report_download does not exist on schedule_task table"
        logger.error(err_msg)
        raise ObjectDoesNotExist(err_msg)
    except Exception as e:
        err_msg = "report download >>> unexpected error: {0}".format(e)
        logger.error(err_msg)
        raise Exception(err_msg)

    # Report download api url and param
    url = settings.AH_SERVER + '/api/reports'
    logger.info("report download >>> url: {0}".format(url))
    logger.info("report download >>> last_download_time: {0}".format(last_download_time))

    params = {
        "last_download_time": str(last_download_time) if last_download_time else ""
    }

    # Check if facility_guid exists in configs table
    try:
        facility_guid = json.loads(Config.objects.get(key='facility').value)['guid']
        params["facility_guid"] = facility_guid
        logger.info("report download >>> facility_guid: {0}".format(facility_guid))

        # Check facility_guid validity
        try:
            uuid.UUID(facility_guid)
        except ValueError:
            err_msg = "report download >>> facility_guid is not valid"
            logger.error(err_msg)
            raise ValueError(err_msg)
        except Exception as e:
            err_msg = "report download >>> exception occurs. Reason: {0}".format(e)
            logger.error(err_msg)
            raise Exception(err_msg)
    except ObjectDoesNotExist:
        err_msg = "report download >>> facility_guid does not exist"
        logger.error(err_msg)
        raise ObjectDoesNotExist(err_msg)
    except Exception as e:
        err_msg = "report download >>> unexpected error: {0}".format(e)
        logger.error(err_msg)
        raise Exception(err_msg)

    logger.info("report download >>> params: {0}".format(params))

    if not facility_guid:
        err_msg = "report download >>> cannot download report without facility_guid"
        logger.error(err_msg)
        raise Exception(err_msg)

    # Store report download request time
    time_of_request = datetime.datetime.now()
    logger.info("report download >>> time_of_request: {0}".format(time_of_request))

    try:
        response = requests.get(url, params)
        logger.info("report download >>> response: {0}".format(response))

        response_content = json.loads(response.content)
        logger.info("report download >>> response_content: {0}".format(response_content))

        if response.status_code == 200:
            num_of_reports = len(response_content['reports'])
            logger.info("report download >>> total number of modified reports found on platform: {0}".format(
                num_of_reports
            ))

            if num_of_reports:
                # Update last_download_time for report_download task
                schedule_task.last_download_time = time_of_request
                schedule_task.save()
                logger.info("report download >>> last_download_time updated to: {0}".format(
                    schedule_task.last_download_time
                ))

                for packet in response_content['reports']:
                    report_data = {}
                    logger.info("report download >>> packet: {0}".format(packet))

                    order_guid = packet["order_guid"]
                    logger.info("report download >>> order_guid: {0}".format(order_guid))

                    report = packet["report"]
                    logger.info("report download >>> report: " + unicode(report))

                    radiologist = packet.get('radiologist', {})
                    logger.info('Radiologist in downloaded report: {0}'.format(radiologist))

                    try:
                        order = Order.objects.get(ah_order_guid=order_guid)
                        order.report = report
                        order.save()

                        from order.tasks import Notification
                        try:
                            Notification.notify_order_reported(Order.model_to_dict(order), radiologist=radiologist)
                        except Exception as ex:
                            logger.critical('Can not notify to dashboard: {0}'.format(ex.message))
                            logger.exception(ex.message)

                        header = ReportHeader.objects.get(pk=1)
                        file_path = os.path.join(settings.MEDIA_ROOT, unicode(header.header_file))

                        if not os.path.exists(file_path):
                            raise Exception("report download >>> header image not found")

                        report_data["order"] = order
                        report_data["settings"] = settings
                        report_data["header_file"] = file_path
                        report_data["created_date"] = str(datetime.date.today())

                        template = get_template('report/order.html')

                        html = template.render(report_data)

                        filename = str(order.pk) + '_' + slugify(order.accession_id) + '__' + slugify(
                            str(datetime.date.today()))

                        filename = filename[:120] + '.pdf'
                        temp_path = '/tmp/alemhealthhub/'

                        if not os.path.exists(temp_path):
                            os.makedirs(temp_path)

                        tmp_filename = '/tmp/alemhealthhub/' + filename
                        tmp_file = open(tmp_filename, "w+b")
                        pisaStatus = pisa.CreatePDF(html, dest=tmp_file)

                        if pisaStatus:
                            order.report_filename.save(filename, File(tmp_file))

                            if os.path.isfile(tmp_filename):
                                os.unlink(tmp_filename)

                        tmp_file.close()

                        # no header_file
                        no_header_report = {}
                        no_header_report["order"] = order
                        no_header_report["settings"] = settings
                        no_header_report["created_date"] = str(datetime.date.today())

                        no_header_html = template.render(no_header_report)

                        no_header_filename = str(order.pk) + '_' + slugify(order.accession_id) + '__' + slugify(
                            str(datetime.date.today()))

                        no_header_filename = no_header_filename[:120] + '.pdf'
                        tmp_filename = '/tmp/alemhealthhub/' + no_header_filename
                        tmp_file = open(tmp_filename, "w+b")
                        pisaStatus = pisa.CreatePDF(no_header_html, dest=tmp_file)

                        if pisaStatus:
                            order.normal_report_filename.save(no_header_filename, File(tmp_file))

                            if os.path.isfile(tmp_filename):
                                os.unlink(tmp_filename)

                        tmp_file.close()

                        logger.info("report download >>> successfully downloaded all modified reports from platform")
                    except ObjectDoesNotExist:
                        err_msg = "report download >>> order does not exist"
                        logger.error(err_msg)
                        raise ObjectDoesNotExist(err_msg)
                    except Exception as e:
                        err_msg = "report download >>> unexpected error: {0}".format(e)
                        logger.error(err_msg)
                        raise Exception(err_msg)
            else:
                logger.info("report download >>> no modified report found on platform")
        else:
            logger.error("report download >>> response status_code: {0}".format(response.status_code))

            if "error" in response_content:
                logger.error("report download >>> response error: {0}".format(response_content["error"]))
    except Exception as e:
        err_msg = "report download >>> unexpected error: {0}".format(e)
        logger.error(err_msg)
        raise Exception(err_msg)


@task(name='sending_report_to_platform', acks_late=True, ignore_result=True)
def sending_report_to_platform():
    if not check_service_status("report_create"):
        return

    try:
        logger.info("Starting to upload reports to platform")

        provider_guid = json.loads(Config.objects.get(key='hospital').value)['guid']
        logger.info("provider_guid: {0}".format(provider_guid))

        orders = Order.objects.filter(meta_status=4).values()
        num_of_orders = len(orders)

        if num_of_orders > 0:
            logger.info("Number of orders with meta_status 4: {0}".format(num_of_orders))

            for order in orders:
                if not order["ah_order_guid"]:
                    logger.warning('No "ah_order_guid" for order {}. Not uploading report'.format(order['guid']))
                    continue
                logger.info("Uploading report for order_guid {0} and ah_order_guid {1}".format(
                    order["guid"],
                    order["ah_order_guid"]
                ))
                logger.info("order report: {0}".format(order["report"]))

                url = settings.AH_SERVER + '/api/reportfromhub/'
                logger.info("report upload url: {0}".format(url))

                try:
                    logger.info("Requesting to platform for report upload")

                    data = json.dumps({
                        "report": order["report"],
                        "status": "5",
                        "order_guid": order["ah_order_guid"],
                        "provider_guid": provider_guid,
                        "type": "report_update",
                        "op_status": ""
                    })
                    logger.info("report upload request data: {0}".format(data))

                    response = requests.post(url, data)
                    logger.info("report upload response: {0}".format(response))
                    logger.info("report upload response details: {0}".format(response.__dict__))

                    getting = response.json()
                    logger.info("report upload response json: {0}".format(getting))

                    if 'error' in getting:
                        logger.error("Error in report upload response. Error is: {0}".format(getting['error']))
                    elif 'success' in getting:
                        logger.info("Success in report upload response: {0}".format(getting['success']))
                        order = Order.objects.get(guid=str(order['guid']))
                        order.meta_status = 3
                        order.save()
                        logger.info("Order {0} meta_status updated to 3".format(order.guid))
                except Exception as e:
                    logger.error("Cannot upload report. Reason: {0}".format(e))
        else:
            logger.info("There are no report-ready order for uploading to platform")
    except Exception as e:
        logger.error("Report upload unexpected error: {0}".format(e))


@task(name='send_report_to_mirth', acks_late=True, ignore_result=True)
def send_report_to_mirth(report_data):
    log_prefix = "report to mirth >>>"
    logger.info("{0} starting to send report to mirth".format(log_prefix))

    try:
        order = Order.objects.get(guid=report_data["order_guid"])
        logger.info("{0} order: {1}".format(log_prefix, order))
    except ObjectDoesNotExist:
        err_msg = "{0} order does not exist with guid {1}".format(
            log_prefix,
            report_data["order_guid"]
        )
        logger.error(err_msg)
        raise ObjectDoesNotExist(err_msg)
    except Exception as e:
        err_msg = "{0} unexpected error: {1}".format(log_prefix, e)
        logger.error(err_msg)
        raise Exception(err_msg)

    url = settings.MIRTH_SERVER_FOR_REPORT
    logger.info("{0} url: {1}".format(log_prefix, url))

    data = json.dumps({
        "patient_id": order.patient_id,
        "patient_name": order.patient_name,
        "patient_gender": order.patient_gender,
        "patient_age": order.patient_age,
        "patient_date_of_birth": order.patient_date_of_birth,
        "accession_id": order.accession_id,
        "modality": order.modality,
        "priority": order.priority,
        "reported_date": "",
        "report_procedure": report_data["procedure"].replace("\n", "\\X0D\\\\X0A\\"),
        "report_clinical_information": report_data["clinical_information"].replace("\n", "\\X0D\\\\X0A\\"),
        "report_comparison": report_data["comparison"].replace("\n", "\\X0D\\\\X0A\\"),
        "report_findings": report_data["findings"].replace("\n", "\\X0D\\\\X0A\\"),
        "report_impression": report_data["impression"].replace("\n", "\\X0D\\\\X0A\\"),
        "reporting_radiologist": ""
    })
    logger.info("{0} request data: {1}".format(log_prefix, data))

    try:
        response = requests.post(url, data)
        logger.info("{0} report send response: {1}".format(log_prefix, response))
        logger.info("{0} report send response details: {1}".format(log_prefix, response.__dict__))

        if response.status_code == 200:
            logger.info("{0} successfully send report to mirth".format(log_prefix))
        else:
            logger.info("{0} cannot send report to mirth".format(log_prefix))
    except Exception as e:
        logger.error("{0} cannot send report to mirth. Error: {1}".format(log_prefix, e))
