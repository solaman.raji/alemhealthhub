import json
import logging
import os
import uuid

import requests
from celery import task
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone

from alemhealthhub import settings
from order.messaging_client import publish
from order.models import Order

logger = logging.getLogger('alemhealth')


@task(name='create_study_in_box_dashboard', acks_late=True)
def create_study_in_box_dashboard(guid):
    # Check guid validity
    try:
        uuid.UUID(guid)

        # Check if order exists
        try:
            order = Order.objects.get(guid=guid)
            logger.info("box dashboard >>> create study >>> guid: " + str(guid))
            platform_order_data = get_platform_order(order.ah_order_guid)

            if not platform_order_data:
                return
        except ObjectDoesNotExist:
            logger.exception("box dashboard >>> create study >>> order does not exist")
            raise Exception("box dashboard >>> create study >>> order does not exist")
        except Exception as e:
            logger.exception("box dashboard >>> create study >>> exception occurs in order exist. Reason: " + str(e))
            raise Exception("box dashboard >>> create study >>> exception occurs in order exist. Reason: " + str(e))
    except ValueError:
        logger.exception("box dashboard >>> create study >>> guid is not valid")
        raise Exception("box dashboard >>> create study >>> guid is not valid")
    except Exception as e:
        logger.exception("box dashboard >>> create study >>> exception occurs in guid validity. Reason: " + str(e))
        raise Exception("box dashboard >>> create study >>> exception occurs in guid validity. Reason: " + str(e))

    data = {
        "study_id": order.id,
        "guid": str(order.guid),
        "src_facility_guid": platform_order_data["src_facility_guid"],
        "src_facility_name": order.hospital_name,
        "modality": order.modality,
        "priority": order.priority,
        "time_sent": platform_order_data["time_sent"],
        "time_received": str(order.created_on),
        "time_reported": None
    }

    url = os.path.join(settings.BOX_DASHBOARD_SERVER, "dashboard-api", "study")
    headers = {'content-type': 'application/json'}

    logger.info("box dashboard >>> create study >>> url: " + str(url))
    logger.info("box dashboard >>> create study >>> headers: " + str(headers))

    try:
        response = requests.post(url, data=json.dumps(data), headers=headers)
        logger.info("box dashboard >>> create study >>> response: " + str(response))

        if response.status_code == 201:
            logger.info("box dashboard >>> create study >>> study successfully created in box dashboard")
        else:
            logger.error("box dashboard >>> create study >>> study could not be created in box dashboard")
            logger.info("box dashboard >>> create study >>> response dict: " + str(response.__dict__))
    except Exception as e:
        logger.exception("box dashboard >>> create study >>> exception occurs in api request. Reason: " + str(e))
        raise Exception("box dashboard >>> create study >>> exception occurs in api request. Reason: " + str(e))


@task(name='update_study_in_box_dashboard', acks_late=True)
def update_study_in_box_dashboard(guid, time_reported=timezone.now(), status="inactive"):
    # Check guid validity
    try:
        uuid.UUID(guid)

        # Check if order exists
        try:
            Order.objects.get(guid=guid)
            logger.info("box dashboard >>> update study >>> guid: " + str(guid))
        except ObjectDoesNotExist:
            logger.exception("box dashboard >>> update study >>> order does not exist")
            raise Exception("box dashboard >>> update study >>> order does not exist")
        except Exception as e:
            logger.exception("box dashboard >>> update study >>> exception occurs in order exist. Reason: " + str(e))
            raise Exception("box dashboard >>> update study >>> exception occurs in order exist. Reason: " + str(e))
    except ValueError:
        logger.exception("box dashboard >>> update study >>> guid is not valid")
        raise Exception("box dashboard >>> update study >>> guid is not valid")
    except Exception as e:
        logger.exception("box dashboard >>> update study >>> exception occurs in guid validity. Reason: " + str(e))
        raise Exception("box dashboard >>> update study >>> exception occurs in guid validity. Reason: " + str(e))

    data = {
        "status": status,
        "time_reported": str(time_reported)
    }

    url = os.path.join(settings.BOX_DASHBOARD_SERVER, "dashboard-api", "study", guid)
    headers = {'content-type': 'application/json'}

    logger.info("box dashboard >>> update study >>> url: " + str(url))
    logger.info("box dashboard >>> update study >>> headers: " + str(headers))

    try:
        response = requests.put(url, data=json.dumps(data), headers=headers)
        logger.info("box dashboard >>> update study >>> response: " + str(response))

        if response.status_code == 201:
            logger.info("box dashboard >>> update study >>> study successfully updated in box dashboard")
        else:
            logger.error("box dashboard >>> update study >>> study could not be updated in box dashboard")
            logger.info("box dashboard >>> update study >>> response dict: " + str(response.__dict__))
    except Exception as e:
        logger.exception("box dashboard >>> update study >>> exception occurs in api request. Reason: " + str(e))
        raise Exception("box dashboard >>> update study >>> exception occurs in api request. Reason: " + str(e))


def get_platform_order(guid):
    url = os.path.join(settings.AH_SERVER, 'api', 'orders', guid)
    params = {"secret_key": settings.CLIENT_SECRET_KEY}

    logger.info("box dashboard >>> get platform order >>> url: " + str(url))
    logger.info("box dashboard >>> get platform order >>> params: " + str(params))

    try:
        response = requests.get(url, params)
        logger.info("box dashboard >>> get platform order >>> response: " + str(response))

        if response.status_code == 200:
            res_data = json.loads(response.text)
            data = {
                "src_facility_guid": res_data["hospital_guid"],
                "time_sent": res_data["created_at"],
            }
            return data
        else:
            logger.exception("box dashboard >>> get platform order >>> could not get order from platform")
            return False
    except Exception as e:
        logger.exception("box dashboard >>> get platform order >>> exception occurs in api request. Reason: " + str(e))
        raise Exception("box dashboard >>> get platform order >>> exception occurs in api request. Reason: " + str(e))


class Notification(object):
    ORDER_SAVED_TEMPLATE = u'Study {id} [ {patient_name} ] is being transferred from {hospital_name}'
    ORDER_DOWNLOADED_TEMPLATE = u'Study {id} [ {patient_name} ]  has successfully arrived from {hospital_name}'
    ORDER_REPORTED_TEMPLATE = u'Study {id} reported'

    @classmethod
    def notify_order_created(cls, order):
        if not settings.NOTIFY_DASHBOARD:
            logger.warning('NOTIFY_DASHBOARD in settings is disabled')
            return

        msg_template = cls.ORDER_SAVED_TEMPLATE
        context = msg_template.format(**order)
        message = {
            'message_guid': str(uuid.uuid4()),
            'context': context,
            'occurred_at': timezone.now().isoformat(),
            'action': 'order-created',
            'study_guid': str(order['guid']),
        }

        logger.info('Notify order created:: {}'.format(order))
        logger.info('Publish message: {}'.format(context))
        publish(message)

        logger.info('POST order guid: {} creation to celery task'.format(order['guid'].hex))
        create_study_in_box_dashboard.delay(order['guid'].hex)

        return True

    @classmethod
    def notify_dicoms_downloaded(cls, order):
        if not settings.NOTIFY_DASHBOARD:
            logger.warning('NOTIFY_DASHBOARD in settings is disabled')
            return

        msg_template = cls.ORDER_DOWNLOADED_TEMPLATE
        context = msg_template.format(**order)
        message = {
            'message_guid': str(uuid.uuid4()),
            'context': context,
            'occurred_at': timezone.now().isoformat(),
            'action': 'dicoms-downloaded',
            'study_guid': str(order['guid']),
        }
        logger.info('Notify dicoms downloaded: {}'.format(order))
        logger.info('Publish message: {}'.format(context))
        publish(message)
        return True

    @classmethod
    def notify_order_reported(cls, order, radiologist=None):
        if not settings.NOTIFY_DASHBOARD:
            logger.warning('NOTIFY_DASHBOARD in settings is disabled')
            return

        msg_template = cls.ORDER_REPORTED_TEMPLATE
        context = msg_template.format(**order)

        # This hack is needed as only study reported at platform will have radiologist.
        if radiologist and 'name' in radiologist:
            context += ' by {}'.format(radiologist.get('name'))

        message = {
            'message_guid': str(uuid.uuid4()),
            'context': context,
            'occurred_at': timezone.now().isoformat(),
            'action': 'reported',
            'study_guid': str(order['guid']),
        }

        logger.info('Notify reporting for order dict: {}'.format(order))
        logger.info('Publish message: {}'.format(context))
        publish(message)

        logger.info('POST order guid: {} reporting to celery task'.format(order['guid'].hex))
        update_study_in_box_dashboard.delay(order['guid'].hex, timezone.now(), "inactive")

        return True
