import datetime
import json
import logging
import os, shutil
import subprocess
import urllib2

import dicom
import requests
from celery import task
from django.conf import settings
from django.core.files import File
from django.core.exceptions import MultipleObjectsReturned
from poster.encode import multipart_encode

import alemhealthhub.settings as settings
from alemhealthhub import settings as temp_settings
from alemhealthhub.models import Config
from alemhealthhub.utils import download_and_save_from_url as d_a_s_f_u
from concurrent_transfer import transfer
from order.models import Order, DicomFile, dicom_file_upload_dir, OrderMeta
from order.tasks.boxdashboard import Notification
from order.tasks.utils import check_service_status, get_default_provider
from mwl_broker.views import ModalityWorkListDetail
from mwl_broker.models import ModalityWorkList as ModalityWorkListModel

logger = logging.getLogger('alemhealth')

choice = {'True': True, 'False': False}


alemhealthhub_tmp_dir = os.path.join(temp_settings.CACHE_DIR, 'tmp')
if not os.path.exists(alemhealthhub_tmp_dir):
    os.makedirs(alemhealthhub_tmp_dir)


# after getting the dicom sending message to the ah-platform
def sent_msg_for_dicom(order_guid, dicom_guid):
    url = settings.AH_SERVER + '/api/dicomforhub'

    params = {
        'order_guid': order_guid,
        'dicom_guid': dicom_guid
    }

    try:
        response = requests.post(url, params)

        data = response.json()

        if (not data['success'] is None):
            logger.info(data['success'])
        else:
            logger.info(data['error'])
    except Exception as e:
        logger.info(str(e))


# after getting study sending message to the ah-platform for cleanup
def sent_msg_for_study(order_guid, flag):
    url = settings.AH_SERVER + '/api/studyforhub'

    params = {
        'order_guid': order_guid,
        'hubsync_flag': flag
    }

    try:
        logger.info('Sent hubsync_flag {} for study {}'.format(flag, order_guid))
        response = requests.post(url, params)

        data = response.json()

        if (not data['success'] is None):
            logger.info(data['success'])
        else:
            logger.info(data['error'])
    except Exception as e:
        logger.info(str(e))


# function for storing dicom from the ah platform
def request_for_dicom(order, dicom_list, compress):
    order_guid = str(order.guid)

    logger.info("Fetching dicom files for Order guid:" + str(order_guid))

    if len(dicom_list) > 0:
        for dicom in dicom_list:
            dicom_guid = dicom["dicom_guid"]
            dicom_name = dicom["dicom_filename"]

            destination_filename = dicom_name.split('/')[len(dicom_name.split('/')) - 1]

            if not compress:
                logger.info("Downloading Without Compression")

                dicom_tmp_path = os.path.join(alemhealthhub_tmp_dir, destination_filename)
            else:
                logger.info("Downloading With Compression")

                dicom_tmp_path = os.path.join(alemhealthhub_tmp_dir, os.path.splitext(destination_filename)[0] + '.zpaq')
                zpaq_file = os.path.splitext(destination_filename)[0] + '.zpaq'

            if not compress:
                download_url = settings.AH_SERVER + '/api/dicomforhub?order_guid=' + order.ah_order_guid + '&dicom_guid=' + dicom_guid
            else:
                download_url = settings.AH_SERVER + '/api/dicomforhub?order_guid=' + order.ah_order_guid + '&dicom_guid=' + dicom_guid + '&compress=1'

            d_a_s_f_u(download_url, dicom_tmp_path)

            if (compress):
                logger.info("Uncompressing zpaq")

                os.system("cd %s;zpaq x %s; rm %s;" % (alemhealthhub_tmp_dir, zpaq_file, zpaq_file))
                # dicom_tmp_path = os.path.join(alemhealthhub_tmp_dir, os.path.splitext(destination_filename)[0] + '.dcm')
                destination_filename, file_extension = os.path.splitext(destination_filename)

                if file_extension:
                    destination_filename += file_extension

                dicom_tmp_path = os.path.join(alemhealthhub_tmp_dir, destination_filename)

            # send individual dicom for indexing

            os.system('storescu localhost 11112 ' + dicom_tmp_path + ' -aec AH_STORE -aet AH1')

            f = open(str(dicom_tmp_path))
            dcm_file = DicomFile()
            dcm_file.order = order
            dcm_file.file.save(destination_filename, File(f))
            dcm_file.save()
            f.close()

            logger.info("Dicom successfully saved")

            os.remove(dicom_tmp_path)

            sent_msg_for_dicom(order.ah_order_guid, dicom_guid)

    return order


#  celery beat task for storing dicom from the ah platform
@task(name='request_for_study', acks_late=True, ignore_result=True)
def request_for_study():
    if not check_service_status("study_download"):
        return

    logger.info("getting provider guid")

    try:
        provider_guid = json.loads(Config.objects.get(key='hospital').value)['guid']

        params = {
            'provider_guid': provider_guid
        }

        url = settings.AH_SERVER + '/api/studyforhub'

        logger.info("GET {} to get studies from AH-Platform".format(url))

        try:
            response = requests.get(url, params)

            data = response.json()

            if 'user_error' in data:
                logger.error(str(data['user_error']))
            elif 'error' in data:
                logger.error(str(data['error']))
            else:
                studies = data['orders']

                # downloading with or without compression
                compress = choice[Config.objects.get(key="compress").value]

                if len(studies) > 0:

                    logger.info("Get {} studies from AH-PLATFORM".format(len(studies)))

                    for order_guid, order_dict in studies.iteritems():
                        order_details = order_dict['order_detail']

                        ah_order_guid = order_details['guid']
                        logger.info('Process received ah_order_guid: {}'.format(ah_order_guid))
                        try:
                            _order = Order.objects.get(ah_order_guid=ah_order_guid)
                            logger.warning("The order {} meta status {} with ah_order_guid {} already exists".format(
                                _order.guid, _order.meta_status, ah_order_guid))

                            if _order.meta_status == 0:
                                logger.info('The order {} meta status is 0. Download the dicoms again'.format(_order.guid))
                                _download_dicoms(_order, compress, order_dict['dicoms'])

                            # Update hubsync_status to synchronization completed
                            sent_msg_for_study(ah_order_guid, 2)

                        except MultipleObjectsReturned as ex:
                            logger.critical('Multiple order found for ah_order_guid: {}'.format(ah_order_guid))
                            logger.exception(ex)

                        except Order.DoesNotExist:
                            try:
                                logger.info("Storing order guid:" + str(ah_order_guid))

                                order = Order()

                                order.ah_order_id = order_details['order_id']
                                order.ah_order_guid = ah_order_guid
                                order.accession_id = order_details['accession_id']
                                order.modality = order_details['modality']
                                order.created_at = order_details['created_at']
                                order.priority = order_details['priority']

                                order.patient_name = order_details['patient_name']
                                order.patient_gender = order_details['patient_gender']
                                order.patient_id = order_details['patient_id']
                                order.patient_bodypartexamined = order_details['metas']['BodyPartExamined']
                                order.patient_studydescription = order_details['metas']['StudyDescription']
                                order.patient_additionalhistory = order_details['metas']['AdditionalPatientHistory']

                                order.hospital_name = order_details['hospital_name']
                                order.hospital_id = order_details['hospital_id']

                                order.doctor_phone = order_details['doctor_phone']

                                order.save()

                                logger.info("Successfully Stored order guid:" + str(ah_order_guid))

                                Notification.notify_order_created(Order.model_to_dict(order))

                                _download_dicoms(order, compress, order_dict['dicoms'])
                            except Exception as e:
                                logger.error(str(e.message))
                                logger.exception(e)

        except Exception as e:
            logger.error(str(e))
            logger.exception(e.message)
    except Config.DoesNotExist as e:
        logger.error(str(e))
        logger.exception(e.message)


def _download_dicoms(order, compress, dicom_list):
    logger.info("Started dicom syncronization")
    logger.info('Download {} dicoms for order: {}'.format(len(dicom_list), order.guid))

    # changing flag
    sent_msg_for_study(order.ah_order_guid, 1)

    # TODO: Revert to celery task to download dicoms
    # order = request_for_dicom.delay(order=order,
    #                                 dicom_list=dicom_list,
    #                                 compress=compress).get()
    order = request_for_dicom(order=order, dicom_list=dicom_list, compress=compress)
    logger.info('Dicom downloading for order {} is done'.format(order.guid))
    order.meta_status = 1
    order.save()
    Notification.notify_dicoms_downloaded(Order.model_to_dict(order))
    logger.info('Set hubsync flag=2 at cloud after successfully downloading')
    sent_msg_for_study(order.ah_order_guid, 2)
    logger.info("dicom syncronization ended")


# celerybeat for sending report to platform


@task(name="send_dicom_to_localserver", acks_late=True)
def send_dicom_to_localserver(guid):
    flag = {'True': True, 'False': False}
    remote_enabled = str(Config.objects.get(key='want_local_server').value)

    if flag[remote_enabled]:
        remote_host = str(Config.objects.get(key='local_host').value)
        remote_port = str(Config.objects.get(key='local_port').value)
        remote_aet = str(Config.objects.get(key='local_aet').value)

        order = Order.objects.get(guid=guid)

        logger.info('Order : ' + str(order.pk) +
                    ' start sending dicom to local server ')

        ah_temp_order_dir = os.path.join(alemhealthhub_tmp_dir, 'dicoms', str(order.guid))
        if not os.path.exists(ah_temp_order_dir):
            logger.info("Creating alemhealthhub temp order directory")
            os.makedirs(ah_temp_order_dir)

        dicom_files = DicomFile.objects.filter(order=order)

        for dicom_file in dicom_files:

            # filename = os.path.basename(dicom_file.file.name)
            # file_image = dicom.read_file(dicom_file.file)

            # ah_temp_order_dir = '/tmp/alemhealth/dicoms/' + str(order.guid)
            dicom_file_path = dicom_file.file.path

            # try:
            # check dicom modality is "HSG"

            transfer_syntax_display = dicom_file.get_transfer_syntax_display()
            if not transfer_syntax_display:
                transfer_syntax_display = ''

            if order.modality == "HSG":
                logger.info(
                    'Decompresses Dicom file  for order : ' + str(order.pk))
                result = os.system(
                    'dcmdjpeg %s %s' %
                    (dicom_file_path, dicom_file_path))

            # logger.info(
            #     'dcmtk command : ' +
            #     'storescu %s %s -aet alemhealth -aec %s %s %s %s  -xv' % (
            #         remote_host, remote_port, remote_aet, dicom_file_path,
            #         ' '.join(settings.STORESCU_PARAMS),
            #         transfer_syntax_display
            #     )
            # )
            try:
                # send the dicom to provider by DCMTK storescu
                subprocess.check_call(
                    'storescu %s %s -aet alemhealthbox -aec %s %s %s %s  -xv' % (
                        remote_host, remote_port, remote_aet, dicom_file_path,
                        ' '.join(settings.STORESCU_PARAMS),
                        transfer_syntax_display
                    ), shell=True
                )

                logger.info("Successfully Send to the Local Server")

            except Exception as e:
                print "Error occured"
                return 2
        return 3

    else:
        return 1


@task(name="send_dicom_to_host", acks_late=True)
def send_dicom_to_host(order):
    logger.info('Order : ' + str(order.pk) + ' start sending dicom to hosts')

    ah_temp_order_dir = os.path.join(alemhealthhub_tmp_dir, 'dicoms', str(order.guid))
    if not os.path.exists(ah_temp_order_dir):
        logger.info("Creating alemhealthhub temp order directory")
        os.makedirs(ah_temp_order_dir)

    dicom_files = DicomFile.objects.filter(order=order)
    counter = 1
    for dicom_file in dicom_files:
        dicom_file_path = dicom_file.file.path
        try:
            subprocess.check_call(
                'storescu %s %s -aet alemhealthbox -aec %s %s  -xv' % (
                    order.host.ip_address, order.host.port, order.host.ae_title, dicom_file_path
                ), shell=True
            )
            logger.info("done for: " + str(counter))
        except Exception as e:
            return 2
        counter += 1
    return 3


# https://stackoverflow.com/questions/2827623/how-can-i-create-an-object-and-add-attributes-to-it#2827664
class Object(object):
    pass


def _get_destination_filepath(order, src_dicom_file):
    dicom_file_name = src_dicom_file
    logger.info("dicom_file_name: {0}".format(dicom_file_name))

    check_dicom_file_name = src_dicom_file.split(" ")
    logger.info("check_dicom_file_name: {0}".format(check_dicom_file_name))

    if len(check_dicom_file_name) >= 1:
        dicom_file_name = src_dicom_file.replace(" ", "_")
        logger.info("dicom_file_name(replaced space with underscore): {0}".format(dicom_file_name))

    dicom_file_desination = os.path.join('dicoms', str(order.guid), dicom_file_name)
    logger.info("dicom_file_desination: {0}".format(dicom_file_desination))

    try:
        _instance = Object()
        setattr(_instance, 'order', order)
        dicom_file_desination_1 = dicom_file_upload_dir(_instance, src_dicom_file)
        logger.info('Generate dicom_file_desination by dicom_file_upload_dir(): {}, matched: {}'.format(
            dicom_file_desination_1, dicom_file_desination == dicom_file_desination_1))
    except:
        logger.warning('Generate dicom_file_desination by dicom_file_upload_dir is for debugging purpose. Ignoring')

    return dicom_file_desination


@task(name="add_dicom_from_listener", acks_late=True)
def add_dicom_from_listener(folder_path, auto_sync=True):
    logger.info("Starting to add dicom from listener")
    logger.info("folder_path: {0}".format(folder_path))
    logger.info("auto_sync: {0}".format(auto_sync))

    from time import gmtime, strftime

    accession_id = get_accession_id_from_dicom_file(folder_path)
    logger.info("accession_id: {0}".format(accession_id))

    if settings.MODALITY_WORK_LIST and accession_id:
        update_modality_work_list_status(accession_id, ModalityWorkListModel.SENT_TO_PACKS)

    dicom_uid = os.path.basename(folder_path)[3:]
    logger.info("dicom_uid: {0}".format(dicom_uid))

    try:
        order = Order.objects.get(dicom_uid=dicom_uid)
        logger.error("Order already exists with id: {0}".format(order.pk))
    except Order.DoesNotExist:
        order = Order()
        order.dicom_uid = dicom_uid
        order.priority = "Routine"
        order.save()
        logger.info("New order created with id: {0}".format(order.pk))

    logger.info("Dicoms are ready to sync from listener folder")

    dicom_files_in_order = order.dicoms.all().values_list("file", flat=True)
    for src_dicom_file in os.listdir(folder_path + "/."):
        logger.info("Source dicom_file: {0}".format(src_dicom_file))

        if src_dicom_file.endswith(".dcm"):
            try:
                desination_dicom_file = _get_destination_filepath(order, src_dicom_file)
                destination_dicom_absolute_path = os.path.join(settings.MEDIA_ROOT, desination_dicom_file)
                logger.info('Dicom destination found in DB: {}, exists: {}, absolute path: {}'.format(
                    desination_dicom_file in dicom_files_in_order,
                    os.path.exists(destination_dicom_absolute_path),
                    destination_dicom_absolute_path))

                if desination_dicom_file in dicom_files_in_order and os.path.exists(destination_dicom_absolute_path):
                    logger.info("Order : " + str(order.pk) + " Dicom : " + str(src_dicom_file) + " already exist ")
                    continue
                else:
                    f = open(os.path.join(folder_path, src_dicom_file))
                    dcm_file = DicomFile()
                    dcm_file.order = order
                    dcm_file.edit_meta = 1
                    dcm_file.file.save(src_dicom_file, File(f))
                    dcm_file.save()
                    f.close()

                    # update after first dicom meta write
                    if order.meta_status == 0:
                        order.meta_status = 5
                        order.save()
                        logger.info("Order {0} first dicom {1} save successfully".format(
                            order.pk,
                            dcm_file.guid
                        ))

                        dicom_data = dicom.read_file(folder_path + '/' + src_dicom_file)
                        order.accession_id = dicom_data.AccessionNumber
                        order.modality = dicom_data.Modality
                        order.created_at = strftime("%Y-%m-%d %H:%M:%S", gmtime())
                        order.patient_name = dicom_data.PatientName
                        order.patient_gender = dicom_data.PatientSex
                        order.patient_id = dicom_data.PatientID

                        if "BodyPartExamined" in dicom_data:
                            logger.info("BodyPartExamined attribute is in dicom_data")
                            logger.info("BodyPartExamined attribute value in dicom_data: {0}".format(
                                dicom_data.BodyPartExamined
                            ))

                            if dicom_data.BodyPartExamined != 'None':
                                logger.info("BodyPartExamined attribute is not None")
                                order.patient_bodypartexamined = dicom_data.BodyPartExamined

                        if "PatientAge" in dicom_data:
                            logger.info("PatientAge attribute is in dicom_data")
                            logger.info("PatientAge attribute value in dicom_data: {0}".format(dicom_data.PatientAge))

                            if dicom_data.PatientAge != 'None':
                                patient_age = dicom_data.PatientAge.lstrip("0").rstrip("Y")
                                logger.info("PatientAge attribute is not None")
                                logger.info("Modified PatientAge attribute: {0}".format(patient_age))
                                order.patient_age = patient_age

                        order.save()
                        logger.info("Patient name added: {0}".format(order.patient_name))
                        logger.info("Patient name added: {0}".format(dicom_data.StudyID))
                    else:
                        logger.info("Order {0} dicom {1} save successfully".format(
                            order.pk,
                            dcm_file.guid
                        ))
            except Exception as err:
                logger.error("Dicom save failed in order: {0}. Error: {1}".format(order.pk, err))

    if settings.MODALITY_WORK_LIST and accession_id:
        update_modality_work_list_status(accession_id, ModalityWorkListModel.CAPTURED)
        delete_modality_work_list_file(accession_id)

    shutil.rmtree(folder_path)

    if auto_sync:
        logger.info("sending order: " + str(order.guid))
        send_dicom_to_alemhealth(guid=order.guid)
        logger.info("sent order: " + str(order.guid))

    return



"""
testing box viewer
"""
@task(name="send_studies_to_proxy", acks_late=True)
def send_studies_to_proxy(dicom_folders):
    logger.info("start sending dicoms")
    for folder in dicom_folders:
        try:
            logger.info("Running the command: storescu %s %s -aet alemhealth -aec %s %s +sd" % (
    "54.169.115.4", "5106", "BOX", os.path.join(temp_settings.DICOM_DIR, folder)))
            subprocess.check_call("storescu %s %s -aet alemhealth -aec %s %s +sd" % (
    "54.169.115.4", "5106", "BOX", os.path.join(temp_settings.DICOM_DIR, folder)), shell=True)
            logger.info("The folder path is: " + os.path.join(settings.DICOM_DIR, folder))
        except Exception as e:
            logger.error("In side the Exception. " + str(e))
    return 1


@task(name='dicom_meta_write', acks_late=True)
def dicom_meta_write(guid):
    log_prefix = "dicom meta write >>>"
    logger.info("{0} starting dicom meta write".format(log_prefix))

    from celery import current_task

    try:
        order = Order.objects.get(guid=guid)
        logger.info("{0} order: {1}".format(log_prefix, order))
        logger.info("{0} order details: {1}".format(log_prefix, order.__dict__))
    except Order.DoesNotExist:
        logger.error("{0} order does not exist with guid {1}".format(log_prefix, order.guid))
        return

    i = 0
    for dicom_file in order.dicoms.all():
        logger.info("{0} dicom_file: {1}".format(log_prefix, dicom_file))
        logger.info("{0} dicom_file details: {1}".format(log_prefix, dicom_file.__dict__))

        try:
            i += 1
            logger.info("{0} dicom_file process count: {1}".format(log_prefix, i))

            try:
                filename = dicom_file.file.name.split('/')[2]
                logger.info("{0} filename: {1}".format(log_prefix, filename))
            except Exception as e:
                logger.error("{0} cannot get filename. error: {1}".format(log_prefix, e))
                continue

            try:
                logger.info("{0} dicom_file.file: {1}".format(log_prefix, dicom_file.file))
                file_image = dicom.read_file(dicom_file.file)
            except IOError as e:
                logger.error("{0} cannot read dicom file {1}. error: {2}".format(log_prefix, dicom_file.file, e))
                continue
            except Exception as e:
                logger.error("{0} cannot read dicom file {1}. error: {2}".format(log_prefix, dicom_file.file, e))
                continue

            try:
                file_image.PatientName = str(order.patient_name)
                file_image.PatientID = str(order.patient_id)
                file_image.PatientBirthDate = str(order.patient_date_of_birth)
                file_image.PatientSex = str(order.patient_gender)
                file_image.PatientTelephoneNumbers = str(order.patient_phone)
                file_image.PatientAge = "0{0}Y".format(order.patient_age) if order.patient_age else ''
                file_image.AccessionNumber = str(order.accession_id)
                file_image.Modality = str(order.modality)
                file_image.StationName = str(order.hospital_name)

                for meta in order.metas.all():
                    logger.info("{0} meta.key: {1}".format(log_prefix, meta.key))
                    if meta.key == 'TransferSyntaxUID' or meta.key == 'LossyImageCompression':
                        continue
                    meta_value = None

                    try:
                        logger.info(u"{0} meta.value: '{1}' meta.key: '{2}'".format(log_prefix, meta.key, meta.value))

                        if meta.value and meta.value != "None":
                            meta_value = meta.value
                    except ValueError as e:
                        logger.error(u"{0} value error for meta.value. error: {1}".format(log_prefix, e))
                    except Exception as e:
                        logger.error(u"{0} unexpected error for meta.value. error: {1}".format(log_prefix, e))

                    logger.info(u"{0} meta_value: {1}".format(log_prefix, meta_value))

                    try:
                        if meta.key in file_image:
                            logger.info(u"{0} meta key '{1}' is in file image".format(log_prefix, meta.key))

                            data_element = file_image.data_element(meta.key)
                            logger.info(u"{0} data_element: {1}".format(log_prefix, data_element))

                            data_element.value = unicode(meta_value)
                        else:
                            logger.error(u"{0} meta key '{1}' is not in file image".format(log_prefix, meta.key))
                            setattr(file_image, meta.key, str(meta_value))
                    except Exception as e:
                        logger.error(u"{0} cannot set meta value {1} of key {2}. error: {3}".format(
                            log_prefix,
                            meta_value,
                            meta.key,
                            e
                        ))
                        continue

                # Save to local media
                try:
                    dicom_file_path = os.path.join(settings.MEDIA_ROOT, "dicoms", str(order.guid), filename)
                    logger.info("{0} dicom_file_path: {1}".format(log_prefix, dicom_file_path))

                    file_image.save_as(dicom_file_path)
                    logger.info("{0} dicom file {1} saved successfully to local media".format(log_prefix, filename))
                except Exception as e:
                    logger.error("{0} cannot save dicom file '{1}' to local media. error: {2}".format(
                        log_prefix,
                        filename,
                        e
                    ))
                    continue

                logger.info("{0} order {1} dicom {2} meta write successful".format(log_prefix, order.pk, dicom_file.id))

                dicom_file.edit_meta = 1
                logger.info("{0} dicom_file.edit_meta: {1}".format(log_prefix, dicom_file.edit_meta))

                dicom_file.save()

                current_task.update_state(state='PROGRESS', meta={'type': 'meta_write', 'current_item': i})
            except Exception as e:
                logger.error("{0} order {1} dicom {2} meta write failed. error: {3}".format(
                    log_prefix,
                    order.pk,
                    dicom_file.id,
                    e
                ))
                pass
        except Exception as e:
            logger.error("{0} order {1} dicom {2} meta write failed. error: {3}".format(
                log_prefix,
                order.pk,
                dicom_file.id,
                e
            ))
            pass

    try:
        if order.ah_order_guid:
            logger.info("Deleting dicom files from platform for order: {0}".format(order.ah_order_id))

            dicom_delete_url = "{0}/api/dicoms/?secret_key={1}&ah_order_guid={2}".format(
                settings.AH_SERVER,
                settings.CLIENT_SECRET_KEY,
                order.ah_order_guid
            )
            logger.info("dicom_delete_url: {0}".format(dicom_delete_url))

            response = requests.delete(dicom_delete_url, headers={'content-type': 'text/plain'})
            logger.info("Dicom files delete response: {0}".format(response.status_code))

            if response.status_code == 200:
                logger.info("Dicom files deleted successfully from platform for order: {0}".format(order.ah_order_id))

                # Checking edit_meta for sending dicom to middleware
                if order.dicoms.filter(edit_meta=0).count() == 0:
                    send_dicom_to_alemhealth(guid=order.guid)
                    logger.info("Middleware dicom sync done successfully")
            else:
                logger.error("Dicom files cannot be deleted from platform for order: {0}".format(order.ah_order_id))
        else:
            if order.dicoms.filter(edit_meta=0).count() == 0:
                send_dicom_to_alemhealth(guid=order.guid)
                logger.info("Middleware dicom sync done successfully")
    except Exception as e:
        logger.error("Exception occurs when deleting dicom files from platform for order: {0}. error: {1}".format(
            order.ah_order_id,
            e
        ))

    return


# Check and create directory
def check_and_create_directory(dir_path, dir_name, parent_dir):
    try:
        if not os.path.exists(dir_path):
            logger.info(dir_name + " directory does not exist inside " + parent_dir + " directory")
            os.makedirs(dir_path)
            logger.info(dir_name + " directory created inside " + parent_dir + " directory")
        else:
            logger.info(dir_name + " directory already exist inside " + parent_dir + " directory")
    except Exception as e:
        logger.error("Exception occurs when creating " + dir_name + " directory inside " + parent_dir + " directory. Reason: " + str(e))
        return False
    return True


# Make archive of order guid directory inside compressed directory
# Archive will be in tar.gz format
# Move archive to compressed_tar directory
def generate_archive(media_dir, guid):
    try:
        logger.info("Creating tar")
        base_name = os.path.join(media_dir, 'compressed_tar', str(guid))
        root_dir = os.path.join(media_dir, 'compressed', str(guid))
        shutil.make_archive(base_name, 'gztar', root_dir)
        logger.info("Tar file " + str(guid) + ".tar.gz created successfully")
    except Exception as e:
        logger.error("Exception occurs when creating tar. Reason: " + str(e))
        return False
    return True


def _get_or_none(model, **kwargs):
    try:
        return model.objects.get(**kwargs)
    except model.DoesNotExist:
        return None


def apply_jpegls_compression(order, dicom_filefield):
    """
    Decide if jpegls compression should be applied to this order.

    Lossy Image Compression 	(0028,2110)
    Specifies whether an Image has undergone lossy compression.
    Enumerated Values:
        00 = Image has NOT been subjected to lossy compression.
        01 = Image has been subjected to lossy compression.
    Check the following link for details.
        http://dicom.nema.org/medical/dicom/current/output/html/part03.html#sect_C.7.6.1.1.5

    dcmcjpls supports the following transfer syntaxes for input (dcmfile-in):

    LittleEndianImplicitTransferSyntax             1.2.840.10008.1.2
    LittleEndianExplicitTransferSyntax             1.2.840.10008.1.2.1
    DeflatedExplicitVRLittleEndianTransferSyntax   1.2.840.10008.1.2.1.99 (*)
    BigEndianExplicitTransferSyntax                1.2.840.10008.1.2.2

    :param order:
    :return:
    """
    # http://www.fujifilmusa.com/products/medical/technical-documents/product-bulletins/pdf/index/fcr_dicom.pdf
    # Check page 38 to know the values

    allowed_transfer_syntax = settings.ALLOWED_TRANSFER_SYNTAX_DCMCJPLS

    try:
        logger.info('Explore tranfer syntax and Lossy compression for order: {}'.format(str(order.guid)))
        transfer_syntax = _get_or_create_transfer_syntax(order, dicom_filefield)
        lossy_image_compression = _get_or_create_order_meta(order, dicom_filefield, 'LossyImageCompression')
        logger.info('Order dicom has TransferSyntaxUID: {}, LossyImageCompression: {}'.format(transfer_syntax,
                                                                                              lossy_image_compression))

        return transfer_syntax is None or transfer_syntax.upper() in allowed_transfer_syntax
    except Exception:
        logger.exception('Exception reading dicom file {} for order {}'.format(dicom_filefield.file, str(order.guid)))
        return True


def _get_or_create_order_meta(order, dicom_filefield, meta_key):
    """
    This method will lookup up OrderMeta model for dicom key.

    If the OrderMeta object has no value a new object will be created and stored

    """
    order_meta_obj = _get_or_none(OrderMeta, order=order, key=meta_key)
    order_meta_value = getattr(order_meta_obj, 'value', None)
    if not order_meta_value:
        dicom_file = dicom.read_file(dicom_filefield.file)
        meta_value = str(dicom_file.get(meta_key, ''))
        if not meta_value:
            logger.info('OrderMeta {key} not found in dicom file'.format(key=meta_key))
            return meta_value
        order_meta_obj, created = OrderMeta.objects.update_or_create(order=order,
                                                                     key=meta_key,
                                                                     defaults={'value': meta_value})
        order_meta_value = getattr(order_meta_obj, 'value', None)
        logger.info('OrderMeta {key} is {status}: {value}'.format(key=meta_key,
                                                                  status='created' if created else 'updated',
                                                                  value=order_meta_value))
    return order_meta_value


# FIXME: Merge this method with _get_or_create_order_meta()
def _get_or_create_transfer_syntax(order, dicom_filefield):
    from dicom.filereader import read_file_meta_info

    _meta_key = 'TransferSyntaxUID'
    order_meta_obj = _get_or_none(OrderMeta, order=order, key=_meta_key)
    order_meta_value = getattr(order_meta_obj, 'value', None)
    if not order_meta_value:
        dicom_file = read_file_meta_info(os.path.join(settings.MEDIA_ROOT, dicom_filefield.file.name))
        meta_value = str(dicom_file.get(_meta_key, ''))
        if not meta_value:
            logger.info('OrderMeta {key} not found in dicom file'.format(key=_meta_key))
            return meta_value
        order_meta_obj, created = OrderMeta.objects.update_or_create(order=order,
                                                                     key=_meta_key,
                                                                     defaults={'value': meta_value})
        order_meta_value = getattr(order_meta_obj, 'value', None)
        logger.info('OrderMeta {key} is {status}: {value}'.format(key=_meta_key,
                                                                  status='created' if created else 'updated',
                                                                  value=order_meta_value))
    return order_meta_value


# Send dicom to middleware
def send_dicom_to_alemhealth(guid):
    logger.info("Enter into send_dicom_to_alemhealth method")

    from poster.streaminghttp import register_openers

    register_openers()
    hospital_id = json.loads(Config.objects.get(key='facility').value)['id']

    # Middleware order sync endpoint
    order_url = settings.MIDDLEWARE_SERVER + "/api/order/?secret_key=" + settings.MIDDLEWARE_SECRET_KEY

    # Media directory
    media_dir = settings.MEDIA_ROOT + '/'

    logger.info("order_url: " + str(order_url))
    logger.info("hospital_id: " + str(hospital_id))

    try:
        order = Order.objects.get(guid=guid)
        logger.info('Order: ' + str(order.pk) + ' start syncing to middleware')

        # if not order.ah_order_guid:
        default_provider_guid = get_default_provider()

        if order.provider_guid:
            provider_guid = str(order.provider_guid)
        elif default_provider_guid:
            try:
                order.provider_guid = default_provider_guid
                order.save()
                logger.info("default_provider_guid {0} updated in order {1} provider_guid".format(
                    default_provider_guid,
                    order.pk
                ))
            except Exception as e:
                err_msg = "Unexpected error: {0}".format(e)
                logger.error(err_msg)
                raise Exception(err_msg)

            provider_guid = default_provider_guid
        else:
            provider_guid = None

        # Middleware order sync data
        data = {
            'order_guid': str(guid),
            'hospital_id': hospital_id,
            'priority': order.priority,
            'doctor_id': None,
            # 'provider_id': json.loads(Config.objects.get(key='hospital').value)['id'],
            'provider_guid': provider_guid,
            'is_storage': True
        }

        logger.info("Order sync API request to middleware")
        response = requests.post(order_url, data=json.dumps(data), headers={'content-type': 'text/plain'})
        response = json.loads(response.text)

        logger.info("Middleware order sync data: " + str(data))
        logger.info("Middleware order sync response: " + str(response))

        # Update order upon middleware order sync response
        if response['status'] == 200:
            logger.info("order: " + str(response['order']['id']) + ' successfully added in hub')
        else:
            logger.error("Middleware order sync API response status: " + str(response['status']))
            return

        # Set compression flag
        # compression_flag = {'True': True, 'False': False}.get(Config.objects.get(key='compress').value)
        compression_flag = True

        _apply_dcmcjpls = (settings.ENABLE_DCMCJPLS
                           and apply_jpegls_compression(order, order.dicoms.filter(edit_meta=1).first()))
        logger.info('Run dcmcjpls for dcm files: {}'.format(_apply_dcmcjpls))

        # Dicom compression
        for dicom_file in order.dicoms.filter(edit_meta=1):
            dicom_file_path = os.path.join(media_dir, dicom_file.file.name)
            logger.info("dicom_file_path: " + str(dicom_file_path))

            # Checking if compression enabled
            if (compression_flag):
                logger.info("Compression started")

                # Check and create compressed directory inside media directory if not exist
                compressed_dir_path = media_dir + "compressed"
                if not check_and_create_directory(compressed_dir_path, "compressed", "media"):
                    return

                # Check and create order guid directory inside compressed directory if not exist
                order_guid_dir_path = media_dir + "compressed/" + str(guid)
                if not check_and_create_directory(order_guid_dir_path, str(guid), "compressed"):
                    return

                # dcmcjpls compression
                try:
                    compressed_file_path = dicom_file_path.replace("dicoms", "compressed")
                    logger.info("compressed_file_path: {} apply compression: {}".format(compressed_file_path,
                                                                                        _apply_dcmcjpls))
                    if _apply_dcmcjpls:
                        os.system("dcmcjpls %s %s" % (dicom_file_path, compressed_file_path + " -v"))
                    else:
                        shutil.copy(dicom_file_path, compressed_file_path)
                except Exception as e:
                    logger.error("Exception occurs when compressing. Reason: " + str(e))
                    return
            else:
                logger.error("Compression is not enabled")
                return

        # Check and create compressed_tar directory inside media directory if not exist
        compressed_tar_dir_path = media_dir + "compressed_tar"
        if not check_and_create_directory(compressed_tar_dir_path, "compressed_tar", "media"):
            return

        # Create tar file
        if not generate_archive(media_dir, guid):
            return

        # Middleware dicom sync endpoint
        dicom_url = "{}/api/dicoms/?secret_key={}&guid={}&jpegls_compressed={}".format(settings.MIDDLEWARE_SERVER,
                                                                                        settings.MIDDLEWARE_SECRET_KEY,
                                                                                        str(order.guid),
                                                                                        _apply_dcmcjpls)
        compressed_tar_file_path = os.path.join(media_dir, "compressed_tar", "{}.tar.gz".format(str(guid)))

        # Dicom sync API request to middleware
        try:
            logger.info('Sync dicoms of order {} to {}'.format(guid, dicom_url))
            response = _post_dicom_to_middleware(dicom_url, compressed_tar_file_path)

            # Order status updated upon middleware dicom sync API response
            if response['status'] == 200:
                logger.info('Dicom sync is successful. Update ah_order_guid of order guid {}'.format(guid))
                order.ah_order_id = response["order"]["ah_order_id"]
                order.ah_order_guid = response["order"]["ah_order_guid"]
                order.status = 2  # Dicom synced successfully to middleware
                order.save()
            else:
                logger.critical("Middleware dicom sync API response error. Response status is: " + str(response['status']))
        except Exception as e:
            logger.critical("Middleware dicom sync API exception occurs. Reason: " + str(e))
            logger.exception(e)

        # Remove compressed and compressed_tar directory
        try:
            shutil.rmtree(media_dir + "compressed")
            logger.info("Compressed directory removed successfully")
            shutil.rmtree(media_dir + "compressed_tar")
            logger.info("Compressed tar directory removed successfully")
        except Exception as e:
            logger.error("Cannot remove directory. Reason: " + str(e))
    except Order.DoesNotExist as e:
        logger.error("Exception occurs in DoesNotExist. Reason: " + str(e))
    except requests.exceptions.ConnectionError as e:
        logger.info("Exception occurs in ConnectionError. Reason: " + str(e))
    except Exception as e:
        logger.info("Exception occurs. Reason: " + str(e))
    return


def _post_dicom_to_middleware(dicom_url, file_path):
    file_size = os.path.getsize(file_path)/1000
    logger.info("Tar path: %s, SIZE: %sKB" % (file_path, file_size))
    logger.info("Dicom sync API request to middleware")
    start_time = datetime.datetime.now()

    if temp_settings.CONCURRENT_UPLOAD:
        response = transfer.transfer(file_path, 'application/x-tar', dicom_url)
    else:
        datagen, headers = multipart_encode({"file": open(file_path, "rb")})
        logger.info("datagen: " + str(datagen))
        logger.info("headers: " + str(headers))

        request = urllib2.Request(dicom_url, datagen, headers)
        response = urllib2.urlopen(request)

        response = json.loads(response.read())
        logger.info("Middleware dicom sync API request as dict: " + str(request.__dict__))

    logger.info("Middleware dicom sync API response as json: " + str(response))
    end_time = datetime.datetime.now()
    logger.info("Total time to post dicom tar(%sKB): %s %s" % (file_size,
                                                              (end_time - start_time),
                                                              'threaded' if temp_settings.CONCURRENT_UPLOAD else ''))
    return response


def get_accession_id_from_dicom_file(folder_path):
    for dicom_file in os.listdir(folder_path + "/."):
        if dicom_file.endswith(".dcm"):
            try:
                dicom_data = dicom.read_file(os.path.join(folder_path, dicom_file))
                if dicom_data.AccessionNumber:
                    return dicom_data.AccessionNumber
            except Exception as e:
                logger.error("Unexpected error: {0}".format(e))
    return None


def update_modality_work_list_status(accession_id, status):
    modality_work_list_detail = ModalityWorkListDetail()

    if modality_work_list_detail.update_status_by_accession_id(accession_id, status):
        logger.info("Modality work list with accession_id {0} status updated to {1}".format(
            accession_id,
            status
        ))
    else:
        logger.error("Modality work list with accession_id {0} cannot update status to {1}".format(
            accession_id,
            status
        ))


def delete_modality_work_list_file(accession_id):
    modality_work_list_detail = ModalityWorkListDetail()

    if modality_work_list_detail.delete_file_by_accession_id(accession_id):
        logger.info("Modality work list with accession_id {0} file deleted successfully".format(accession_id))
    else:
        logger.error("Modality work list with accession_id {0} cannot delete file".format(accession_id))
