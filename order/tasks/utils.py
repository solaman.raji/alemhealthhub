import json
import logging
from uuid import UUID

from alemhealthhub.models import Config
from django.core.exceptions import ObjectDoesNotExist

logger = logging.getLogger('alemhealth')

SETTINGS_KEY = "settings"
HOSPITAL_KEY = "hospital"


def check_service_status(service_type):
    settings_key = "settings"
    service_status = False
    services = ["study_download", "report_create", "report_download"]

    logger.info("settings_key: {0}".format(settings_key))
    logger.info("service_type: {0}".format(service_type))
    logger.info("service_status default value: {0}".format(service_status))
    logger.info("services: {0}".format(services))

    if service_type not in services:
        logger.error("service_type {0} is not in services".format(service_type))
        return service_status

    try:
        settings_data = json.loads(Config.objects.get(key=settings_key).value)
        logger.info("settings_data: {0}".format(settings_data))

        try:
            if service_type == "study_download":
                service_status = settings_data["study"]["download"]
            elif service_type == "report_create":
                service_status = settings_data["report"]["create"]
            elif service_type == "report_download":
                service_status = settings_data["report"]["download"]
        except KeyError as e:
            logger.error("{0} not found".format(e))
        except Exception as e:
            logger.error("Unexpected error: {0}".format(e))
    except ObjectDoesNotExist:
        logger.error("{0} key does not exist".format(settings_key))
    except Exception as err:
        logger.error("Unexpected error: {0}".format(err))

    logger.info("{0}: {1}".format(service_type, service_status))

    if service_status:
        logger.info("{0} service is enabled".format(service_type))
    else:
        logger.info("{0} service is disabled".format(service_type))

    return service_status


def is_box():
    try:
        Config.objects.get(key=HOSPITAL_KEY)
    except ObjectDoesNotExist:
        return True
    except Exception:
        return True
    return False


def is_setting_available():
    try:
        return json.loads(Config.objects.get(key=SETTINGS_KEY).value)
    except ObjectDoesNotExist:
        return False
    except Exception:
        return False


def validate_uuid4(uuid_string, guid_type):
    guid = None

    try:
        guid = UUID(uuid_string, version=4).hex
    except ValueError as e:
        logger.error("{0} {1}, {2}".format(guid_type, uuid_string, e))
    except Exception as e:
        logger.error("Unexpected error: {0}".format(e))

    return guid


def find_default_provider_value(data):
    default_provider_guid = None

    try:
        settings_default_provider_guid = data["provider"]["default"]
        logger.info("settings_default_provider_guid: {0}".format(settings_default_provider_guid))

        if settings_default_provider_guid:
            default_provider_guid = validate_uuid4(settings_default_provider_guid, "default_provider_guid")
    except KeyError as e:
        logger.error("{0} not found".format(e))
    except Exception as e:
        logger.error("Unexpected error: {0}".format(e))

    return default_provider_guid


def get_default_provider():
    default_provider_guid = None

    if is_box():
        settings_data = is_setting_available()

        if settings_data:
            default_provider_guid = find_default_provider_value(settings_data)

    return default_provider_guid
