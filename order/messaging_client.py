import json
import logging
import urlparse

from haigha.connection import Connection
from haigha.message import Message

from alemhealthhub import settings

logger = logging.getLogger('alemhealth.messaging')


class MessageClient(object):
    durable = True
    auto_delete = False

    def __init__(self, amqp_url,
                 exchange=settings.RABBITMQ_DASHBOARD['EXCHANGE'],
                 exchange_type=settings.RABBITMQ_DASHBOARD['EXCHANGE_TYPE'],
                 queue=settings.RABBITMQ_DASHBOARD['QUEUE'],
                 routing_key=settings.RABBITMQ_DASHBOARD['ROUTING_KEY'], ):
        super(MessageClient, self).__init__()
        self._connection = None
        self._channel = None
        self._closing = False
        self._consumer_tag = None

        self.exchange = exchange
        self.exchange_type = exchange_type
        self.queue = queue
        self.routing_key = routing_key

        self._url = amqp_url
        logger.debug('Start initializing a message publisher')
        self._init_publisher()

    def _init_publisher(self):
        # FIXME: Hard coded value in vhost
        url_parts = urlparse.urlparse(self._url)
        self._connection = Connection(user=url_parts.username,
                                      password=url_parts.password,
                                      host=url_parts.hostname,
                                      # vhost=url_parts.path,
                                      vhost='/',
                                      heartbeat=None,
                                      debug=True)
        self._channel = self._connection.channel()
        self._channel.exchange.declare(self.exchange, self.exchange_type)
        self._channel.queue.declare(self.queue, auto_delete=self.auto_delete, durable=self.durable)
        self._channel.queue.bind(self.queue, self.exchange, routing_key=self.routing_key)
        logger.info('Initialized Publisher')

    @classmethod
    def get_publisher(cls):
        amqp_url = settings.RABBITMQ_DASHBOARD['AMQP_URL']
        return cls(amqp_url)

    def consume(self, auto_close=True):
        message = self._channel.basic.get(self.queue)
        logger.info("Consumed message: {} from queue: {}".format(message, self.queue))
        if not message:
            return None

        delivery_tag = message.delivery_info['delivery_tag']
        self._channel.basic.ack(delivery_tag=delivery_tag)

        logger.info("acknowledge mesage with tag: {}".format(delivery_tag))
        if not self._connection.closed:
            self._connection.close() if auto_close else ''
        return unicode(message.body)

    def publish(self, message):
        logger.info("Publish message: {} to exchange: {}".format(message, self.exchange))
        self._channel.basic.publish(Message(message, application_headers={'hello': 'world'}),
                                    self.exchange, self.routing_key)
        self._connection.close()


def publish(message):
    if type(message) == dict:
        message = json.dumps(message)
    logger.info('Publish message: %s' % message)
    publisher = MessageClient.get_publisher()
    publisher.publish(message)


def consume():
    publisher = MessageClient.get_publisher()
    message = publisher.consume()
    try:
        return json.loads(message)
    except:
        return message


def main():
    message = {'type': 'dummy', 'body': 'Hello World!'}
    logger.info('Published message: %s ' % message)
    publish(message)

    consumed_content = consume()
    logger.info('Consumed message: %s' % consumed_content)

    assert message == consumed_content


if __name__ == '__main__':
    main()
