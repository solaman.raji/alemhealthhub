RABBITMQ_DASHBOARD = dict(
    EXCHANGE='ah-feed-exchange-test',
    EXCHANGE_TYPE='direct',
    QUEUE='ah-activity-test',
    ROUTING_KEY='alemhealth',
    AMQP_URL='amqp://guest:guest@localhost:5672/%2F',
)
