from django.test import TestCase
from django.test import override_settings

from order.messaging_client import MessageClient
from ..tests import RABBITMQ_DASHBOARD


# FIXME: Overriding is not working and it needs to be investigated

@override_settings(RABBITMQ_DASHBOARD=RABBITMQ_DASHBOARD)
class TestMessageClient(TestCase):
    def setUp(self):
        self.queue = 'test-messaging'

    @override_settings(RABBITMQ_DASHBOARD=RABBITMQ_DASHBOARD)
    def test_simple_message_publish(self):
        """test if a simple message can be published"""
        message = 'Test Message Publish and consume'
        MessageClient.get_publisher().publish(message)

        consumed_message = MessageClient.get_publisher().consume(auto_close=True)

        assert message == consumed_message

    @override_settings(RABBITMQ_DASHBOARD=RABBITMQ_DASHBOARD)
    def test_context_message_publish(self):
        """Test if a context message can be published"""
        message = '''{  
           "action":"order-created",
           "message_guid":"8aeb7240-3ca2-494b-b993-8bc9fe0e9b55",
           "occurred_at":"2017-11-15T05:42:12.890535+00:00",
           "context":"Study 13 of patient ABI UOWARE from NoHil is created"
        }'''
        MessageClient.get_publisher().publish(message)

        consumed_message = MessageClient.get_publisher().consume(auto_close=True)

        assert message == consumed_message
