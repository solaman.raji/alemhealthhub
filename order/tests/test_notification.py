import uuid

from django.test import TestCase, override_settings

from order.messaging_client import consume
from order.tasks import Notification
from ..tests import RABBITMQ_DASHBOARD


# FIXME: Overriding is not working and it needs to be investigated

@override_settings(RABBITMQ_DASHBOARD=RABBITMQ_DASHBOARD)
class TestNotification(TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        consume()

    def test_order_reported(self):
        """test if notification is sent if an order is reported"""
        guid = uuid.uuid4()
        order = dict(
            id=12,
            guid=guid,
        )
        with self.settings(NOTIFY_DASHBOARD=False):
            assert Notification.notify_order_reported(order)

    def test_order_saved(self):
        """test if notification is sent if an order is saved"""
        guid = uuid.uuid4()
        order = dict(
            id=13,
            guid=guid,
            patient_name='ABI UOWARE',
            hospital_name='NoHil',

        )
        with self.settings(NOTIFY_DASHBOARD=True):
            assert Notification.notify_order_created(order)

    def test_dicoms_downloaded(self):
        """test if notification is sent if an order is downloaded"""
        guid = uuid.uuid4()
        order = dict(
            id=14,
            guid=guid,
            patient_name='ABU GUL',
            hospital_name='NoHil',
        )
        with self.settings(NOTIFY_DASHBOARD=True):
            assert Notification.notify_dicoms_downloaded(order)
