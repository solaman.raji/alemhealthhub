from django.core.management import BaseCommand
from order.tasks import add_dicom_from_listener


class Command(BaseCommand):
    args = 'dicom_file_path'
    help = 'Add dicom from storescp to AH platform'

    def handle(self, *args, **options):
        folder_path = args[0]
        add_dicom_from_listener.delay(folder_path=folder_path)
