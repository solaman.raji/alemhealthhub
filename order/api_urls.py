from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

# importing alemhealthhub api
from order.api import (
    OrderList,
    OrderDescription,
    DicomPrint,
    HospitalStudy,
    ReportView,
    ReportHeaderView,
    BoxAppIntegration,
    ISOFileView,
    SendDicomLocal,
    Completion,
    KnowledgeBase,
    DicomImage
)

urlpatterns = [
    # Order List
    url(r'^api/orderlist/?$', OrderList.as_view()),

    # Order Description
    url(r'^api/order/(?P<guid>\w+)?$', OrderDescription.as_view()),

    # Order printing
    url(r'^api/order/dicom-print/(?P<guid>\w+)?$', DicomPrint.as_view()),

    # Hospital Study
    url(r'^api/hospital_study/(?P<guid>\w+)?$', HospitalStudy.as_view()),

    # getting report
    url(r'^api/report/(?P<guid>\w+)$', ReportView.as_view()),

    # setting report
    url(r'^api/report$', ReportView.as_view()),

    # setting report header
    url(r'^api/reportheader/$', ReportHeaderView.as_view()),

    # Send Dicom To cd Burner
    url(r'^api/senddicomtolocal', SendDicomLocal.as_view()),

    # testing box app
    url(r'^api/send_to_proxy', BoxAppIntegration.as_view()),

    # testing box app
    url(r'^api/create_iso', ISOFileView.as_view()),

    # Completion Statistics
    url(r'^api/completion', Completion.as_view()),

    # Knowledge Base
    url(r'^api/knowledge_base', KnowledgeBase.as_view()),

    # Access dicom images by accession_id
    url(r'^api/dicom-image/(?P<accession_id>[-\w]+)?$', DicomImage.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
