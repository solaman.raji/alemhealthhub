from fpdf import FPDF, HTMLMixin
from alemhealthhub import settings


class PdfReport(FPDF, HTMLMixin):
    def header(self):
        # insert my logo
        self.image(settings.BASE_DIR + settings.STATIC_URL + "assets/images/header.png", 10, 5, 180)
        # Arial bold 15
        self.set_font('Arial', 'B', 15)
        
        # self.cell()
        # Move to the right
        self.cell(0)
        # Line break
        self.ln(72)

    def footer(self):
        self.set_y(-15)
        # Arial italic 8
        self.set_font('Arial', 'I', 8)
        # Page number
        self.cell(0, 10, 'Page ' + str(self.page_no()) + '/{nb}', 0, 0, 'C')

class NormalPdfReport(FPDF, HTMLMixin):
    def header(self):
        # insert my logo
        # self.image(settings.BASE_DIR + settings.STATIC_URL + "assets/images/logo.jpg", 10, 5, 180)
        # Arial bold 15
        self.set_font('Arial', 'B', 15)

        # self.cell()
        # Move to the right
        self.cell(0)
        # Line break
        self.ln(72)

    def footer(self):
        self.set_y(-15)
        # Arial italic 8
        self.set_font('Arial', 'I', 8)
        # Page number
        self.cell(0, 10, 'Page ' + str(self.page_no()) + '/{nb}', 0, 0, 'C')
