from datetime import timedelta
from order.tasks import (
    request_for_study,
    sending_report_to_platform,
    report_download_from_platform,
    add_dicom_from_listener,
    send_report_to_mirth
)

BROKER_URL = 'amqp://guest:guest@localhost:5672/'
BROKER_TRANSPORT_OPTIONS = {'fanout_prefix': True, 'fanout_patterns': True, 'visibility_timeout': 480}
CELERY_RESULT_BACKEND = BROKER_URL
CELERY_IGNORE_RESULT = True


CELERY_ROUTES = {
    'request_for_study': {'queue': 'study.download'},
    'sending_report_to_platform': {'queue': 'report.upload'},
    'report_download_from_platform': {'queue': 'report.download'},
    'add_dicom_from_listener':  {'queue': 'add.dicom.from.listener'},
    'send_report_to_mirth': {'queue': 'report.mirth'},
}


CELERYBEAT_SCHEDULE = {
    'request_for_study_every_50_sec': {
        'task': 'request_for_study',
        'schedule': timedelta(seconds=50),
        'args': (),
        'options': {'serializers': 'json', 'expires': 60},
    },

    'sending_report_to_platform': {
        'task': 'sending_report_to_platform',
        'schedule': timedelta(seconds=50),
        'args': (),
        'options': {'serializers': 'json', 'expires': 60},
    },

    'report_download_from_platform': {
        'task': 'report_download_from_platform',
        'schedule': timedelta(seconds=50),
        'args': (),
        'options': {'serializers': 'json', 'expires': 60},
    },
}

CELERY_TIMEZONE = 'UTC'
