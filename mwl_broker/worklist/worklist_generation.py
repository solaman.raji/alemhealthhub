import os
import subprocess
import tempfile

# FIXME: Move these to settings
# FIXME: Handle AE Title from settings
worklist_cache_directory = '/var/cache/alemhealthhub/worklist/alemmwl'
worklist_output_directory = '/var/lib/alemhealthhub/worklist/alemmwl'
if not os.path.exists(worklist_output_directory):
    os.mkdir(worklist_output_directory)
if not os.path.exists(worklist_cache_directory):
    os.mkdir(worklist_cache_directory)


class WorklistGenerator():
    sample_template = '''(0008,0050) SH {ref_id}
(0008,0005) CS  [ISO_IR 100]
(0010,0010) PN {patient_name}
(0010,0020) LO {patient_id}
(0010,0030) DA {date_of_birth}Y
(0010,0040) CS {gender_char}
(0020,000d) UI {study_guid}
(0032,1060) LO Retinal Scan
(0008,0060) CS  OT
(0040,0001) AE  TP_AM_NW400_001
(0040,0002) DA 19951015
(0010,4000) LT Glucose-Reading:{lab_result_score} mmol/L'''

    @classmethod
    def convert_to_dcm(cls, dump_input, filename=None):
        dcm_output_path = tempfile.NamedTemporaryFile('w+', dir=worklist_output_directory)
        dcm_output_path = open(os.path.join(worklist_output_directory, filename), 'w+') if filename else dcm_output_path

        fin = tempfile.NamedTemporaryFile('w+')
        fin = open(os.path.join(worklist_cache_directory, filename + '.dump'), 'w+') if filename else fin
        print 'write dump file to ', fin.name
        fin.write(dump_input)

        print 'Use dump2dcm from %s to %s' % (fin.name, dcm_output_path.name)
        subprocess.call(['which', 'dump2dcm'])
        # assert os.path.exists(dump_input_path), 'Input file not found: %s' % dump_input_path
        subprocess.call(['dump2dcm', '-v', '-g', fin.name, dcm_output_path.name])
        return dcm_output_path


if __name__ == '__main__':
    sample_dump = '''(0008,0050) SH 85efd038-2daf-4095-8f2b-be3e0b5902b5
(0008,0005) CS  [ISO_IR 100]
(0010,0020) LO 1521eb9a-a0d2-454f-9dcb-4408d190d7f1
(0010,0030) DA 34Y
(0010,0040) CS M
(0020,000d) UI ecc31761-13d8-40f6-95d4-e85f8cfa3067
(0008,0060) CS  OT
(0040,0001) AE  TP_AM_NW400_001
(0040,0002) DA 19951015
(0010,4000) LT Glucose-Reading: mmol/L'''

    outfile = WorklistGenerator.convert_to_dcm(sample_dump)
    print 'Done'
