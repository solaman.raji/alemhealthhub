import uuid
import datetime


class Profile():
    def __init__(self, name, gender, age=None, phone=None, **kwargs):
        self.name = name
        self.gender = gender
        self.age = age
        self.phone = phone


class Patient():
    def __init__(self, guid, profile):
        self.guid = guid
        self.profile = profile


class Study():
    def __init__(self, guid, modality, contrast, clinicalDescription, bodyPart, lab_result_score='', **kwargs):
        self.guid = guid if guid else uuid.uuid4()
        self.modality = modality
        self.lab_result_score = lab_result_score
        self.contrast = contrast
        self.clinical_description = clinicalDescription
        self.body_part = bodyPart


class Order():
    def __init__(self, guid, doctor, patient, studies):
        self.guid = guid
        self.doctor = doctor
        self.patient = patient
        self.studies = studies
        self.order_date = datetime.date.today().isoformat()
