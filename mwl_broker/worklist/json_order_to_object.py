import StringIO
import json
import sys

from order_hierarchy import Order, Patient, Study, Profile
from worklist_generation import WorklistGenerator

sample_json = '''
{
  "patient": {
    "guid": "1521eb9a-a0d2-454f-9dcb-4408d190d7f1",
    "name": "hasan",
    "phone": "01788401901",
    "age": 34,
    "gender": "M"
  },
  "order": {
    "guid": "85efd038-2daf-4095-8f2b-be3e0b5902b5",
    "doctorName": "Dr. Harison",
    "studyList": [
      {
        "guid": "9005fe26-0bce-46a0-a824-17b9ec8182ed",
        "modality": "X Ray",
        "contrast": "Oral contrast",
        "clinicalDescription": "This is clinical description",
        "bodyPart": "This is body part",
        "procedureDescription": "This is procedure description",
        "diagnosticCenter": "Padma Lab"
      },
      {
        "guid": "ecc31761-13d8-40f6-95d4-e85f8cfa3067",
        "modality": "MRI",
        "contrast": "Oral contrast",
        "clinicalDescription": "This is clinical description",
        "bodyPart": "This is body part",
        "procedureDescription": "This is procedure description",
        "diagnosticCenter": "Padma Lab"
      },
      {
        "guid": "40a4470b-08ff-49b5-a84e-55c9bf83b113",
        "modality": "CT Scan",
        "contrast": "No contrast",
        "clinicalDescription": "This is clinical description",
        "bodyPart": "This is body part",
        "procedureDescription": "This is procedure description",
        "diagnosticCenter": "LabAid diagnostic center"
      }
    ]
  }
}'''


def get_mwl_template():
    _template_list = [
        '(0008,0050) SH {ref_id}',
        '(0008,0005) CS  ISO_IR 100',
        '(0010,0020) LO {patient_id}',
        '(0010,0030) DA {date_of_birth}Y',
        '(0010,0040) CS {gender_char}',
        '(0020,000d) UI {study_guid}',
        '(0032,1060) LO Retinal Scan',
        '(0008,0060) CS  OT',
        '(0040,0001) AE  TP_AM_NW400_001',
        '(0040,0002) DA 19951015',
        '(0010,4000) LT Glucose-Reading:{lab_result_score} mmol/L',
    ]


def get_profile_iod(_profile):
    _template = '''(0010,0010) PN {name}
(0010,0040) CS {gender}
(0010,1010) AS {age}
(0010,2154) SH {phone}
(0010,21b0) LT None 
(0010,0030) DA {age}Y'''

    return _template.format(**_profile.__dict__)


def get_patient_iod(_patient):
    _template = '''(0010,0020) LO {guid}
'''

    return '\n'.join([_template.format(**_patient.__dict__), get_profile_iod(_patient.profile)])


def get_order_header_iod(_order):
    _template = '''
(0008,0050) SH {guid}
(0040,0002) DA {order_date},'''

    # return '\n'.join([_template.format(**_order.__dict__), get_patient_iod(_order.patient)])
    return _template.format(**_order.__dict__)


def get_study_iod(_study):
    _template = '''(0020,000d) UI {guid}
(0008,0060) CS  {modality}
(0040,0001) AE  alemmwl
(0010,4000) LT Glucose-Reading:{lab_result_score} mmol/L'''

    return _template.format(**_study.__dict__)


def generate_study_iod(order):
    # _profile_iod = get_profile_iod(order.patient.profile)
    _patient_iod = get_patient_iod(order.patient)
    _order_header = get_order_header_iod(order)

    for _std in order.studies:
        yield '\n'.join([_order_header, _patient_iod, get_study_iod(_std)])


def initialize_order(order_dict):

    _profile = Profile(**order_dict['patient'])
    _patient = Patient(order_dict['patient']['guid'], _profile)

    _studies = [Study(**_std) for _std in order_dict['order']['studyList']]

    return Order(order_dict['order']['guid'], order_dict['order']['doctorName'], _patient, _studies)


if __name__ == '__main__':
    filepath = sys.argv[1]
    sio = StringIO.StringIO(sample_json)
    order_dict = json.load(sio, encoding='UTF-8')
    print "order_dict: {0}".format(order_dict)

    _order = initialize_order(order_dict)
    print "_order: {0}".format(_order)

    # print get_order_header_iod(_order)
    # print get_profile_iod(_order.patient.profile)
    print "get patient iod: {0}".format(get_patient_iod(_order.patient))
    # print '\n\n\n'.join(list(generate_study_iod(_order)))

    for study in generate_study_iod(_order):
        f = WorklistGenerator.convert_to_dcm(study, filepath)
        print 'Work is available at ', f.name