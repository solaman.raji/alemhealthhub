from rest_framework import serializers
from mwl_broker.models import ModalityWorkList as ModalityWorkListModel


class ModalityWorkListCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = ModalityWorkListModel

    def create(self, validated_data):
        return ModalityWorkListModel.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.status = validated_data.get("status", instance.status)
        instance.save()
        return instance

    def to_representation(self, obj):
        return {
            "guid": obj.guid,
            "patient_id": obj.patient_id,
            "patient_name": obj.patient_name,
            "patient_gender": obj.patient_gender,
            "patient_age": obj.patient_age,
            "patient_date_of_birth": obj.patient_date_of_birth,
            "patient_phone": obj.patient_phone,
            "patient_body_part_examined": obj.patient_body_part_examined,
            "patient_study_description": obj.patient_study_description,
            "patient_additional_history": obj.patient_additional_history,
            "doctor_name": obj.doctor_name,
            "accession_id": obj.accession_id,
            "modality": obj.modality,
            "priority": obj.priority,
            "status": obj.get_status_display(),
            "request_data": obj.request_data,
        }


class ModalityWorkListSerializer(serializers.ModelSerializer):
    guid = serializers.UUIDField(read_only=True)
    status = serializers.CharField(source='get_status_display', read_only=True)
    request_data = serializers.JSONField(read_only=True)

    class Meta:
        model = ModalityWorkListModel
        fields = (
            "guid",
            "patient_id",
            "patient_name",
            "patient_gender",
            "patient_age",
            "patient_date_of_birth",
            "patient_phone",
            "patient_body_part_examined",
            "patient_study_description",
            "patient_additional_history",
            "doctor_name",
            "accession_id",
            "modality",
            "priority",
            "status",
            "request_data",
        )
