from __future__ import unicode_literals

from django.db import models
from jsonfield import JSONField
from uuidfield import UUIDField


def modality_work_list_directory_path(instance, filename):
    return 'worklist/alemmwl/{0}'.format(filename)


class ModalityWorkList(models.Model):
    REQUESTED = "requested"
    SENT_TO_PACKS = "sent_to_packs"
    CAPTURED = "captured"

    modality_list = (
        ('CT', 1),
        ('NM', 2),
        ('MR', 3),
        ('DS', 4),
        ('DR', 5),
        ('US', 6),
        ('OT', 7),
        ('HSG', 8),
        ('CR', 9),
    )

    priority_list = (
        ('Routine', 'Routine'),
        ('STAT', 'STAT')
    )

    status_list = (
        (REQUESTED, 'Requested'),
        (SENT_TO_PACKS, 'Sent to PACKS'),
        (CAPTURED, 'Captured'),
    )

    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )

    guid = UUIDField(auto=True, unique=True)
    patient_id = models.CharField(max_length=100, unique=True)
    patient_name = models.CharField(max_length=100, null=True, blank=True)
    patient_gender = models.CharField(max_length=2, null=True, blank=True, choices=GENDER_CHOICES)
    patient_age = models.CharField(max_length=10, null=True, blank=True)
    patient_date_of_birth = models.CharField(max_length=100, null=True, blank=True)
    patient_phone = models.CharField(max_length=100, null=True, blank=True)
    patient_body_part_examined = models.CharField(max_length=200, null=True, blank=True)
    patient_study_description = models.TextField(null=True, blank=True)
    patient_additional_history = models.TextField(null=True, blank=True)
    doctor_name = models.CharField(max_length=100, null=True, blank=True)
    accession_id = models.CharField(max_length=100, unique=True)
    modality = models.CharField(max_length=10, null=True, blank=True, choices=modality_list)
    priority = models.CharField(max_length=10, null=True, blank=True, choices=priority_list)
    filename = models.FileField(upload_to=modality_work_list_directory_path, null=True, blank=True)
    status = models.CharField(max_length=50, choices=status_list, default=REQUESTED)
    request_data = JSONField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'modality_work_list'

    def __unicode__(self):
        return self.accession_id
