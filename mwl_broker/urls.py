from django.conf.urls import url

from . import views
from mwl_broker.views import ModalityWorkList, ModalityWorkListDetail

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^mwl/order/$', views.Broker.as_view(), name='broker'),
    url(r'^api/modality_work_list/$', ModalityWorkList.as_view(), name='broker'),
    url(r'^api/modality_work_list/(?P<guid>\w+)?$', ModalityWorkListDetail.as_view()),
]
