from django.db import IntegrityError
from django.test import TestCase

from mwl_broker.models import ModalityWorkList as ModalityWorkListModel


class ModalityWorkListModelTestCase(TestCase):
    pk = 1
    patient_id = "P-1000"
    patient_name = "Patient 1000"
    patient_gender = "M"
    patient_age = "042Y"
    patient_date_of_birth = "19111111"
    patient_phone = "1234567890"
    patient_body_part_examined = "Abdomen"
    patient_study_description = "This is patient study description"
    patient_additional_history = "This is patient additional history"
    doctor_name = "Doctor 1000"
    accession_id = "AH-01-02-1000"
    modality = "CT"
    priority = "Routine"

    data = {
        "patient_id": patient_id,
        "patient_name": patient_name,
        "patient_gender": patient_gender,
        "patient_age": patient_age,
        "patient_date_of_birth": patient_date_of_birth,
        "patient_phone": patient_phone,
        "patient_body_part_examined": patient_body_part_examined,
        "patient_study_description": patient_study_description,
        "patient_additional_history": patient_additional_history,
        "doctor_name": doctor_name,
        "accession_id": accession_id,
        "modality": modality,
        "priority": priority
    }

    def setUp(self):
        ModalityWorkListModel.objects.create(**self.data)

    def test_fetch_modality_work_list(self):
        modality_work_list = ModalityWorkListModel.objects.get(pk=self.pk)
        self.assertEqual(modality_work_list.id, self.pk)

    def test_create_modality_work_list(self):
        data = {
            "patient_id": "P-1001",
            "accession_id": "AH-01-02-1001",
        }
        modality_work_list = ModalityWorkListModel.objects.create(**data)
        self.assertEqual(modality_work_list.id, 2)
        self.assertEqual(modality_work_list.patient_id, data["patient_id"])
        self.assertEqual(modality_work_list.patient_name, None)
        self.assertEqual(modality_work_list.patient_gender, None)
        self.assertEqual(modality_work_list.patient_age, None)
        self.assertEqual(modality_work_list.patient_date_of_birth, None)
        self.assertEqual(modality_work_list.patient_phone, None)
        self.assertEqual(modality_work_list.patient_body_part_examined, None)
        self.assertEqual(modality_work_list.patient_study_description, None)
        self.assertEqual(modality_work_list.patient_additional_history, None)
        self.assertEqual(modality_work_list.doctor_name, None)
        self.assertEqual(modality_work_list.accession_id, data["accession_id"])
        self.assertEqual(modality_work_list.modality, None)
        self.assertEqual(modality_work_list.priority, None)

    def test_unique(self):
        with self.assertRaises(IntegrityError):
            ModalityWorkListModel.objects.create(**self.data)

    def test_unique_patient_id(self):
        self.data["accession_id"] = "AH-01-02-1001"
        with self.assertRaises(IntegrityError):
            ModalityWorkListModel.objects.create(**self.data)

    def test_unique_accession_id(self):
        self.data["patient_id"] = "P-1001"
        with self.assertRaises(IntegrityError):
            ModalityWorkListModel.objects.create(**self.data)

    def test_not_null(self):
        data = {
            "patient_id": None,
            "accession_id": None,
        }
        with self.assertRaises(IntegrityError):
            ModalityWorkListModel.objects.create(**data)

    def test_not_null_patient_id(self):
        data = {
            "patient_id": None,
            "accession_id": "AH-01-02-1001",
        }
        with self.assertRaises(IntegrityError):
            ModalityWorkListModel.objects.create(**data)

    def test_not_null_accession_id(self):
        data = {
            "patient_id": "P-1001",
            "accession_id": None,
        }
        with self.assertRaises(IntegrityError):
            ModalityWorkListModel.objects.create(**data)
