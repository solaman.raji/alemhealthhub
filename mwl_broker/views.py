import os
import json
import uuid
import logging

from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from django.views.generic import View

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from alemhealthhub.settings import (
    WORK_LIST_DB_DIR,
    WORK_LIST_CACHE_DIR,
    WORK_LIST_DEFAULT_AE
)
from mwl_broker.worklist import json_order_to_object
from mwl_broker.worklist.worklist_generation import WorklistGenerator
from mwl_broker.models import ModalityWorkList as ModalityWorkListModel
from mwl_broker.serializers import ModalityWorkListCreateSerializer, ModalityWorkListSerializer

logger = logging.getLogger('alemhealth')


def index(request):
    return HttpResponse("Hello, world. You're at the MWL Broker index.")


@require_http_methods(["POST"])
def receive_order(request):
    print "Received request by post method"
    pass


@method_decorator(csrf_exempt, name='dispatch')
class Broker(View):
    # @method_decorator(csrf_exempt)
    # def dispatch(self, request, *args, **kwargs):
    #     return super(Broker, self).dispatch(request, *args, **kwargs)

    @csrf_exempt
    def get(self, request, *args, **kwargs):
        print 'Return the order ', request.__dict__
        return HttpResponse('result')

    @csrf_exempt
    def post(self, request, *args, **kwargs):
        # form = self.form_class(request.POST)
        # print 'Return the order ', request.body
        order_dict = json.loads(request.body, encoding='UTF-8')
        print "Order request: ", order_dict

        _order = json_order_to_object.initialize_order(order_dict)

        for study in json_order_to_object.generate_study_iod(_order):
            f = WorklistGenerator.convert_to_dcm(study, filename='.'.join([str(uuid.uuid4()), 'wl']))
            print 'Work is available at ', f.name
        return HttpResponse("Order is received")


class ModalityWorkList(APIView):
    log_prefix = "modality work list >>>"

    def get(self, request):
        logger.info("{0} get all modality work list".format(self.log_prefix))

        modality_work_list = ModalityWorkListModel.objects.all()
        logger.info("{0} modality_work_list: {1}".format(self.log_prefix, modality_work_list))
        logger.info("{0} num of modality_work_list: {1}".format(self.log_prefix, len(modality_work_list)))

        serializer = ModalityWorkListSerializer(modality_work_list, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        logger.info("{0} create new modality work list".format(self.log_prefix))
        logger.info("{0} request.body: {1}".format(self.log_prefix, request.body))

        request.data["request_data"] = json.loads(request.body)
        serializer = ModalityWorkListCreateSerializer(data=request.data)

        try:
            if serializer.is_valid(raise_exception=True):
                logger.info("{0} serializer is valid".format(self.log_prefix))
                serializer.save()
                status_code = status.HTTP_201_CREATED
                data = serializer.data
                work_list_db_file_path, work_list_cache_file_path = self.generate_work_list_files(data["guid"])
                dicom_data_set = self.get_dicom_data_set(data)
                self.write_work_list_cache_file(work_list_cache_file_path, dicom_data_set)
                self.dcm2dump_work_list_cache_file(work_list_cache_file_path, work_list_db_file_path)
                self.update_work_list_filename(data["guid"], os.path.basename(work_list_db_file_path))
        except Exception as e:
            status_code = status.HTTP_400_BAD_REQUEST
            data = {
                "code": status_code,
                "message": "Validation Failed",
                "errors": self.error_mapping(serializer.errors)
            }
            logger.error("{0} serializer validation error: {1}".format(self.log_prefix, e))

        logger.info("{0} response data: {1}".format(self.log_prefix, data))
        logger.info("{0} response status_code: {1}".format(self.log_prefix, status_code))
        return Response(data, status=status_code)

    def error_mapping(self, errors):
        error_list = []

        for field, message in errors.iteritems():
            error_list.append({
                "field": field,
                "message": message[0]
            })

        return error_list

    def generate_work_list_files(self, guid):
        logger.info("{0} generate work list files".format(self.log_prefix))
        logger.info("{0} WORK_LIST_DB_DIR: {1}".format(self.log_prefix, WORK_LIST_DB_DIR))
        logger.info("{0} WORK_LIST_CACHE_DIR: {1}".format(self.log_prefix, WORK_LIST_CACHE_DIR))
        logger.info("{0} WORK_LIST_DEFAULT_AE: {1}".format(self.log_prefix, WORK_LIST_DEFAULT_AE))

        work_list_db_dir = os.path.join(WORK_LIST_DB_DIR, WORK_LIST_DEFAULT_AE)
        logger.info("{0} work_list_db_dir: {1}".format(self.log_prefix, work_list_db_dir))

        work_list_db_filename = "{0}.wl".format(guid)
        logger.info("{0} work_list_db_filename: {1}".format(self.log_prefix, work_list_db_filename))

        work_list_db_file_path = os.path.join(work_list_db_dir, work_list_db_filename)
        logger.info("{0} work_list_db_file_path: {1}".format(self.log_prefix, work_list_db_file_path))

        if self.check_and_create_directory(work_list_db_dir, WORK_LIST_DEFAULT_AE, WORK_LIST_DB_DIR):
            work_list_lockfile = "lockfile"
            logger.info("{0} work_list_lockfile: {1}".format(self.log_prefix, work_list_lockfile))

            work_list_lockfile_path = os.path.join(work_list_db_dir, work_list_lockfile)
            logger.info("{0} work_list_lockfile_path: {1}".format(self.log_prefix, work_list_lockfile_path))

            open(work_list_lockfile_path, 'w+').close()

        open(work_list_db_file_path, 'w+').close()

        work_list_cache_dir = os.path.join(WORK_LIST_CACHE_DIR, WORK_LIST_DEFAULT_AE)
        logger.info("{0} work_list_cache_dir: {1}".format(self.log_prefix, work_list_cache_dir))

        self.check_and_create_directory(work_list_cache_dir, WORK_LIST_DEFAULT_AE, WORK_LIST_CACHE_DIR)

        work_list_cache_filename = "{0}.dump".format(guid)
        logger.info("{0} work_list_cache_filename: {1}".format(self.log_prefix, work_list_cache_filename))

        work_list_cache_file_path = os.path.join(work_list_cache_dir, work_list_cache_filename)
        logger.info("{0} work_list_cache_file_path: {1}".format(self.log_prefix, work_list_cache_file_path))

        open(work_list_cache_file_path, 'w+').close()

        return work_list_db_file_path, work_list_cache_file_path

    def get_dicom_data_set(self, data):
        logger.info("{0} get dicom data set".format(self.log_prefix))

        dicom_data_set_template = """
                                    (0010,0010) PN {patient_name}
                                    (0010,0020) LO {patient_id}
                                    (0010,0030) DA {patient_date_of_birth}
                                    (0010,0040) CS {patient_gender}
                                    (0008,0050) SH {accession_id}
                                    (0008,0060) CS {modality}
                                    """

        dicom_data_set = dicom_data_set_template.format(**data)
        logger.info("{0} dicom_data_set: {1}".format(self.log_prefix, dicom_data_set))

        return dicom_data_set

    def write_work_list_cache_file(self, file_path, dicom_data_set):
        with open(file_path, 'w+') as f:
                f.write(dicom_data_set)
                f.close()

    def dcm2dump_work_list_cache_file(self, src_file_path, dst_file_path):
        os.system("dump2dcm {0} {1}".format(src_file_path, dst_file_path))

    def update_work_list_filename(self, guid, filename):
        modality_work_list = ModalityWorkListModel.objects.get(guid=guid)
        modality_work_list.filename = os.path.join(WORK_LIST_DEFAULT_AE, filename)
        modality_work_list.save()

    def check_and_create_directory(self, dir_path, dir_name, parent_dir):
        try:
            if not os.path.exists(dir_path):
                logger.info("{0} directory does not exist inside {1} directory".format(dir_name, parent_dir))
                os.makedirs(dir_path)
                logger.info("{0} directory created inside {1} directory".format(dir_name, parent_dir))
            else:
                logger.info("{0} directory already exist inside {1} directory".format(dir_name, parent_dir))
        except Exception as e:
            logger.error("Exception occurs when creating {0} directory inside {1} directory. Reason: {2}".format(
                dir_name,
                parent_dir,
                e
            ))
            return False

        return True


class ModalityWorkListDetail(APIView):
    log_prefix = "modality work list >>>"

    def get_object_by_guid(self, guid):
        try:
            return ModalityWorkListModel.objects.get(guid=guid)
        except ModalityWorkListModel.DoesNotExist:
            logger.error("{0} object with guid {1} doesn't exist".format(
                self.log_prefix,
                guid
            ))
            return None

    def get_object_by_accession_id(self, accession_id):
        try:
            return ModalityWorkListModel.objects.get(accession_id=accession_id)
        except ModalityWorkListModel.DoesNotExist:
            logger.error("{0} object with accession_id {1} doesn't exist".format(
                self.log_prefix,
                accession_id
            ))
            return None

    def get(self, request, guid):
        logger.info("{0} get modality work list".format(self.log_prefix))

        guid = self.validate_uuid4(guid)
        logger.info("{0} guid after validation: {1}".format(self.log_prefix, guid))

        if not guid:
            logger.error("{0} invalid guid".format(self.log_prefix))
            status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            data = {
                "code": status_code,
                "message": "Invalid GUID",
            }
        else:
            modality_work_list = self.get_object_by_guid(guid)
            logger.info("{0} modality_work_list: {1}".format(self.log_prefix, modality_work_list))

            if modality_work_list:
                serializer = ModalityWorkListSerializer(modality_work_list)
                status_code = status.HTTP_200_OK
                data = serializer.data
            else:
                status_code = status.HTTP_404_NOT_FOUND
                data = {
                    "code": status_code,
                    "message": "Not Found",
                }

        logger.info("{0} response data: {1}".format(self.log_prefix, data))
        logger.info("{0} response status_code: {1}".format(self.log_prefix, status_code))
        return Response(data, status=status_code)

    def validate_uuid4(self, uuid_string):
        try:
            return uuid.UUID(uuid_string, version=4).hex
        except ValueError:
            return False
        except TypeError:
            return False

    def update_status_by_accession_id(self, accession_id, status):
        modality_work_list = self.get_object_by_accession_id(accession_id)
        logger.info("{0} modality_work_list: {1}".format(self.log_prefix, modality_work_list))

        if modality_work_list:
            serializer = ModalityWorkListCreateSerializer(
                modality_work_list,
                data={"status": status},
                partial=True
            )

            if serializer.is_valid():
                serializer.save()
                return True

        return False

    def delete_file_by_accession_id(self, accession_id):
        modality_work_list = self.get_object_by_accession_id(accession_id)
        logger.info("{0} modality_work_list: {1}".format(self.log_prefix, modality_work_list))

        if modality_work_list and modality_work_list.filename:
            filename_url = modality_work_list.filename.url
            logger.info("{0} filename_url: {1}".format(self.log_prefix, filename_url))

            filename, file_extension = os.path.splitext(filename_url)
            logger.info("{0} filename: {1}".format(self.log_prefix, filename))
            logger.info("{0} file_extension: {1}".format(self.log_prefix, file_extension))

            work_list_db_file_path = os.path.join(WORK_LIST_DB_DIR, "{0}.wl".format(filename))
            logger.info("{0} work_list_db_file_path: {1}".format(self.log_prefix, work_list_db_file_path))

            work_list_cache_file_path = os.path.join(WORK_LIST_CACHE_DIR, "{0}.dump".format(filename))
            logger.info("{0} work_list_cache_file_path: {1}".format(self.log_prefix, work_list_cache_file_path))

            if os.path.isfile(work_list_db_file_path) and os.path.isfile(work_list_cache_file_path):
                os.remove(work_list_db_file_path)
                os.remove(work_list_cache_file_path)
                return True

        return False
