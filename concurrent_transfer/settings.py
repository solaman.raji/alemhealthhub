# TODO: This url should be generated from settings by appending middleware URL and rest
from alemhealthhub import settings as hub_settings

# upload_split_url_template = 'http://flif-dev.pub/transfer/upload/{guid}/parts/{partno}'
upload_split_url_template = hub_settings.MIDDLEWARE_SERVER + '/transfer/upload/{guid}/parts/{partno}?secret_key=' + hub_settings.MIDDLEWARE_SECRET_KEY

# delivery_url_template = 'http://flif-dev.pub/transfer/delivery/{guid}'
delivery_url_template = hub_settings.MIDDLEWARE_SERVER + '/transfer/delivery/{guid}?secret_key=' + hub_settings.MIDDLEWARE_SECRET_KEY
