import tempfile

from concurrent_transfer.file_split import split_file
from concurrent_transfer.threaded_request import request_new_file_transfer, post_file_parts, post_file


def transfer(filepath, content_type, destination_url):
    # request for new upload
    # get a transfer guid
    # split files into parts
    # Prepare requests to upload each parts
    # raise/retry on exception
    # send request to transfer file to destination url

    transfer_guid = request_new_file_transfer()
    tmp_dir = tempfile.mkdtemp('ah', prefix='split')
    file_paths = split_file(filepath, tmp_dir)
    print 'File splitted in %s' % tmp_dir, file_paths

    post_file_parts(transfer_guid, filepath, file_paths, 'application/octet-stream')

    response = post_file(transfer_guid, filepath, destination_url, content_type)
    # FIXME: Clean the directory with the files inside
    # os.rmdir(tmp_dir)
    return response


if __name__ == '__main__':
    print 'Concurrent transfer module'
    # A Zip file in current directory
    _file_path = '/alemhealthhub/dicom-475.zip'
    _file_path = '/alemhealthhub/media/compressed_tar/3a0f7ac38fa840529c11aba7024a447e.tar.gz'
    _content_type = 'application/x-gzip'
    _destination_url = 'http://flif-dev.pub/api/dicoms'

    response = transfer(_file_path, _content_type, _destination_url)
    print 'Status code', response['status_code']
    print 'Message', response['message']
