import json
import os
import uuid

import requests
from requests_toolbelt import threaded, MultipartEncoder

from concurrent_transfer import file_split
from concurrent_transfer.settings import upload_split_url_template, delivery_url_template


def post_file(transfer_guid, src_file, destination_url, content_type):
    _delivery_url = delivery_url_template.format(guid=transfer_guid)
    print 'GET uploaded file status'
    response = requests.get(_delivery_url)
    response.raise_for_status()
    print 'Uploaded file status: ', response.text
    # FIXME: File size and hash should be calculated and passed to server for verification
    file_hash = 'dummy-file-hash'
    file_size = 121211

    payload = {'filename': os.path.basename(src_file),
               'content-type': content_type,
               'destination-url': destination_url,
               'file': ('filename', file('/dev/null', 'rb'), content_type),
               'file-hash': file_hash,
               'file-size': file_size,}

    print 'Request to deliver file'
    response = requests.post(_delivery_url, data=payload)
    print 'Final File delivery status', response.status_code
    common_response = {'status_code': response.status_code, 'message': response.text}
    common_response.update(response.json())
    return common_response


def post_file_parts(transfer_guid, src_file_name, filepaths_to_upload, content_type):
    request_list = []
    for request in _prepare_requests(transfer_guid, src_file_name=src_file_name,
                                     filepaths_to_upload=filepaths_to_upload,
                                     content_type=content_type):
        print "request: ", request
        request_list.append({
            'method': 'POST',
            'url': request['url'],
            'data': request['data'],
            'headers': request['headers'],
        })
    responses_generator, exceptions_generator = threaded.map(request_list)
    for response in responses_generator:
        print response.text

    for exc in exceptions_generator:
        print exc.message

    print 'Uploading file parts is done!'


def _prepare_requests(transfer_guid, src_file_name, filepaths_to_upload, content_type):
    url_template = upload_split_url_template
    print 'Prepare request data', url_template, src_file_name, content_type
    for filepath in filepaths_to_upload:
        filename = os.path.basename(filepath)
        encoder = MultipartEncoder(
            fields={'source_file_name': src_file_name,
                    'filepart_name': os.path.basename(filepath),
                    'filepart': ('file', open(filepath, 'rb'), content_type)}
        )
        yield {"url": url_template.format(guid=transfer_guid, partno=filename),
               "data": encoder,
               "headers": {'Content-Type': encoder.content_type}}


def request_new_file_transfer():
    # This should inform the server part about new upload request.
    # Which will return a new id to upload file parts
    return uuid.uuid4()


if __name__ == '__main__':
    _file_path = 'dicom-12222.zip'
    _content_type = 'application/zip'
    _splitted_dir = '/tmp/upload/000/'

    file_paths = file_split.split_file(_file_path, _splitted_dir)

    _transfer_guid = request_new_file_transfer()
    post_file_parts(_transfer_guid, 'dicom-12222.zip', file_paths, _content_type)

    post_file(_transfer_guid, _file_path, '', _content_type)

