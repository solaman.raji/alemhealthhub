import os
from sys import platform


def _get_filepart_paths(splitted_dir_path):
    filepart_paths = []

    for path in os.listdir(splitted_dir_path):
        splitted_file_path = os.path.join(splitted_dir_path, path)

        if os.path.isfile(os.path.join(splitted_file_path)):
            filepart_paths.append(splitted_file_path)

    return filepart_paths


def check_os():
    check_os_status = False
    if platform == "linux" or platform == "linux2":
        check_os_status = True
    return check_os_status


def split_file(filepath, splitted_dir, number_of_parts=10):
    if check_os():
        '''Return the tmp directory location where it kept the splitted files'''
        print 'Split file %s into %s' % (filepath, splitted_dir)
        split_cmd = "split -n " + str(number_of_parts) + " " + filepath + " -d " + splitted_dir + '/' + os.path.basename(filepath)
        print 'execute: ', split_cmd
        os.system(split_cmd)
        return _get_filepart_paths(splitted_dir)
    else:
        raise Exception('Split command will run only in linux environment')


if __name__ == '__main__':
    _filepath = 'omotayo_fadeke.zip'
    _splitted_dir = '/tmp/upload/000/'
    filepaths = split_file(_filepath, _splitted_dir)
    print filepaths
