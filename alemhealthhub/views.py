from django.shortcuts import render_to_response
from django.template import RequestContext

from django.conf import settings


def dashboard(request):
    data = {
        'AWS_STORAGE_BUCKET_NAME': settings.AWS_STORAGE_BUCKET_NAME,
        'USE_S3': settings.USE_S3,
        'STATIC_GUID': settings.STATIC_GUID,
    }

    return render_to_response(
        'dashboard/index.html', data, RequestContext(request))
