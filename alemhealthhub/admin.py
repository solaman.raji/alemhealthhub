from django.contrib import admin
from .models import *

admin.site.register(Config)
admin.site.register(ReportModel)
admin.site.register(UserProfile)
