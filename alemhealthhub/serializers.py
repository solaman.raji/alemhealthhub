from rest_framework import serializers
from .models import *
from django.contrib.auth.models import Group

class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ('name',)

class UserAuthSerializer(serializers.ModelSerializer):
    groups = GroupSerializer(many=True)
    class Meta:
        model = User
        fields = ('pk', 'username', 'first_name', 'email', 'groups')


class ConfigSerializer(serializers.ModelSerializer):
    value = serializers.CharField(read_only=True)

    class Meta:
        model = Config
        fields = '__all__'


class HostSerializer(serializers.ModelSerializer):
    value = serializers.CharField(read_only=True)

    class Meta:
        model = Host
        fields = '__all__'

class ReportSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReportModel
        fields = '__all__'

class ReportJsonSerializer(serializers.ModelSerializer):
    order_guid = serializers.UUIDField(required=True)

    class Meta:
        model = ReportJsonModel
        fields = '__all__'
