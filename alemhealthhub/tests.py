from django.test import TestCase

from alemhealthhub.utils import (
    get_hdd_status,
    get_http_status,
    ServerStatus,
    read_battery_status,
    run_battery_status_command,
)
import httpretty


class TestHubOnlineStatusAPI(TestCase):
    def setUp(self):
        super(TestHubOnlineStatusAPI, self).setUp()
        self.online_status_uri = '/api/online-status'

    @httpretty.activate
    def test_online_status_api_endpoint_exists(self):
        httpretty.HTTPretty.allow_net_connect = False

        response = self.client.head(self.online_status_uri)
        assert response.status_code == 200

    @httpretty.activate
    def test_api_response_for_success(self):
        httpretty.HTTPretty.allow_net_connect = False

        platform_url = "http://cloud.dev"
        middleware_url = "http://flif.dev"

        httpretty.register_uri(httpretty.GET, platform_url,
                               body='<html></html>',
                               status=200)
        httpretty.register_uri(httpretty.GET, middleware_url,
                               body='<html></html>',
                               status=200)

        with self.settings(AH_SERVER=platform_url, MIDDLEWARE_SERVER=middleware_url):
            response = self.client.get(self.online_status_uri)

        assert response.status_code == 200
        assert response.content_type == 'application/json'

        content = response.json()
        assert 'hub_connected' in content and content['hub_connected']
        assert 'platform_connected' in content and content['platform_connected'] == ServerStatus.available
        assert 'middleware_connected' in content and content['middleware_connected'] == ServerStatus.available
        assert 'storage' in content and 'primary' in content['storage']
        assert 'storage' in content and 'media' in content['storage']

    def test_api_reponse_without_internet(self):
        pass

    def test_api_response_when_cloud_not_available(self):
        pass


# TODO: Add test case for timeout
class TestCloudServerAccessible(TestCase):
    @httpretty.activate
    def test_platform_server_available(self):
        url = "http://cloud.alemhealth.com"
        httpretty.register_uri(httpretty.GET, url,
                               body='<html></html>',
                               status=200)

        assert get_http_status(url) == ServerStatus.available

    @httpretty.activate
    def test_platform_server_not_available(self):
        url = "http://cloud.alemhealth.com"
        httpretty.register_uri(httpretty.GET, url,
                               body='Gateway timeout error',
                               status=502)
        assert get_http_status(url) == ServerStatus.not_available


    @httpretty.activate
    def test_middleware_server_available(self):
        url = "http://ah_mw.cloud.alemhealth.com"
        httpretty.register_uri(httpretty.GET, url,
                               body='<html></html>',
                               status=200)

        assert get_http_status(url) == ServerStatus.available

    @httpretty.activate
    def test_middleware_server_not_available(self):
        url = "http://ah_mw.cloud.alemhealth.com"
        httpretty.register_uri(httpretty.GET, url,
                               body='Gateway timeout error',
                               status=502)
        assert get_http_status(url) == ServerStatus.not_available

    # @httpretty.activate
    # def test_middleware_server_get_timedout(self):
    #     url = "http://ah_mw.cloud.alemhealth.com"
    #     httpretty.register_uri(httpretty.GET, url,)
    #     self.assertEqual(get_http_status(url), ServerStatus.no_response)



class TestStorageStatus(TestCase):
    def test_get_storage_status(self):
        expected_status = dict(
            primary=dict(capacity='256 GB', used='150 GB'),
            media=dict(capacity='4096 GB', used='1900 GB'),
        )
        actual_status = get_hdd_status()
        assert actual_status.keys() == expected_status.keys()


battery_status_command_output = '''Init SSL without certificate database
battery.capacity: 100
battery.charge: 55
battery.charge.low: 5
battery.charge.warning: 20
battery.current: 0.000
battery.mfr.date: ?
battery.runtime: 3932100
battery.temperature: 27.66
battery.type: ?
battery.voltage: 10.48
device.mfr: Mini-Box.Com
device.model: OPEN-UPS2
device.serial: LI-ION
device.type: ups
driver.name: usbhid-ups
driver.parameter.pollfreq: 30
driver.parameter.pollinterval: 2
driver.parameter.port: auto
driver.parameter.productid: d005
driver.version: 2.7.2
driver.version.data: openUPS HID 0.1
driver.version.internal: 0.38
input.current: 0.000
input.voltage: 695.81
output.current: 0.000
output.voltage: 502.73
ups.mfr: Mini-Box.Com
ups.model: OPEN-UPS2
ups.productid: d005
ups.serial: LI-ION
ups.status: OL
ups.vendorid: 04d8'''


class TestUPSBatteryStatus(TestCase):
    def test_get_battery_status(self):
        expected_charge = '55%'
        assert expected_charge == read_battery_status(run_battery_status_command('echo battery.charge: 55'))

    def test_battery_hardware_available(self):
        pass

    def test_battery_command_not_working(self):
        pass
