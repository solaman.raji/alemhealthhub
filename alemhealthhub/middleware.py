from django.http import HttpResponse, HttpResponseGone, HttpResponsePermanentRedirect
from django.contrib.auth.models import User
from alemhealthhub.models import *
from django.core.management import call_command
from django.shortcuts import redirect
from alemhealthhub.views import dashboard
import os


class ProviderSettings(object):
    # custom_urlpatterns = set(filename.rpartition('/')[2][:-3] for filename in glob.glob(url_modules_path))

    """Hospital Settings Middelware to setup Hospital"""

    def process_request(self, request):
        try:
            Config.objects.get(key='hospital')
        except Exception as e:
            # import pdb;pdb.set_trace()

            # app to migrate
            make_mig = call_command('makemigrations','alemhealthhub', interactive=False)
            make_mig = call_command('makemigrations','order', interactive=False)

            # migrate
            call_command('migrate', interactive=False)


    redirect('dashboard')
