import StringIO
import subprocess
import json
import os
import urllib2
from collections import namedtuple

import requests
from django.conf import settings
from django.http import HttpResponse


def download_and_save_from_url(download_url, full_file_path):
    if not (download_url or full_file_path):
        raise Exception('No argument can be empty or None')
    response = urllib2.urlopen(download_url)

    try:
        new_file = open(full_file_path, 'w')
        new_file.write(response.read())

    except Exception as e:
        print str(e)
    finally:
        new_file.close()


def api_error_message(type, msg=""):
    error_message = {
        'DoesNotExist': {
            'code': '501',
            'message': msg + ' does not Exist',
            'description': ''
        },
        'DeleteSuccessfully': {
            'code': '202',
            'message': msg + ' delete Successfully',
            'description': ''
        },
        'ParameterMissing': {
            'code': '400',
            'message': 'Post Parameter Missing',
            'description': ''
        },
        'IntegrityError': {
            'code': '500',
            'message': msg,
            'description': ''
        },
        "ResourceAlreadyExist": {
            'code': '409',
            'message': msg + ' already exist',
            'description': ''
        },
        "InvalidAccessKey": {
            'code': '403',
            'message': 'You provided an invalid api key',
            'description': 'You provided an invalid api key.'
        }

    }

    return HttpResponse(json.dumps(error_message[type]),
                        mimetype="application/json",
                        status=int(error_message[type]['code']))


def dict_to_json(data, status):
    return HttpResponse(json.dumps(data), status=status)
    # return HttpResponse(json.dumps(data), mimetype="application/json", status=status)


def load_settings_from_config_file(config_file, silent=False):
    # Usage: locals().update(load_settings_from_config_file('/opt/alemhealthhub/conf/config.json', silent=True)or {})
    try:
        with open(config_file) as json_file:
            config = json.load(json_file)
            return config
    except IOError as e:
        if silent:
            return False
        e.strerror = 'Unable to load configuration file (%s)' % e.strerror
        raise


ss = namedtuple('ServerStatus', 'available no_response not_available')
ServerStatus = ss._make(['available', 'no_response', 'not_available'])


def get_http_status(url):
    assert url, 'Url is not specified to get status'
    try:
        response = requests.get(url, timeout=10.0)
        if response.status_code == 200:
            return ServerStatus.available
        else:
            return ServerStatus.not_available
    except:
        return ServerStatus.no_response


DiskUsage = namedtuple('DiskUsage', 'total used free')


def disk_usage(path):
    """Return disk usage statistics about the given path.

    Will return the namedtuple with attributes: 'total', 'used' and 'free',
    which are the amount of total, used and free space, in bytes.
    """
    st = os.statvfs(path)
    free = st.f_bavail * st.f_frsize
    total = st.f_blocks * st.f_frsize
    used = (st.f_blocks - st.f_bfree) * st.f_frsize
    return DiskUsage(total, used, free)


def get_hdd_status():
    primary_disk_usage = disk_usage(settings.BASE_DIR)
    media_disk_usage = disk_usage(settings.MEDIA_ROOT)

    return dict(
        primary=primary_disk_usage._asdict(),
        media=media_disk_usage._asdict()
    )


def run_battery_status_command(command):
    command_output = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE).stdout.read()
    return command_output


def read_battery_status(output=''):
    _charge_text = '-'
    lines = [line for line in StringIO.StringIO(output) if 'battery.charge:' in line]
    if lines:
        _charge_text = str(lines[0]).split(':', 1)[1].strip()
    return _charge_text + '%'


def get_battery_status():
    # FIXME: Run command to get UPS status
    # FIXME: Add condition when UPS hardware not available
    status = {'available': settings.BATTERY_HARDWARE_AVAILABLE}
    if settings.BATTERY_HARDWARE_AVAILABLE:
        output = run_battery_status_command(settings.BATTERY_STATUS_COMMAND)
        status['charge'] = read_battery_status(output)
    return status
