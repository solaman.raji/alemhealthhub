import json
import logging
import uuid
from collections import OrderedDict

import os
import requests
from django.conf import settings
from django.contrib.auth import authenticate, login
from django.core.exceptions import ObjectDoesNotExist
from django.db import connection
from django.test.client import RequestFactory
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from alemhealthhub.models import (
    ReportModel,
    ReportJsonModel,
    Technician,
    Radiologist,
    User,
    Group,
    Config,
)
from alemhealthhub.utils import (
    get_http_status,
    get_hdd_status,
    api_error_message,
    get_battery_status,
)
from order.tasks import (
    report_template_upload_to_platform,
    send_report_to_mirth
)
from .serializers import (
    ReportSerializer,
    ReportJsonSerializer,
    UserAuthSerializer,
    ConfigSerializer,
    HostSerializer,
)

logger = logging.getLogger('alemhealth')
logger.setLevel(logging.INFO)


# check db
class CheckExistence(APIView):
    def get(self, request):
        data = {}

        try:
            if (len(Config.objects.all()) > 0):
                data["status"] = 1
            else:
                raise Exception('table not set')
        except Exception as e:
            data["status"] = 0

        return Response(data)


# box setup
class BoxSetup(APIView):
    def post(self, request):
        data = {}

        try:
            configure = json.loads(request.body)
            configure = configure['config']

            if configure['password'] != configure['retype_password']:
                data['error'] = "Password Doesn't Match"
            else:
                try:
                    guid = configure['guid']

                    logger.info("guid before modify: " + str(guid))
                    logger.info("guid type before modify: " + str(type(guid)))
                    logger.info("guid len before modify: " + str(len(guid)))

                    guid = str(guid)
                    guid = guid.strip()
                    guid = guid.strip(None)

                    logger.info("guid after modify: " + str(guid))
                    logger.info("guid type after modify: " + str(type(guid)))
                    logger.info("guid len after modify: " + str(len(guid)))

                    # For escaping null character
                    try:
                        uuid.UUID(guid)
                    except:
                        logger.critical("Invalid uuid: %s" % guid)
                        raise Exception("Invalid uuid: %s" % guid)

                    logger.info("Guid len after deduction: " + str(len(guid)))

                    url = settings.AH_SERVER + '/api/providers/' + str(guid)
                    logger.info("box setup url: " + str(url))
                    param = {
                        'secret_key': settings.CLIENT_SECRET_KEY
                    }

                    logger.info("param: " + str(param))
                    logger.info("BoxSetup request url: " + str(url))

                    headers = {'content-type': 'application/json; charset=utf-8'}
                    result = requests.get(url, params=param, headers=headers)

                    logger.info("BoxSetup request result: " + str(result))
                    logger.info("BoxSetup request result dict: " + str(result.__dict__))
                    logger.info("BoxSetup request result header: " + str(result.headers))
                    logger.info("BoxSetup request result header content type: " + str(result.headers['Content-Type']))
                    logger.info("BoxSetup request result encoding: " + str(result.encoding))

                    if result.status_code == 200:
                        # hospital
                        config, created = Config.objects.get_or_create(key='hospital')
                        config.value = json.dumps(result.json())
                        config.save()

                        # compression
                        config, created = Config.objects.get_or_create(key='compress')
                        config.value = 'True'
                        config.save()

                        # cd burner server
                        config, created = Config.objects.get_or_create(key='want_local_server')
                        config.value = 'False'
                        config.save()

                        # nw_balance_mode
                        config, created = Config.objects.get_or_create(key='nw_balance_mode')
                        config.value = 'multi'
                        config.save()

                        # box root password
                        config, created = Config.objects.get_or_create(key='box_root_password')
                        config.value = 'ubuntu'
                        config.save()

                        try:
                            user = User()
                            user.first_name = configure['first_name']
                            user.username = configure['username']
                            user.set_password(configure['password'])
                            user.is_staff = True
                            user.is_superuser = True
                            user.save()
                            data['success'] = 'Provider added successfully'

                            # for direct login
                            user = authenticate(username=configure['username'], password=configure["password"])

                            # serializer = UserAuthSerializer(user, many=True)

                            serializer = UserAuthSerializer(user)
                            data['user'] = serializer.data

                        except Exception as e:
                            data['error'] = 'Sorry, something wrong. Try again. Reason: ' + str(e)
                    else:
                        data['error'] = 'Provider not found in Alemhealth Platform'
                except Exception as e:
                    data['error'] = 'Sorry, something wrong. Try again. Reason: ' + str(e)
        except:
            data['error'] = 'invalid post request'

        return Response(data)  # login


class UserAuth(APIView):
    def get(self, request):
        data = {}

        try:
            logger.info('Request GET Data: {}'.format(request.GET))
            logger.info('Request cookies: {}'.format(request.COOKIES))
            username = str(request.GET.get("username"))
            password = str(request.GET.get("password"))

            data = self.authenticate_user(request, username, password)
        except:
            data['error'] = "Invalid Post Request"

        return Response(data)

    def post(self, request):
        data = {}

        try:
            body = json.loads(request.body)
            logger.info('Request POST Data: {}'.format(request.POST))
            logger.info('Request cookies: {}'.format(request.COOKIES))
            username = str(body.get("username"))
            password = str(body.get("password"))

            data = self.authenticate_user(request, username, password)

        except:
            data['error'] = "Invalid Post Request"

        return Response(data)

    @classmethod
    def authenticate_user(cls, request, username, password):
        data = {}
        try:
            logger.info('Authenticate User: {} with password.'.format(username))
            user = authenticate(username=username, password=password)

            if user is not None:
                if user.is_active:
                    logger.info('User: {} is active. Login the user'.format(username))
                    login(request, user)
                    serializer = UserAuthSerializer(user)

                    data['success'] = "Successfully logged in"

                    data['user'] = serializer.data
                    logger.info('User {} logged in successfully'.format(username))
                else:
                    raise Exception('invalid')
            else:
                raise Exception('invalid')

        except Exception as e:
            data['error'] = "Invalid Username or Password"
        return data


class Members(APIView):
    def get(self, request, pk):
        data = {}

        try:
            user = User.objects.get(id=pk)
            serializers = UserAuthSerializer(user)

            data['user'] = serializers.data

        except Exception as e:
            data['error'] = str(e)

        return Response(data)

    def put(self, request):
        data = {}

        try:
            packet = json.loads(request.body)

            try:
                user = User.objects.get(id=packet["id"])

                user.username = packet["username"]

                if "password" in packet:
                    user.set_password(packet["password"])

                user.first_name = packet["first_name"]

                user.save()

                data["success"] = "Successfully Updated"

            except User.DoesNotExist as e:
                data["error"] = "User Doesn't Exist"
        except ValueError as e:
            data['error'] = "Invalid Put Request"

        return Response(data)

    def post(self, request):
        data = {}
        configure = json.loads(request.body)
        try:
            User.objects.get(username=configure['username'])
            data["error"] = "User already exists"
        except User.DoesNotExist:
            user = User()
            user.first_name = configure['first_name']
            user.username = configure['username']
            user.set_password(configure['password'])
            user.is_staff = True
            user.save()
            group = Group.objects.get_or_create(name=configure["group"])
            user.groups.add(group)
            serializers = UserAuthSerializer(user)
            data['user'] = serializers.data
            data['success'] = 'User added successfully'
        except Exception as e:
            data["error"] = "User not created. Reason: " + str(e)
        return Response(data)


class ConfigView(APIView):
    def get(self, request):
        data = {}
        configs = Config.objects.all()
        serializer = ConfigSerializer(configs, many=True)
        data["success"] = serializer.data
        return Response(data)

    def put(self, request):
        data = {}

        try:
            packet = json.loads(request.body)
            new_config = packet["config"]
            facility_access_key = new_config['facility_access_key']

            logger.info("facility guid: " + str(facility_access_key))
            logger.debug("guid type: " + str(type(facility_access_key)))
            logger.debug("guid len: " + str(len(facility_access_key)))

            try:
                uuid.UUID(facility_access_key)
            except:
                logger.critical("Not a valid guid: %s" % facility_access_key)
                raise Exception("Invalid guid: %s" % facility_access_key)

            # Check validity of facility_access_key
            logger.info("Checking facility access key validity.")
            url = settings.AH_SERVER + '/api/hospitals/' + facility_access_key + '?secret_key=' + settings.CLIENT_SECRET_KEY
            result = requests.get(url)
            logger.info("result " + str(result._content))

            if result.status_code != 200:
                new_config['facility_access_key'] = ''
                logger.error("Facility access key is not valid.")
                data["error"] = "Facility access key is not valid"
            else:
                try:
                    facility, created_facility = Config.objects.get_or_create(key="facility")
                    facility.value = str(result._content)
                    facility.save()
                except Exception as e:
                    logger.info("cannot add facility dict. Reason: " + str(e))

                logger.info("Facility access key is valid.")
                data["success"] = "Successfully Updated"

            try:
                for key, value in new_config.iteritems():
                    config, created = Config.objects.get_or_create(key=str(key))
                    config.value = str(value)
                    config.save()
            except Exception as e:
                data["error"] = str(e)
        except ValueError:
            data["error"] = "Invalid Request"

        return Response(data)


class System(APIView):
    def get(self, request):
        if request.GET.get("reboot"):
            try:
                configs = Config.objects.all()
            except Config.DoesNotExist:
                return api_error_message("DoesNotExist", "Config")

            data = {}
            data['configs'] = {a.key: a.value for a in configs}

            sudoPassword = data['configs']['box_root_password']
            command = "shutdown -r now"

            os.system('echo %s|sudo -S %s' % (sudoPassword, command))
            data = {}
            return Response(data, 200)


class Hosts(APIView):
    def get(self, request):
        from alemhealthhub.models import Host

        data = {}

        hosts = Host.objects.all()

        serializer = HostSerializer(hosts, many=True)

        data["success"] = serializer.data

        return Response(data)

    def post(self, request):
        from alemhealthhub.models import Host

        body = json.loads(request.body)
        data = {}

        try:
            host = Host()
            host.name = body["name"]
            host.port = body["port"]
            host.ae_title = body["ae_title"]
            host.ip_address = body["ip_address"]
            host.save()
            data['success'] = 'Host added successfully'
            serializer = HostSerializer(host)
            data['host'] = serializer.data
        except Exception as e:
            data['error'] = 'Host is not created'

        return Response(data)

    def put(self, request):
        from alemhealthhub.models import Host

        body = json.loads(request.body)
        data = {}

        try:
            Host.objects.filter(id=body["id"]).update(name=body["name"], port=body["port"], ae_title=body["ae_title"],
                                                      ip_address=body["ip_address"])
            data["success"] = "Host information has been updated"

        except Host.DoesNotExist as e:
            data["error"] = str(e)

        return Response(data)

    def delete(self, request):
        from alemhealthhub.models import Host

        data = {}

        try:
            host = Host.objects.get(id=request.GET.get("id"))
            host.delete()
            data["success"] = "Host information deleted"

        except Exception as e:
            data["error"] = str(e)

        return Response(data)


class CheckHost(APIView):
    def get(self, request, host_id):
        from alemhealthhub.models import Host
        data = {}

        if host_id:
            try:
                host = Host.objects.get(id=host_id)
                ae_title = host.ae_title
                port = host.port
                ip_address = host.ip_address
                check_host = os.system('echoscu %s %s -aec %s' % (
                    ip_address, port, ae_title
                )
                                       )
                if check_host is 0:
                    data["success"] = "Verification Succeeded"
                else:
                    data["error"] = "Verification Failed"
            except Exception as e:
                data["error"] = "Host not found"
        return Response(data)


# Report APIView
class ReportView(APIView):
    def post(self, request):
        data = {}
        parser = json.loads(request.body)

        try:
            report = ReportModel()
            if parser["procedure"] is not None:
                report.procedure = parser["procedure"]
            if parser["clinical_information"] is not None:
                report.clinical_information = parser["clinical_information"]
            if parser["comparison"] is not None:
                report.comparison = parser["comparison"]
            if parser["findings"] is not None:
                report.findings = parser["findings"]
            if parser["impression"] is not None:
                report.impression = parser["impression"]

            report.report_name = parser["report_name"]

            report.save()
            serializer = ReportSerializer(report)
            data["report"] = serializer.data
            data["status"] = 200

            # Upload report template to platform
            report_template_upload_to_platform.delay(template_id=report.id)
        except Exception as e:
            data["error"] = "Cannot create report. Reason: " + str(e)
            data["status"] = 500

        return Response(data)

    def get(self, request):
        data = {}
        report_id = request.GET.get("report_id")
        if report_id:
            try:
                report = ReportModel.objects.get(pk=report_id)
                serializer = ReportSerializer(report)
                data['report'] = serializer.data
                data["status"] = 200
            except ReportModel.DoesNotExist:
                data["error"] = "Cannot find report with id: " + str(report_id)
                data["status"] = 500
        else:
            try:
                page = request.GET.get('page')
                per_page = request.GET.get('per_page')

                if not per_page:
                    post_per_page = 25
                else:
                    post_per_page = int(per_page)

                if not page:
                    page = 1
                else:
                    page = int(page)

                offset = page * post_per_page - post_per_page
                pagesize = page * post_per_page
                reports = ReportModel.objects.all()
                count = reports.count()
                if count > 0:
                    reports = reports[offset:pagesize]
                    serializer = ReportSerializer(reports, many=True)
                    data["reports"] = serializer.data
                    data["count"] = count
                    data["status"] = 200
                else:
                    data["error"] = "There is no report here."
                    data["status"] = 400
            except Exception as err:
                data["error"] = "Cannot retrieve data. Reason: " + str(err)
                data["status"] = 500

        return Response(data)

    def put(self, request):
        data = {}
        parser = json.loads(request.body)
        print parser
        # import pdb; pdb.set_trace()
        try:
            report = ReportModel.objects.get(pk=parser["id"])
            if parser["procedure"] is not None:
                report.procedure = parser["procedure"]
            if parser["clinical_information"] is not None:
                report.clinical_information = parser["clinical_information"]
            if parser["comparison"] is not None:
                report.comparison = parser["comparison"]
            if parser["findings"] is not None:
                report.findings = parser["findings"]
            if parser["impression"] is not None:
                report.impression = parser["impression"]

            report.save()
            serializer = ReportSerializer(report)
            data["report"] = serializer.data
            data["status"] = 200

        except ReportModel.DoesNotExist:
            data["error"] = "Cannot find report with id: " + str(parser["id"])
            data["status"] = 400

        except Exception as e:
            data["error"] = "Cannot update report. Reason: " + str(e)
            data["status"] = 500

        return Response(data)

    def delete(self, request):
        data = {}
        report_id = request.GET.get("id")
        try:
            report = ReportModel.objects.filter(pk=report_id).delete()
            data["success"] = "Successfully deleted the report"
            data["status"] = 200

        except Exception as err:
            data["error"] = "Cannot delete the report. Reason: " + str(err)
            data["status"] = 500

        return Response(data)


# FIXME: Use LoginRequiredMixin to make it authenticated request

class ReportJsonView(APIView):
    templates = OrderedDict()
    templates['procedure'] = "<div style='text-align: center;'><u><b> {procedure} </b></u></div>"
    templates['clinical_information'] = "<div style='text-align: left;'><b>Clinical Information:</b></br> {clinical_information} </div>"
    templates['comparison'] = "<div style='text-align: left;'><b>Comparison:</b></br> {comparison} </div>"
    templates['findings'] = "<div style='text-align: left;'><b>Findings:</b></br> {findings} </div>"
    templates['impression'] = "<div style='text-align: left;'><b>Impression:</b></br> {impression} </div>"

    def post(self, request):
        data = {}
        parser = json.loads(request.body)
        logger.info('Report JSON POST: {}'.format(parser))

        try:
            data["report"] = self.save_report(parser)
            self.update_order_report(parser)
            data["status"] = 200
        except Exception as e:
            data["error"] = "Cannot create report. Reason: " + str(e)
            data["status"] = 500

        return Response(data)

    def get(self, request, order_guid):
        data = {}
        logger.info('GET Report JSON: {}'.format(order_guid))
        try:
            uuid.UUID(order_guid)
            report = ReportJsonModel.objects.get(order_guid=order_guid)
            serializer = ReportJsonSerializer(report)
            data['report'] = serializer.data
            data["status"] = 200
        except ReportJsonModel.DoesNotExist:
            data["error"] = "Cannot find report with id: " + str(order_guid)
            data["status"] = 500

        return Response(data)

    def put(self, request):
        data = {}
        parser = json.loads(request.body)
        logger.info('{} submits report JSON {}'.format(request.user, parser))

        try:
            data["report"] = self.save_report(parser)
            data['pdf-generation'] = self.update_order_report(parser)
            data["status"] = 200
        except ReportJsonModel.DoesNotExist:
            data["error"] = "Cannot find report with order guid: " + str(parser["order_guid"])
            data["status"] = 400

        except Exception as e:
            data["error"] = "Cannot update report. Reason: " + str(e)
            data["status"] = 500

        return Response(data)

    @classmethod
    def save_report(cls, report_data):
        if settings.MODALITY_WORK_LIST:
            send_report_to_mirth.delay(report_data)

        order_guid = report_data["order_guid"]
        logger.info('Save report json for order guid: {}'.format(order_guid))

        report, created = ReportJsonModel.objects.get_or_create(order_guid=order_guid)
        logger.info('Report is {} for order guid: {}'.format('created' if created else 'updated', order_guid))

        report.procedure = report_data.get('procedure', None)
        report.clinical_information = report_data.get('clinical_information', None)
        report.comparison = report_data.get('comparison', None)
        report.findings = report_data.get('findings', None)
        report.impression = report_data.get('impression', None)
        report.save()

        logger.info('Report json is saved for order guid: {}'.format(order_guid))

        serializer = ReportJsonSerializer(report)
        return serializer.data

    @classmethod
    def update_order_report(cls, report_data):
        logger.info('Submit report to generate PDF: {}'.format(report_data))
        order_guid = report_data["order_guid"]
        user_id = report_data["user_id"]

        # FIXME: Make it pythonic
        report = ""
        for key, template in cls.templates.iteritems():
            if key in report_data and report_data[key]:
                value = template.format(**report_data)
                logger.info('Report template for key {} and value {}'.format(key, value))
                report += value

        logger.info('Generated report template: ', report)

        request_body = {
            "order_guid": order_guid,
            "report": report,
            "user_id": user_id,
        }

        # Submit Report Using Request Factory
        url = 'api/report/{}'.format(order_guid)
        request_factory_object = RequestFactory()
        request = request_factory_object.put(url, content_type="application/json")
        request._body = json.dumps(request_body)

        from order.api import ReportView
        response = ReportView.as_view()(request)
        response.render()
        logger.info('PUT Report HTML to old order api: {}'.format(response.content))
        return response.content

    def delete(self, request):
        data = {}
        order_guid = request.GET.get("order_guid")
        try:
            ReportJsonModel.objects.get(order_guid=order_guid).delete()
            data["success"] = "Successfully deleted the report"
            data["status"] = 200

        except Exception as err:
            data["error"] = "Cannot delete the report. Reason: " + str(err)
            data["status"] = 500

        return Response(data)


class HubUserView(APIView):
    def post(self, request):
        data = {}
        parser = json.loads(request.body)
        try:
            user = User.objects.get(username=parser["username"])
            data["error"] = "User already exists"
        except User.DoesNotExist:
            user = User()
            user.first_name = parser["first_name"]
            user.username = parser["username"]
            user.set_password(parser["password"])
            user.save()
            g, d = Group.objects.get_or_create(name=parser["group"])
            g.user_set.add(user)

            if parser["group"] == "radiologist":
                radiologist = Radiologist()
                radiologist.user = user
                radiologist.title = parser["title"]
                radiologist.mobile = parser["mobile"]
                radiologist.save()

            if parser["group"] == "technician":
                technician = Technician()
                technician.user = user
                technician.title = parser["title"]
                technician.mobile = parser["mobile"]
                technician.save()

            serializer = UserAuthSerializer(user)
            data["user"] = serializer.data
            data["success"] = "User has been created"
        except Exception as e:
            data["error"] = "Cannot add User. Reason: " + str(e)

        return Response(data)


class HomeView(APIView):
    def get(self, request, type):
        data = {}
        try:
            chart_type = str(type)
            # import pdb; pdb.set_trace()
            if chart_type == "1":
                cursor = connection.cursor()
                sql = "select count(*), STRFTIME('%Y-%m-%d', created_at) as `date` from orders GROUP BY STRFTIME('%Y-%m-%d', created_at)"
                cursor.execute(sql)
                orders = cursor.fetchall()
                data["orders"] = orders
            if chart_type == "2":
                cursor = connection.cursor()
                sql = "select count(*), modality from orders GROUP BY modality"
                cursor.execute(sql)
                orders = cursor.fetchall()
                data["orders"] = orders
            if chart_type == "3":
                cursor = connection.cursor()
                sql = "select count(*), meta_status from orders GROUP BY meta_status"
                cursor.execute(sql)
                orders = cursor.fetchall()
                data["orders"] = orders

        except Exception as e:
            data["error"] = "Cannot get data. " + str(e)
        return Response(data)


class HubOnlineStatus(APIView):
    def get(self, request):
        platform_status = get_http_status(settings.AH_SERVER)
        middleware_status = get_http_status(settings.MIDDLEWARE_SERVER)
        storage_status = get_hdd_status()
        battery_status = get_battery_status()
        status_data = dict(
            hub_connected=True,
            platform_connected=platform_status,
            middleware_connected=middleware_status,
            storage=storage_status,
            battery_status=battery_status,
        )
        return Response(status_data, status=status.HTTP_200_OK, content_type='application/json')


class ProviderList(APIView):
    facility_key = "facility"

    def get(self, request):
        try:
            data = self.get_provider_list()
        except ObjectDoesNotExist:
            message = "{} key does not exist".format(self.facility_key)
            data = {
                "error": {
                    "status": status.HTTP_404_NOT_FOUND,
                    "message": message
                }
            }
            logger.error(message)
            return Response(data, status=status.HTTP_404_NOT_FOUND)
        except Exception as err:
            message = "exception occurs in get provider list. reason: {}".format(err.message)
            data = {
                "error": {
                    "status": status.HTTP_500_INTERNAL_SERVER_ERROR,
                    "message": "something went wrong",
                }
            }
            logger.error(message)
            return Response(data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        return Response(data, status=status.HTTP_200_OK)

    def get_provider_list(self):
        data = []
        config_facility_data = json.loads(Config.objects.get(key=self.facility_key).value)

        if "providers" in config_facility_data:
            providers = config_facility_data['providers']

            for provider in providers:
                provider_data = {
                    "value": uuid.UUID(provider["guid"]).hex,
                    "label": provider["name"],
                }
                data.append(provider_data)

        return data


class SettingsDetail(APIView):
    settings_key = "settings"

    def get(self, request):
        try:
            data = json.loads(Config.objects.get(key=self.settings_key).value)
            logger.info("config settings data: {0}".format(data))

            if self.get_provider_selection(data):
                data['provider']['provider_list'] = ProviderList().get_provider_list()
                logger.info("config settings data with provider list: {0}".format(data))
        except ObjectDoesNotExist:
            message = "{0} key does not exist".format(self.settings_key)
            data = {
                "error": {
                    "status": status.HTTP_404_NOT_FOUND,
                    "message": message
                }
            }
            logger.error(message)
            return Response(data, status=status.HTTP_404_NOT_FOUND)
        except Exception as err:
            message = "exception occurs in get provider list. reason: {}".format(err.message)
            data = {
                "error": {
                    "status": status.HTTP_500_INTERNAL_SERVER_ERROR,
                    "message": "something went wrong",
                }
            }
            logger.error(message)
            return Response(data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        return Response(data, status=status.HTTP_200_OK)

    def get_provider_selection(self, settings):
        provider_selection = False

        try:
            provider_selection = settings["provider"]["selection"]
        except KeyError as e:
            logger.error("{0} not found".format(e))
        except Exception as e:
            logger.error("Unexpected error: {0}".format(e))

        logger.info("provider_selection: {0}".format(provider_selection))
        return provider_selection
