from django.db import models
from uuidfield import UUIDField
import jsonfield

# importing the admin models
from django.contrib.auth.models import User
from django.contrib.auth.models import Group


def signature_file_upload(instance, filename):
    return 'signature_file/' + filename


class UserProfile(models.Model):
    user = models.OneToOneField(User, related_name='profile')
    guid = UUIDField(auto=True)
    access_key = UUIDField(auto=True)
    signature_file = models.FileField(upload_to=signature_file_upload, null=True, blank=True)

    class Meta:
        db_table = 'user_profiles'

    def __unicode__(self):
        return str(self.user.first_name + ' ' + self.user.last_name)


# save add user profile while save user

def add_user_profile(sender, instance, **kwargs):
    try:
        if instance.id == 1:
            group, created = Group.objects.get_or_create(name='admin')
            group.user_set.add(instance)

        UserProfile.objects.get_or_create(user=instance)
    except Exception as e:
        pass


models.signals.post_save.connect(add_user_profile, sender=User)


class Config(models.Model):
    key = models.CharField(max_length=128, unique=True)
    value = jsonfield.JSONField(blank=True, null=True)

    # want_local_server = models.BooleanField(default=False)
    # local_host = models.CharField(max_length=100, unique=True,blank=True,null=True)
    # local_port = models.CharField(max_length=50,unique=True,blank=True,null=True)
    # local_aet = models.CharField(max_length=100,unique=True,blank=True,null=True)
    #
    # compress = models.BooleanField(default=False)

    class Meta:
        db_table = 'configs'

    def __unicode__(self):
        return self.key


class Host(models.Model):
    name = models.CharField(max_length=255)
    port = models.IntegerField()
    ae_title = models.CharField(max_length=255)
    ip_address = models.CharField(max_length=15)

    class Meta:
        db_table = 'host_info'

    def __unicode__(self):
        return self.key

class ReportModel(models.Model):
    procedure = models.TextField(null=True, blank=True)
    clinical_information = models.TextField(null=True, blank=True)
    comparison = models.TextField(null=True, blank=True)
    findings = models.TextField(null=True, blank=True)
    impression = models.TextField(null=True, blank=True)
    report_name = models.CharField(max_length=200)

    def __unicode__(self):
        return str(self.report_name)

    class Meta:
        db_table = "report"

class ReportJsonModel(models.Model):
    order_guid = UUIDField(auto=False)
    procedure = models.TextField(null=True, blank=True)
    clinical_information = models.TextField(null=True, blank=True)
    comparison = models.TextField(null=True, blank=True)
    findings = models.TextField(null=True, blank=True)
    impression = models.TextField(null=True, blank=True)

    def __unicode__(self):
        return str(self.order_guid)

    class Meta:
        db_table = "report_json"

class Radiologist(models.Model):
    user = models.OneToOneField(User, related_name="radiologist")
    title = models.CharField(max_length=25, null=True, blank=True)
    mobile = models.CharField(max_length=50, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(arg):
        return str(self.name)

    class Meta:
        db_table = "radiologist"

class Technician(models.Model):
    user = models.OneToOneField(User, related_name="technician")
    title = models.CharField(max_length=25, null=True, blank=True)
    mobile = models.CharField(max_length=50, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(arg):
        return str(self.name)

    class Meta:
        db_table = "technician"


class ScheduleTask(models.Model):
    task_type = models.CharField(max_length=100, null=False, blank=False, unique=True)
    last_download_time = models.DateTimeField(null=True, blank=True, default=None)

    def __unicode__(self):
        return str(self.task_type)

    class Meta:
        db_table = "schedule_task"
