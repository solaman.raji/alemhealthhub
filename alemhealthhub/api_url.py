from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns

from alemhealthhub.api import (
    CheckExistence,
    BoxSetup,
    UserAuth,
    Members,
    ConfigView,
    System,
    Hosts,
    ReportView,
    ReportJsonView,
    HubUserView,
    CheckHost,
    HomeView,
    HubOnlineStatus,
    ProviderList,
    SettingsDetail,
)

urlpatterns = [
    # db exists or not
    url(r'^api/check_existence', CheckExistence.as_view()),

    # box setup
    url(r'^api/boxsetup$', BoxSetup.as_view()),

    # logging in
    url(r'^api/getting_in$', UserAuth.as_view()),

    # user get
    url(r'^api/user/(?P<pk>[0-9]+)/$', Members.as_view()),

    # user set
    url(r'^api/user$', Members.as_view()),

    # config get/put
    url(r'^api/config$', ConfigView.as_view()),

    # reboot
    url(r'^api/reboot', System.as_view()),

    # host
    url(r'^api/host', Hosts.as_view()),

    # report
    url(r'^api/reporttemplate', ReportView.as_view()),

    url(r'^api/report_json$', ReportJsonView.as_view()),

    url(r'^api/report_json/(?P<order_guid>\w+)$', ReportJsonView.as_view()),

    # report
    url(r'^api/hub_user', HubUserView.as_view()),

    # check_host
    url(r'^api/check_host/(?P<host_id>\w+)$', CheckHost.as_view()),

    # home page
    url(r'^api/home/(?P<type>[0-9]+)/$', HomeView.as_view()),

    url(r'^docs/', include('rest_framework_docs.urls')),

    url(r'^api/online-status', HubOnlineStatus.as_view()),

    url(r'^api/providers/?$', ProviderList.as_view()),
    url(r'^api/settings/?$', SettingsDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
