module.exports = function (grunt) {
    grunt.initConfig({
        concat: {
            dashboard_js: {
                src: [

                    'static/client/app/common/filters/commonfilter.js',
                    'static/bower_components/jquery/dist/jquery.js',
                    'static/bower_components/bootstrap/dist/js/bootstrap.min.js',
                    'static/bower_components/angular/angular.min.js',
                    'static/bower_components/angular-resource/angular-resource.min.js',
                    'static/bower_components/angular-ui-router/release/angular-ui-router.js',
                    'static/bower_components/angular-cookies/angular-cookies.min.js',
                    'static/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
                    'static/bower_components/angular-ui-select/dist/select.min.js',

                    'static/bower_components/angular-animate/angular-animate.min.js',
                    'static/bower_components/angular-sanitize/angular-sanitize.min.js',
                    'static/bower_components/ngtoast/dist/ngToast.js',


                    'static/bower_components/angular-google-chart/ng-google-chart.js',
                    'static/bower_components/textAngular/dist/textAngular-rangy.min.js',
                    'static/bower_components/textAngular/dist/textAngular-sanitize.min.js',
                    'static/bower_components/textAngular/dist/textAngular.min.js',
                    'static/bower_components/moment/min/moment.min.js',
                    'static/bower_components/select2/dist/js/select2.js',

                    'static/bower_components/lodash/dist/lodash.js',
                    'static/bower_components/angularjs-dropdown-multiselect/src/angularjs-dropdown-multiselect.js',

                    'static/bower_components/textAngular/dist/textAngular-rangy.min.js',
                    'static/bower_components/textAngular/dist/textAngular-sanitize.min.js',
                    'static/bower_components/textAngular/dist/textAngular.min.js',

                    'static/bower_components/angular-xeditable/dist/js/xeditable.js',
                    'static/client/app/libs/*.js',

                    'static/client/app/app.js',
                    'static/client/app/modules/*/app.js',
                    'static/client/app/modules/*/*.js',
                    'static/client/app/common/*/*.js',
                    'static/client/app/common/*.js',

                    'static/dashboard/js/main.js'



                ],
                dest: 'static/assets/build/script.js'
            },
            dashboard_css: {
                src: [
                    'static/bower_components/bootstrap/dist/css/bootstrap.min.css',
                    'static/bower_components/font-awesome/css/font-awesome.min.css',
                    'static/bower_components/textAngular/src/textAngular.css',
                    'static/bower_components/ngtoast/dist/ngToast.css',
                    'static/bower_components/select2/dist/css/select2.css',
                    'static/bower_components/angular-ui-select/dist/select.min.css',

                    'static/bower_components/textAngular/dist/textAngular.css',
                    'static/bower_components/angular-xeditable/dist/css/xeditable.css',
                    'static/dashboard/css/main.css',
                    'static/dashboard/css/style.css',
                    'static/dashboard/css/responsive.css',

                ],
                dest: 'static/assets/build/style.css'
            }


        },
        watch: {
            files: [
                'static/client/app/app.js',
                'static/client/app/modules/*/app.js',
                'static/client/app/modules/*/*.js',
                'static/client/app/common/*/*.js',
                'static/client/app/libs/*.js',
                'static/dashboard/css/ah-admin.css',
                'static/new-dashboard/css/dicom-viewer.css',
                'static/dashboard/css/main.css',
                'static/dashboard/css/style.css',
                'static/dashboard/css/responsive.css',
                'static/dashboard/js/main.js'

            ],
            tasks: ['dev:alemhealth']
        }
    });
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    //register tasks
    grunt.registerTask('dev:alemhealth',
        [
            'concat:dashboard_js',
            'concat:dashboard_css'
        ]);


    grunt.registerTask('dev', ['dev:alemhealth']);

};
