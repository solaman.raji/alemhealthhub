'use strict';

angular.module('CommonApp.filters',[])
    
    

.filter('meta_status',function() {

    return function(key) {

        var meta_status = {
            1 : 'Report Pending',
            2 : 'Report Syncing',
            3 : 'Report Send'
        };


        return meta_status[key]
    }

})


.filter('momentCalander', function() {
  return function(input) {
    date = moment(input).format('D MMM YYYY h:m A');
    return date;
  };
})

.filter('AppStorage', function() {
  return function(input) {

    var use_s3 = $("#use_s3").val();
    var bucket = $("#AWS_STORAGE_BUCKET_NAME").val();
    if( use_s3 == 'False' ){
        var media_url = '/media/';
    }else{
        media_url = 'https://s3-ap-southeast-1.amazonaws.com/' + bucket + '/media/';
    }
    return media_url + input
  };
})

;