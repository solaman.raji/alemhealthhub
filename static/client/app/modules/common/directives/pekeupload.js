'use restrict';
angular.module('CommonApp.directives').directive('pekeupload', function($parse, $cookieStore){
    return {
        restrict: 'A',
        require : 'ngModel',
        scope: {
            text  : '@peke',
            url  : '@url',
            single : '@single',
            obj : '@obj',
            maxlength : '@maxlength'

        },
        link: function (scope, element, attrs, ctrl) {
            $(element).pekeUpload({
                theme:'bootstrap',
                allowedExtensions:"jpeg|jpg|png|gif|csv|dcm",
                url : scope.url,
                btnText : scope.text,
                data : JSON.stringify({ "access_key" : $cookieStore.get('user').access_key }),
                onFileSuccess: function(file, data){
                    scope.$apply(function() {
                        if(scope.single == 'false'){

                            var images = ctrl.$modelValue;

                            if ( scope.maxlength && images.length < parseInt(scope.maxlength)){
                                images.push({id: data.id, filename: data.filename});
                                ctrl.$setViewValue(images);
                            }else if(scope.maxlength == undefined){
                                images.push({id: data.id, filename: data.filename});
                                ctrl.$setViewValue(images);
                            }
                        }
                          else{
                            if(scope.obj == 'true'){
                                var image = ctrl.$modelValue;

                                ctrl.$setViewValue({
                                    guid: data.guid,
                                    id: data.id,
                                    file: data.file,
                                    //metas: image.metas
                                });
                            }
                            else{
                                var image = data.file;
                                ctrl.$setViewValue(image);
                            }


                        }

                    });
                    $('.file').css('display', 'none')

                },
                onFileError: function(error){

                    console.log(scope)
                }

            });
        }
    }
});