angular.module('CommonApp.directives').directive('flatDatePicker', function() {
    return {
        restrict : "A",
        require : 'ngModel',
        //replace : true,
        transclude : true,
        //template : '<div><textarea></textarea></div>',

        scope: {
            onSelect: "&onSelect"
        },
        link : function(scope, element, attrs, ctrl) {

          var datepickerSelector = element;
            datepickerSelector.datepicker({
              showOtherMonths: true,
              selectOtherMonths: true,
              dateFormat: "dd MM, yy",
              yearRange: '-1:+1',
              onSelect: function(dateText){
                  scope.$apply(function(){
                    ctrl.$setViewValue(dateText);
                    scope.onSelect();
                  });

              }

            }).prev('.btn').on('click', function (e) {
              e && e.preventDefault();
              datepickerSelector.focus();
            });

        }
    };
});