angular.module('CommonApp.services',[])

.service('CheckDBService',function($resource) {
     return $resource('api/check_existence', {}, {
            query : { method: 'GET' },
            update : { method: 'PUT' },
            create:{method : 'POST'}
        }
    );
})