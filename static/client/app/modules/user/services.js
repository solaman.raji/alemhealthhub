'use strict';

angular.module('UserApp.services')

.factory('AuthService', function($resource){
    return $resource('api/getting_in', {}, {
            get_in : {
                method : 'GET'
            }
        }
    );
})

.factory('HomeService', function($resource){
    return $resource('api/home/:type', {type: '@type'}, {
            get : {
                method : 'GET'
            }
        }
    );
})

.factory('AddUserService', function($resource){
    return $resource('api/hub_user/', {}, {
            save : {
                method : 'POST'
            }
        }
    );
})



.factory('UserinfoService', function($resource) {
    return $resource('api/user/:pk',{pk:'@pk'},{
        query : {
            method : 'GET'
        },
        save : {
            method : 'PUT'
        }
    })
})




.factory('ConfigService', function($resource) {
    return $resource('api/config',{},{
        query : {
            method : 'GET'
        },
        save : {
            method : 'PUT'
        }
    })
})


.factory('RebootService', function($resource) {
    return $resource('api/reboot',{},{
        query : {
            method : 'GET'
        }
    })
})


.factory('ReportHeaderService', function($resource) {
    return $resource('api/reportheader/',{},{
        query : {
            method : 'GET'
        }
    })
})

.factory('HostService', function($resource) {
    return $resource('api/host', {}, {
        query: {
            method: 'GET'
        },
        create: {
            method: 'POST'
        },
        save: {
            method: 'PUT'
        },
        remove: {
            method: 'DELETE'
        }
    });
})


.service('ModalService', ['$modal', function ($modal) {
    var templateDir = '/static/client/app';
    var modalDefaults = {
        backdrop: true,
        keyboard: true,
        modalFade: true,
        templateUrl: templateDir + '/templates/main_app/user/modal.html'
    };
    var modalOptions = {
        closeButtonText: 'Close',
        actionButtonText: 'OK',
        headerText: 'Proceed?',
        bodyText: 'Perform this action?'
    };

    this.showModal = function (customModalDefaults, customModalOptions) {
        if (!customModalDefaults) customModalDefaults = {};
        customModalDefaults.backdrop = 'static';
        return this.show(customModalDefaults, customModalOptions);
    };

    this.show = function (customModalDefaults, customModalOptions) {
        //Create temp objects to work with since we're in a singleton service
        var tempModalDefaults = {};
        var tempModalOptions = {};

        //Map angular-ui modal custom defaults to modal defaults defined in service
        angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

        //Map modal.html $scope custom properties to defaults defined in service
        angular.extend(tempModalOptions, modalOptions, customModalOptions);

        if (!tempModalDefaults.controller) {
            tempModalDefaults.controller = function ($scope, $modalInstance) {
                $scope.modalOptions = tempModalOptions;
                $scope.modalOptions.ok = function (result) {
                    $modalInstance.close(result);
                };
                $scope.modalOptions.close = function (result) {
                    $modalInstance.dismiss('cancel');
                };
            }
        }

        return $modal.open(tempModalDefaults).result;
    };
}]);
