'use strict';

angular.module('UserApp')

    .config(function ($stateProvider) {
        var templateDir = '/static/client/app';

        $stateProvider

            .state('login', {
                url: '/login',
                templateUrl: templateDir + '/templates/main_app/user/login.html',
                controller: 'loginCtrl'
            })

            .state('logout', {
                url: '/logout',
                controller: 'logutCtrl'
            })

            .state('dashboard.home', {
                url: "/dashboard",
                views: {
                    'dashboard-view': {
                        templateUrl: templateDir + "/templates/main_app/user/home.html"
                    }
                }
            })

            .state('dashboard.usersettings', {
                url: "/usersettings",
                views: {
                    'dashboard-view': {
                        templateUrl: templateDir + "/templates/main_app/user/usersettings.html",
                        controller : 'UserSettingsCtrl'
                    }
                }
            })

            .state('dashboard.boxsettings', {
                url: "/boxsettings",
                views: {
                    'dashboard-view': {
                        templateUrl: templateDir + "/templates/main_app/user/boxsettings.html",
                        controller : 'BoxSettingsCtrl'
                    }
                }
            })
            .state('dashboard.hostsettings', {
                url: "/hostsettings",
                views: {
                    'dashboard-view': {
                        templateUrl: templateDir + "/templates/main_app/user/hosts.html",
                        controller : 'HostSettingsCtrl'
                    }
                }
            })
            .state('dashboard.adduser', {
                url: "/adduser",
                views: {
                    'dashboard-view': {
                        templateUrl: templateDir + "/templates/main_app/user/add-user.html",
                        controller : 'AddUserCtrl'
                    }
                }
            })
    })

    .run(function ($cookies) {
        //debugger;
        console.log("user")

    })

;
angular.module('UserApp.services', []);
angular.module('UserApp.controllers', []);
angular.module('UserApp.directives', []);
