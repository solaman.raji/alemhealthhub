'use strict';

angular.module('UserApp.controllers')

    .controller('homeCtrl', function ($scope, HomeService) {
        console.log("Home controller is here");

        // this is totalOrderChart
        $scope.totalOrderChart = {};

        $scope.totalOrderChart.data = {
            "cols": [
                {id: "t", label: "Date", type: "string"},
                {id: "s", label: "Orders", type: "number"}
            ]
        };

        $scope.totalOrderChart.type = 'ColumnChart'
        $scope.totalOrderChart.options = {
            'title': 'Total order count'
        }

        $scope.report_date = {startDate: null, endDate: null};

        $scope.$watch('report_date', function(data){
            $scope.totalOrderChart.data['rows'] = [];
            try {
                HomeService.get({type: 1}, function(res){
                    console.log(res);
                    angular.forEach(res['orders'], function (key) {
                        $scope.totalOrderChart.data.rows.push(
                            {
                                c: [
                                    {v: key[1]},
                                    {v: key[0]},
                                ]
                            }
                        );
                    });
                    console.log($scope.totalOrderChart.data.rows);
                })
            } catch (e) {
                console.log(e);
            }
        });


        // pie chart for modality
        $scope.chartObject = {};

        $scope.chartObject.data = {
            "cols": [
                {id: "t", label: "Topping", type: "string"},
                {id: "s", label: "Slices", type: "number"}
            ]
        };

        $scope.chartObject.type = 'PieChart';
        $scope.chartObject.cssStyle = "height:500px; width:100%;";
        $scope.chartObject.options = {
            'title': 'Modalities by percentage'
        };
        $scope.chartObject.data["rows"] = [];
        try {
            HomeService.get({type: 2}, function(res){
                console.log("pie >>>", res);
                angular.forEach(res['orders'], function (key) {
                    $scope.chartObject.data.rows.push(
                        {
                            c: [
                                {v: key[1]},
                                {v: key[0]}
                            ]
                        }
                    );
                });
            });
        } catch (e) {
            console.log(e);
        }

        // pie chart for meta status
        $scope.meata_status = {};

        $scope.meata_status.data = {
            "cols": [
                {id: "t", label: "Topping", type: "string"},
                {id: "s", label: "Slices", type: "number"}
            ]
        };

        $scope.meata_status.type = 'PieChart';
        $scope.meata_status.cssStyle = "height:500px; width:100%;";
        $scope.meata_status.options = {
            'title': 'Report Complete Percentage'
        };
        $scope.meata_status.data["rows"] = [];
        try {
            HomeService.get({type: 3}, function(res){
                console.log("pie >>>", res);
                angular.forEach(res['orders'], function (key) {
                    if(key[1] == 5){
                        $scope.meata_status.data.rows.push(
                            {
                                c: [
                                    {v: "Image Recieved"},
                                    {v: key[0]}
                                ]
                            }
                        );
                    }
                    if(key[1] == 4){
                        $scope.meata_status.data.rows.push(
                            {
                                c: [
                                    {v: "Report Created"},
                                    {v: key[0]}
                                ]
                            }
                        );
                    }
                    if(key[1] == 3){
                        $scope.meata_status.data.rows.push(
                            {
                                c: [
                                    {v: "Report Sent"},
                                    {v: key[0]}
                                ]
                            }
                        );
                    }
                    if(key[1] == 2){
                        $scope.meata_status.data.rows.push(
                            {
                                c: [
                                    {v: "Report Syncing"},
                                    {v: key[0]}
                                ]
                            }
                        );
                    }
                    if(key[1] == 1){
                        $scope.meata_status.data.rows.push(
                            {
                                c: [
                                    {v: "Report Pending"},
                                    {v: key[0]}
                                ]
                            }
                        );
                    }
                });
            });
        } catch (e) {
            console.log(e);
        }
    })

    .controller('AddUserCtrl', function($scope, AddUserService, ngToast){
        $scope.add_user = function(){
            if ($scope.stuff.password != $scope.stuff.rep_password) {
                ngToast.create({
                    className: 'danger',
                    content: "Password Doesn't Match"
                });
            }

            console.log($scope.stuff);
            try {
                if (Object.keys($scope.stuff).length != 7) {
                    throw 'Please fill up the form fileds properly'
                }
                else {
                    angular.forEach($scope.stuff, function (value, key) {
                        if ($.trim(value) == '') {
                            throw 'Form Field is Empty';
                        }
                    })
                }
                AddUserService.save($scope.stuff, function(res){
                    console.log(res)
                    if(res.success != undefined){
                        ngToast.create({
                            className: 'success',
                            content: res.success,
                            dismissOnTimeout: true,
                            timeout: 1500
                        });
                    }
                    else {
                        ngToast.create({
                            className: 'danger',
                            content: "Cannot create user",
                            dismissOnTimeout: true,
                            timeout: 1500
                        });
                    }
                })

            } catch (e) {
                console.log(e)
            }
        }

    })

    .controller('loginCtrl', function ($rootScope, $scope, $state, $cookies, ngToast, AuthService) {
        if ($cookies.get('user') != undefined) {
            $state.transitionTo('dashboard')
        }

        $scope.config = {};

        $scope.log_in = function () {

            try {
                // simple form validation
                if (Object.keys($scope.config).length != 2) {
                    throw 'Please fill up the form fileds properly'
                }
                else {
                    angular.forEach($scope.config, function (value, key) {
                        if ($.trim(value) == '') {
                            throw 'Form Field is Empty';
                        }
                    })
                }

                AuthService.get_in({username: $scope.config.username, password: $scope.config.password},
                    function success(data) {
                        if (data.error != undefined) {
                            ngToast.create({
                                className: 'danger',
                                content: data.error,
                                dismissOnTimeout: true,
                                timeout: 1500
                            });
                        }
                        else {
                            $rootScope.$emit('direct_login', data);
                        }
                    },
                    function err(error) {
                        console.log(error);
                    }
                );
            }
            catch (e) {
                ngToast.create({
                    className: 'warning',
                    dismissOnTimeout: true,
                    timeout: 1500,
                    content: e
                });
            }

        }


    })


    .controller('logutCtrl', function ($state, $cookies) {
        if ($cookies.get('user') != undefined) {
            $cookies.remove('user');
        }

        $state.transitionTo('login');

    })


    .controller('UserSettingsCtrl', function ($scope, ngToast, $cookies, $timeout, UserinfoService) {

        // current user id
        var pk = JSON.parse($cookies.get('user')).pk;

        // user info
        $scope.user = {};

        $timeout(function () {
            try {
                UserinfoService.query({pk: pk}, function (res) {
                    $scope.user = res.user;
                }, function (err) {
                    console.log(err);
                })
            } catch (e) {
                console.trace(e);
            }
        }, 0);


        // save changes
        $scope.user_save = function () {

            if ($scope.user.password != $scope.user.rep_password) {
                ngToast.create({
                    className: 'danger',
                    content: "Password Doesn't Match"
                });
            }
            else {

                UserinfoService.save($scope.user, function (res) {

                    if (res.success != undefined) {
                        ngToast.create({
                            className: 'success',
                            content: res.success
                        });
                    }
                    else {
                        ngToast.create({
                            className: 'danger',
                            content: res.error
                        })
                    }

                }, function (err) {
                    console.trace(err);
                })

            }

        }

    })


    .controller('BoxSettingsCtrl', function ($scope, ngToast, $state, $timeout, ConfigService,RebootService, FileUploader, $cookieStore, $stateParams, ReportHeaderService) {

        try {
            ReportHeaderService.query({}, function(res){
                if(res.success != undefined){
                    console.log(res);
                    $scope.header_file = res.header_file.header_file;
                }
            })

        } catch (e) {
            console.log(e)
        }

        $scope.config = {};

        $timeout(function () {

            ConfigService.query({}, function (res) {

                if (res['success'] != undefined) {
                    var packet = res['success'];

                    angular.forEach(packet, function (obj, key) {
                        $scope.config[obj.key] = obj.value;
                    });

                    $scope.cd_burner_check = $scope.config.want_local_server == 'True' ? true : false;
                }


            }, function (err) {
                console.trace(err);
            })

        }, 0);


        // want compression or not
        $scope.compress_checking = function () {

            if ($scope.compress_check) {
                $scope.config.compress = 'True';
            }
            else {
                $scope.config.compress = 'False';
            }

        };


        // want cd burner or not
        $scope.cd_burner_checking = function () {
            if ($scope.cd_burner_check) {
                $scope.config.want_local_server = 'True';
            }
            else {
                $scope.config.want_local_server = 'False';
            }
        };


        // network balance check
        $scope.check_mul = function () {
            if ($scope.mul) {
                $scope.config.nw_balance_mode = 'multi';
            }
        };


        // shut down
        $scope.shut_down = function() {
            $scope.shutdown_flag = true;

            RebootService.query({reboot:1},function(res) {},function (err) {
                console.log(err);
            })
        };


        $scope.check_sol = function () {
            if ($scope.sol) {
                $scope.config.nw_balance_mode = 'solo';
            }
        };

        $scope.save_config = function () {

            if ($scope.config.want_local_server == 'True') {
                try {
                    if (($scope.config.local_host != undefined) && ($scope.config.local_port != undefined) && ($scope.config.local_aet != undefined)) {
                        if (($.trim($scope.config.local_host) != '') && ($.trim($scope.config.local_port) != '') && ($.trim($scope.config.local_aet) != '')) {

                            ngToast.create({
                                className: 'info',
                                content: "updating"
                            });

                            ConfigService.save(
                                {'config': $scope.config},
                                function (res) {
                                    if (res.success != undefined) {
                                        ngToast.create({
                                            className: 'success',
                                            content: res["success"]
                                        });
                                        $state.transitionTo('dashboard.studies');
                                    }
                                    else {
                                        ngToast.create({
                                            className: 'danger',
                                            content: res["error"]
                                        })
                                    }

                                },
                                function (err) {
                                    console.log(err)
                                }
                            )
                        }
                        else {
                            throw "CD server cridentials are needed";
                        }
                    }
                    else {
                        throw "CD server cridentials are needed";
                    }
                } catch (e) {
                    ngToast.create(
                        {
                            className: 'danger',
                            content: e
                        }
                    );
                }

            }
            else {
                ngToast.create({
                    className: 'info',
                    content: "updating"
                });

                ConfigService.save(
                    {'config': $scope.config},
                    function (res) {
                        if (res.success != undefined) {
                            ngToast.create({
                                className: 'success',
                                content: res["success"]
                            });
                            $state.transitionTo('dashboard.studies');
                        }
                        else {
                            ngToast.create({
                                className: 'danger',
                                content: res["error"]
                            })
                        }

                    },
                    function (err) {
                        console.log(err)
                    }
                )
            }

        }
        $scope.token = $('input[name=csrfmiddlewaretoken]').val();
        console.log($scope.token);

        $scope.header_image_uploader = new FileUploader();
        $scope.header_upload_options = {
            url: '/api/reportheader/',
            headers: {
                'X-CSRFToken': $scope.token,
                'Authentication': $cookieStore.get('user') ? $cookieStore.get('user').access_key : ''

            },
            formData: [],
            autoUpload: true
        };

        $scope.header_image_uploader.onAfterAddingFile = function(){
            $scope.header_image_uploader.uploadAll();
        };

        $scope.header_image_uploader.onCompleteItem = function (item, response) {
            response = JSON.parse(response);
            var index = $scope.header_image_uploader.getIndexOfItem(item);
            $scope.header_image_uploader.queue[index].data = response;
            console.log(item);
            // console.log(JSON.parse(response));

            $scope.header_file = response.header_file;
        };
    })
    .controller('HostSettingsCtrl', function ($scope, $filter, ngToast, HostService, ModalService) {
        $scope.users = [];

        try {
            HostService.query(function(res) {
                if(res.success != undefined && res.success.length > 0){
                    $scope.users = res.success;
                }
                else if(res.error != undefined)
                    console.log(res);
            }, function(err) {
                console.log(err);
            });
        }
        catch (e) {
            console.log(e);
        }

        $scope.add_host = function(){
            if($scope.host != undefined && $scope.host != null) {

                //cheking whether user has insertrd all data
                if(Object.keys($scope.host).length != 4) {
                    ngToast.create({
                        className: 'danger',
                        content: "All fields are required"
                    });
                    return;
                }

                //checking the port value is numeric or not
                if(isNaN($scope.host.port) || ($scope.host.port < 1) || ($scope.host.port > 65535)){
                    ngToast.create({
                        className: 'danger',
                        content: "Port should between 1 - 65535"
                    });
                    return;
                }

                //checking the ip address is valid or not
                if (! /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test($scope.host.ip_address)) {
                    ngToast.create({
                        className: 'danger',
                        content: "Please enter a valid IP address"
                    });
                    return;
                }

                //if all good then make a request
                try {
                    HostService.create($scope.host, function(res) {
                        if(res.success != undefined){
                            $scope.users.push(res.host);
                        }
                        else if(res.error != undefined)
                            ngToast.create({
                                className: res.error,
                                content: "Some error occured"
                            });
                    }, function(err) {
                        console.log(err);
                    });
                }
                catch (e) {
                    console.log(e);
                }

            }
            $scope.host = {};
        };

        $scope.saveUser = function (user) {
            try {
                HostService.save(user, function(res) {
                    if(res.success != undefined){
                        ngToast.create({
                            className: "success",
                            content: res.success
                        });
                    }
                    else if(res.error != undefined) {
                        ngToast.create({
                            className: "danger",
                            content: res.error
                        });
                    }
                }, function(err) {
                    console.log(err);
                });
            }
            catch (e) {
                console.log(e);
            }

        };

        $scope.checkName = function (name) {
            if(name == "") return  "Please enter value for the name field ";
        };

        $scope.checkPort = function (port) {
            if(isNaN(port) || (port < 1) || (port > 65535)) return "Port should between 1 - 65535";
        };

        $scope.checkAe_title = function (ae_title) {
            if(ae_title == "") return  "Please enter value for the AE Title field ";
        };
        $scope.checkIp_address = function (ip_address) {
            if(! /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ip_address))
                return "Please insert a valid ip address";
        };

        var modalOptions = {
            closeButtonText: 'Cancel',
            actionButtonText: 'Delete Host',
            headerText: 'Delete this host',
            bodyText: 'Are you sure you want to delete this host?'
        };

        $scope.removeHost = function (id) {
            var self = this;
            ModalService.showModal({}, modalOptions).then(function (result) {
                HostService.remove({id: id}, function (res) {
                    if (res.success != undefined) {
                        $scope.users.splice(self.$index, 1);
                        ngToast.create({
                            className: "success",
                            content: res.success
                        });
                    }
                    else if (res.error != undefined) {
                        ngToast.create({
                            className: "danger",
                            content: res.success
                        });
                    }
                }, function (err) {
                    console.log(err);
                });
            });
        };
    });
