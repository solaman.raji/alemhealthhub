function loadStudyDicomView(studyViewer, data, $filter){
    // Load the first series into the viewport
    $('#wadoURL').val();

    console.log("called loadStudyJson");
    var dicomList = [];
    var currentStackIndex = 0;
    var seriesIndex = 0;
    var dicomStack = {
        seriesDescription: data.metas.SeriesDescription,
        stackId : data.id,
        imageIds: [],
        seriesIndex : seriesIndex,
        currentImageIdIndex: 0,
        frameRate: data.frameRate
    }
    if(data.numberOfFrames !== undefined) {
        var numberOfFrames = data.numberOfFrames;
        for(var i=0; i < numberOfFrames; i++) {
            var imageId = data.dicoms[0].file + "?frame=" + i;
            if(imageId.substr(0, 4) !== 'http') {
                //imageId = "dicomweb://cornerstonetech.org/images/ClearCanvas/" + imageId;
                imageId = "dicomweb:" + $filter('AppStorage')(imageId);
            }
            dicomStack.imageIds.push(imageId);
        }
    } else {
        data.dicoms.forEach(function(image) {
            var imageId = image.file;
            if(imageId.substr(0, 4) !== 'http') {
                //imageId = "dicomweb://cornerstonetech.org/images/ClearCanvas/" + image.imageId;
                imageId = "dicomweb:" + $filter('AppStorage')(imageId);
            }
            dicomStack.imageIds.push(imageId);
        });
    }
    console.log(data);
    
    
    // resize the parent div of the viewport to fit the screen
    var imageViewer = $(studyViewer).find('.imageViewer')[0];
    var viewportWrapper = $(imageViewer).find('.viewportWrapper')[0];
    var parentDiv = $(studyViewer).find('.viewer')[0];
    viewportWrapper.style.width = (parentDiv.style.width - 60) + "px";
    viewportWrapper.style.height= (window.innerHeight - 150) + "px";

    var studyRow = $(studyViewer).find('.studyRow')[0];
    var width = $(studyRow).width();
    $(parentDiv).width(width);
    viewportWrapper.style.width = (parentDiv.style.width - 60) + "px";
    viewportWrapper.style.height= (window.innerHeight - 150) + "px";

    // image enable the dicomImage element and activate a few tools
    var element = $(studyViewer).find('.viewport')[0];
    var parent = $(element).parent();
    var childDivs = $(parent).find('.overlay');
    var topLeft = $(childDivs[0]).find('div');
    $(topLeft[0]).text(data.patient.name);
    $(topLeft[1]).text(data.patient.patient_id);
    //var topRight= $(childDivs[1]).find('div');
    $(topLeft[2]).text(dicomStack.seriesDescription);
    $(topLeft[3]).text(data.metas.StudyDate);
    //var bottomLeft = $(childDivs[2]).find('div');
    //var bottomRight = $(childDivs[3]).find('div');

    function onNewImage(e, param) {
        $('#loading').show();
        // if we are currently playing a clip then update the FPS
        var playClipToolData = cornerstoneTools.getToolState(element, 'playClip');
        if(playClipToolData !== undefined && playClipToolData.data.length > 0 && playClipToolData.data[0].intervalId !== undefined && param.frameRate !== undefined) {
            $(topLeft[4]).text("FPS: " + Math.round(param.frameRate));
        } else {
            if($(topLeft[4]).text().length > 0) {
                $(topLeft[4]).text("");
            }
        }
        $(topLeft[6]).text("Image #" + (dicomStack.currentImageIdIndex + 1) + "/" + dicomStack.imageIds.length);
    }
    $(element).on("CornerstoneNewImage", onNewImage);

    function onImageRendered(e, param) {
        //debugger
        $('#loading').hide();
        $(topLeft[7]).text("Zoom:" + param.viewport.scale.toFixed(2));
        $(topLeft[8]).text("WW/WL:" + Math.round(param.viewport.voi.windowWidth) + "/" + Math.round(param.viewport.voi.windowCenter));
        $(topLeft[5]).text("Render Time:" + param.renderTimeInMs + " ms");
    }
    //element.addEventListener("CornerstoneImageRendered", onImageRendered, false);
    $(element).on("CornerstoneImageRendered", onImageRendered);

    var imageId = dicomStack.imageIds[0];
    // image enable the dicomImage element
    cornerstone.enable(element);
    cornerstone.loadAndCacheImage(imageId).then(function(image) {
        cornerstone.displayImage(element, image);
        if(dicomStack.frameRate !== undefined) {
            cornerstone.playClip(element, dicomStack.frameRate);
        }

        cornerstoneTools.mouseInput.enable(element);
        cornerstoneTools.mouseWheelInput.enable(element);
        cornerstoneTools.touchInput.enable(element);

        // Enable all tools we want to use with this element
        cornerstoneTools.wwwc.activate(element, 1); // ww/wc is the default tool for left mouse button
        cornerstoneTools.pan.activate(element, 2); // pan is the default tool for middle mouse button
        cornerstoneTools.zoom.activate(element, 4); // zoom is the default tool for right mouse button
        cornerstoneTools.probe.enable(element);
        cornerstoneTools.length.enable(element);
        cornerstoneTools.ellipticalRoi.enable(element);
        cornerstoneTools.rectangleRoi.enable(element);
        cornerstoneTools.wwwcTouchDrag.activate(element);
        cornerstoneTools.zoomTouchPinch.activate(element);

        // stack tools
        cornerstoneTools.addStackStateManager(element, ['playClip']);
        cornerstoneTools.addToolState(element, 'stack', dicomStack);
        cornerstoneTools.stackScrollWheel.activate(element);
        cornerstoneTools.stackPrefetch.enable(element);


        function disableAllTools()
        {
            cornerstoneTools.wwwc.disable(element);
            cornerstoneTools.pan.activate(element, 2); // 2 is middle mouse button
            cornerstoneTools.zoom.activate(element, 4); // 4 is right mouse button
            cornerstoneTools.probe.deactivate(element, 1);
            cornerstoneTools.length.deactivate(element, 1);
            cornerstoneTools.ellipticalRoi.deactivate(element, 1);
            cornerstoneTools.rectangleRoi.deactivate(element, 1);
            cornerstoneTools.stackScroll.deactivate(element, 1);
            cornerstoneTools.wwwcTouchDrag.deactivate(element);
            cornerstoneTools.zoomTouchDrag.deactivate(element);
            cornerstoneTools.panTouchDrag.deactivate(element);
            cornerstoneTools.stackScrollTouchDrag.deactivate(element);
        }

        var buttons = $(studyViewer).find('button');
        // Tool button event handlers that set the new active tool
        $(buttons[0]).on('click touchstart',function() {
            disableAllTools();
            cornerstoneTools.wwwc.activate(element, 1);
            cornerstoneTools.wwwcTouchDrag.activate(element);
        });
        $(buttons[1]).on('click touchstart',function() {
            disableAllTools();
            var viewport = cornerstone.getViewport(element);
            if(viewport.invert === true) {
                viewport.invert = false;
            }
            else {
                viewport.invert = true;
            }
            cornerstone.setViewport(element, viewport);
        });
        $(buttons[2]).on('click touchstart',function() {
            disableAllTools();
            cornerstoneTools.zoom.activate(element, 5); // 5 is right mouse button and left mouse button
            cornerstoneTools.zoomTouchDrag.activate(element);
        });
        $(buttons[3]).on('click touchstart',function() {
            disableAllTools();
            cornerstoneTools.pan.activate(element, 3); // 3 is middle mouse button and left mouse button
            cornerstoneTools.panTouchDrag.activate(element);
        });
        $(buttons[4]).on('click touchstart',function() {
            disableAllTools();
            cornerstoneTools.stackScroll.activate(element, 1);
            cornerstoneTools.stackScrollTouchDrag.activate(element);
        });
        $(buttons[5]).on('click touchstart',function() {
            disableAllTools();
            cornerstoneTools.length.activate(element, 1);
        });
        $(buttons[6]).on('click touchstart',function() {
            disableAllTools();
            cornerstoneTools.probe.activate(element, 1);
        });
        $(buttons[7]).on('click touchstart',function() {
            disableAllTools();
            cornerstoneTools.ellipticalRoi.activate(element, 1);
        });
        $(buttons[8]).on('click touchstart',function() {
            disableAllTools();
            cornerstoneTools.rectangleRoi.activate(element, 1);
        });
        $(buttons[9]).on('click touchstart',function() {
            var frameRate = dicomStack.frameRate;
            if(frameRate === undefined) {
                dicomStack.frameRate = 9;
            }
            cornerstoneTools.playClip(element, dicomStack.frameRate);
        });
        $(buttons[10]).on('click touchstart',function() {
            cornerstoneTools.stopClip(element);
        });
        $(buttons[0]).tooltip();
        $(buttons[1]).tooltip();
        $(buttons[2]).tooltip();
        $(buttons[3]).tooltip();
        $(buttons[4]).tooltip();
        $(buttons[5]).tooltip();
        $(buttons[6]).tooltip();
        $(buttons[7]).tooltip();
        $(buttons[8]).tooltip();
        $(buttons[9]).tooltip();

        function resizeStudyViewer() {
            console.log('resize handled...');
            var studyRow = $(studyViewer).find('.studyRow')[0];
            var height = $(studyRow).height();
            var width = $(studyRow).width();
            $(parentDiv).width(width);
            viewportWrapper.style.width = (parentDiv.style.width - 200) + "px";
            viewportWrapper.style.height= (window.innerHeight - 100) + "px";
            cornerstone.resize(element, true);
        }
        
        $(window).resize(function() {
            resizeStudyViewer();
        });
        resizeStudyViewer();
        timeout = setTimeout(resizeStudyViewer, 300);
    });
    $(cornerstone).on("CornerstoneImageLoadProgress", function(event, oProgress){
        if (oProgress.imageId == imageId) {
            $('.dicom_percent').text(oProgress.percentComplete + '%');
        };
        
    });  
};



