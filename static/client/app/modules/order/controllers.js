'use strict';

angular.module('OrderApp.controllers')

    .controller("reportFormatUpdateCtrl", function($scope, reportFormatService, $stateParams, $state, ngToast){
        try{
            reportFormatService.query({
                report_id: $stateParams.id,
                provider_id: 10
            }, function(res){
                console.log(res.report);
                $scope.report = res.report;
            }, function(err){
                console.log(err);
            });
        } catch(e){
            console.log(e);
        };

        $scope.deleteReport = function(id) {
            console.log(id);
            // return;
            reportFormatService.delete({
                id: id
            }, function(res){
                console.log(res);
                $state.transitionTo('dashboard.reportFormat');
            }, function(err){
                console.log(err);
            });
        };

        $scope.report_update = function(report){
            console.log("The report object is ", report);
            console.log("The report object id is ", report.id);
            reportFormatService.save(report, function(res){
                console.log(res);
                ngToast.create({
                    className: 'success',
                    content:  "Report updated successfully"
                });
                $state.transitionTo('dashboard.reportFormat');
            }, function(err){
                ngToast.create({
                    className: 'danger',
                    content:  "Cannot update report"
                });
            });
        };


    })

    .controller("reportFormatCtrl", function ($scope, ngToast, reportFormatService, $stateParams, $state){
        console.log("report format page");

        $scope.bigCurrentPage = 1;
        $scope.maxSize = 20;

        $scope.loadReports = function() {
            reportFormatService.query({
                page: $scope.bigCurrentPage,
                per_page: $scope.maxSize,
                provider_id: 10
            }, function(res){
                console.log(res);
                $scope.reports = res.reports;
                $scope.bigTotalItems = res.count;
            }, function (err) {
                console.log(err);
            });
        };

    })

    .controller("addReportCtrl", function ($scope, ngToast, reportFormatService, $stateParams, $state){
        $scope.report_create = function(){
            if(typeof $scope.report.report_name == "undefined"){
                ngToast.create({
                    className: 'danger',
                    content:  "Must enter report name"
                });
                return;
            }
            if (typeof $scope.report.procedure == "undefined")
                $scope.report.procedure = ""
            if (typeof $scope.report.clinical_information == "undefined")
                $scope.report.clinical_information = ""
            if (typeof $scope.report.comparison == "undefined")
                $scope.report.comparison = ""
            if (typeof $scope.report.findings == "undefined")
                $scope.report.findings = ""
            if (typeof $scope.report.impression == "undefined")
                $scope.report.impression = ""
            // console.log($scope.report); return;
            reportFormatService.create($scope.report, function(res){
                console.log(res);
                ngToast.create({
                    className: 'success',
                    content:  "Report created successfully"
                });
                $state.transitionTo('dashboard.reportFormat');
            }, function(err){
                ngToast.create({
                    className: 'danger',
                    content:  "Cannot create report"
                });
            });
        }
    })

    .controller("hospitalStudiesCtrl", function ($scope, ngToast, $state, studyDetailService, $cookies, $stateParams, ConfigService, hospitalStudyService) {
        $scope.hospitalStudy = {};
        $scope.user_group = JSON.parse($cookies.get('user')).groups[0].name;

        var modality_list = [
            {id: 1, label: 'CT', text: "CT - Computed Tomography"},
            {id: 2, label: 'NM', text: "NM - Nuclear Medicine"},
            {id: 3, label: 'MR', text: "MR - Magnetic Resonance Imaging"},
            {id: 4, label: 'DS', text: "DS - Digital Subtraction Angiography"},
            {id: 5, label: 'DR', text: "DR"},
            {id: 6, label: 'US', text: "US - Ultrasound"},
            {id: 7, label: 'OT', text: "OT - Other"},
            {id: 8, label: 'HSG', text: "HSG - Hysterosalpingogram"},
            {id: 9, label: 'CR', text: "CR - Computed Radiography"},
            {id: 10, label: 'OP', text: "OP - Opthamology"},
            {id: 11, label: 'MG', text: "MG - Mammogram"}
        ];

        // get default provider config
        ConfigService.query({}, function success(data) {

            $scope.configs = data.configs;

            try {
                $scope.configs.modality_list = JSON.parse(data.configs.modality_list.replace(/u'id'/g, '"id"'));
                if ($scope.configs.modality_list) {
                $scope.selectedModalityList = [];
                _.each($scope.configs.modality_list, function (modality) {
                    _.each(modality_list, function (i) {
                        if (i.id == modality.id) {
                            $scope.selectedModalityList.push(i);
                        }
                    });
                });
            }
            } catch (e) {
                console.log("Modality list error. Reason: " + e);
            }
        });

        var guid = {
            guid: $stateParams.guid
        };

        $scope.deleteHospitalStudy = function(){
            hospitalStudyService.delete(guid, function(res){
                if(res.success != undefined){
                    ngToast.create({
                        className: 'success',
                        content:  "study deleted"
                    });
                    $state.transitionTo('dashboard.studies');
                }
                else {
                    ngToast.create({
                        className: 'danger',
                        content:  "Cannot delete study"
                    });
                }
            })
        }

        console.log(guid);

        try {
            studyDetailService.query(guid, function (res) {
                if (res.success != undefined) {
                    $scope.hospitalStudy = res.success;
                    console.log("Response: ", res.success);
                    var patient_date_of_birth_obj = getDateObj($scope.hospitalStudy.patient_date_of_birth, true);
                    $scope.hospitalStudy.patient_date_of_birth = moment(patient_date_of_birth_obj).format("DD-MMM-YYYY");
                }
            }, function (err) {
                console.log(err.error);
            })
        } catch (e){
            console.log(e);
        }

        $scope.format = 'dd-MMM-yyyy';
        $scope.hospitalStudy.studydate_opened = false;

        $scope.openStudyDatepicker = function ($event) {
            console.log("Inside date picker ", $event);
            $event.preventDefault();
            $event.stopPropagation();
            $scope.hospitalStudy.studydate_opened = true;
        };

        $scope.openPatientBirthDatepicker = function ($event) {
            console.log("Inside patient date of birth date picker ", $event);
            $event.preventDefault();
            $event.stopPropagation();
            $scope.hospitalStudy.patient_date_of_birth_opened = true;
        };

        $scope.$watch('hospitalStudy.metas', function (metas) {
            if(metas != undefined) {
                var data = {};

                metas.map(function (meta) {
                    data[meta.key] = meta.value;
                });

                delete $scope.hospitalStudy.metas;
                $scope.hospitalStudy.meta_data = data;
                var date = getDateObj($scope.hospitalStudy.meta_data.StudyDate, false);
                $scope.hospitalStudy.meta_data.StudyDate = moment(date).format("DD-MMM-YYYY");
                $scope.hospitalStudy.meta_data.StudyTime = getTimeObj($scope.hospitalStudy.meta_data.StudyTime);
            }
        });

        $scope.submitOrder = function (hospitalStudy) {

            // console.log("hospitalStudy: ", hospitalStudy);

            $scope.hospitalStudy.write_meta = true;
            $scope.hospitalStudy.is_storage = true;

            // Format Study Date
            $scope.hospitalStudy.meta_data.StudyDate = new Date($scope.hospitalStudy.meta_data.StudyDate);
            $scope.hospitalStudy.meta_data.StudyDate = getDateStr($scope.hospitalStudy.meta_data.StudyDate);

            // Format Patient Date of Birth
            $scope.hospitalStudy.patient_date_of_birth = new Date($scope.hospitalStudy.patient_date_of_birth);
            $scope.hospitalStudy.patient_date_of_birth = getDateStr($scope.hospitalStudy.patient_date_of_birth);

            // Format Study Time
            // $scope.hospitalStudy.meta_data.StudyTime = getTimeStr($scope.hospitalStudy.meta_data.StudyTime);

            hospitalStudyService.update({guid: $stateParams.guid}, $scope.hospitalStudy, function success(response) {
                ngToast.create({
                    className: 'success',
                    content:  "Order update successfully and start syncing"
                });
                $state.transitionTo('dashboard.studies');
            }, function err(error) {
                console.log("Catch error")
            });
        };

        $scope.cancel = function () {
            console.log("Cancel clicked");
            $state.transitionTo('dashboard.studies');
        };


    })

    .controller('studiesCtrl', function ($scope, createIsoService, $cookies, $filter, $sce, ngToast, studiesService, sendDicomService, HostService, checkHostService, $window) {

        $scope.search = null;
        $scope.bigCurrentPage = 1;
        $scope.maxSize = 10;
        $scope.user_group = JSON.parse($cookies.get('user')).groups[0].name;
        // $scope.bigTotalItems = undefined;

        $scope.clickEvent = function (guid, meta_status) {
            var base_url = window.location.protocol +"//"+ window.location.hostname;
            var base_uri = (window.location.port) ? base_url + ":" + window.location.port : base_url;
            var linkToViewer = base_url + ":8080" + "/alemhubviewer/index.php?file=dicoms/"; // viewer port is 8080
            var linkToReport = base_uri + "/#/studydetails/";
            var linkToHospitalStudies = base_uri + "/#/hospital-studies/";
            var url1 = linkToViewer + guid + "&htmlMode=on";
            var url2 = meta_status != 5 ? linkToReport + guid : linkToHospitalStudies + guid;
            console.log("hospital url is: ", url2);
            console.log("guid is: ", guid);
            console.log("url1 is: ", url1);
            window.open(url2, "_self");
            if($scope.user_group == 'admin'){
                $window.open(url1, "url1", "_blank", 'modal=yes', 'height=3840', 'width=2160');
            }
        };


        $scope.load_study = function () {
            studiesService.query({

                search: $scope.search,
                page: $scope.bigCurrentPage,
                per_page: $scope.maxSize

            }, function (res) {
                console.log(res.pagination);
                $scope.allOrders = res.orders;

                $scope.orders = res.orders;
                $scope.bigTotalItems = res.pagination.count;
                console.log($scope.orders)

            }, function (err) {
                console.trace(err);
            });

        };
        $scope.gethost = window.location.protocol +"//"+ window.location.hostname;

        // creating an array of selected orders
        $scope.get_orders_array = [];

        $scope.push_order = function(order){
            if(order.add_host_status == true)
                $scope.get_orders_array.push(order);
            else
                $scope.get_orders_array.pop(order);
            if($scope.get_orders_array.length < 1)
                $scope.disabled_button = false;
        };


        //add host to the studies
        $scope.assign_host = function () {
            //console.log($scope.selectedHost.id, $scope.get_orders_array);
            if($scope.get_orders_array.length < 1 && $scope.selectedHost.id == undefined){
                ngToast.create({
                    className: 'danger',
                    timeout: 2000,
                    content: "No host or study selected"
                });

                return;
            }
            var assign_to_workstarions = [];

            for(var i in $scope.get_orders_array) {
                assign_to_workstarions.push({
                    host_id: $scope.selectedHost.id,
                    guid: $scope.get_orders_array[i].guid
                });
            }
            console.log(assign_to_workstarions);

            try{
                studiesService.save({assigned: assign_to_workstarions}, function(res){
                    if(res.success != undefined)
                        ngToast.create({
                            className: 'success',
                            timeout: 2000,
                            content: res.success
                        });

                    else if(res.error != undefined)
                        ngToast.create({
                            className: 'danger',
                            timeout: 2000,
                            content: res.error
                        });
                }, function (err) {
                    console.log(err);
                })
            }
            catch (e){
                console.log(e);
            }
            $scope.get_orders_array = [];
            $scope.orders = [];
            studiesService.query({

                'search': $scope.search

            }, function (res) {

                $scope.orders = res.orders;

            }, function (err) {
                console.trace(err);
            });
        };

        $scope.check_host = function () {
            var host_id = $scope.selectedHost.id;

            if(host_id == undefined){
                ngToast.create({
                    className: 'danger',
                    timeout: 2000,
                    content: "There is no host selected"
                });
            }
            else {
                try {
                    checkHostService.query({host_id: host_id}, function(res){
                        if(res.success != undefined)
                            ngToast.create({
                                className: 'success',
                                timeout: 2000,
                                content: "Host is ready"
                            });
                        if(res.error != undefined)
                            ngToast.create({
                                className: 'danger',
                                timeout: 2000,
                                content: "Host is not ready"
                            });
                    }, function (err) {
                        console.log(err)
                    });
                }
                catch (e){
                    console.log(e);
                }
            }


        };

        // generating host list
        $scope.hosts = [];

        try {
            HostService.query(function(res) {
                if(res.success != undefined && res.success.length > 0){
                    $scope.hosts = res.success;
                }
                else if(res.error != undefined)
                    console.log(res);
            }, function(err) {
                console.log(err);
            });
        }
        catch (e) {
            console.log(e);
        }


        $scope.send_dicom = function (order) {

            ngToast.create({
                className: 'info',
                timeout: 2000,
                content: "Sending Dicom To Local Server"
            });

            sendDicomService.send_dicom({'guid': order.guid}, function (res) {

                if (res.error != undefined) {
                    ngToast.create({
                        className: 'danger',
                        timeout: 2000,
                        content: res.error
                    });
                }

                if (res.success != undefined) {
                    ngToast.create({
                        className: 'success',
                        timeout: 2000,
                        content: res.success
                    });
                }

            }, function (err) {
                ngToast.create({
                    className: 'danger',
                    timeout: 2000,
                    content: err
                });
            });

        };

        $scope.todayStudy = function () {
            $scope.orders = $scope.allOrders;
            var todaysOrder = _.filter($scope.orders, function (order) {
                var dates = new Date(order.created_at);
                var today = new Date();

                return dates.getDate() == today.getDate() && dates.getMonth() == today.getMonth() && dates.getYear() == dates.getYear();
            });

            if(todaysOrder.length > 0) {
                $scope.orders = [];
                $scope.orders = todaysOrder;
            }
            else {
                $scope.orders = [];
                ngToast.create({
                    className: 'danger',
                    timeout: 2000,
                    content: "No record found"
                });
            }
        };

        $scope.yesterdayStudy = function () {
            $scope.orders = $scope.allOrders;
            var yesterdaysOrder = _.filter($scope.orders, function (order) {
                var dates = new Date(order.created_at);
                var today = new Date();

                return dates.getDate() == (today.getDate()  - 1) && dates.getMonth() == today.getMonth() && dates.getYear() == dates.getYear();
            });
            if(yesterdaysOrder.length > 0){
                $scope.orders = [];
                $scope.orders = yesterdaysOrder;
            }
            else {
                $scope.orders = [];
                ngToast.create({
                    className: 'danger',
                    timeout: 2000,
                    content: "No record found"
                });
            }

        };

        $scope.studyUrgency = function () {
            $scope.orders = $scope.allOrders;
            console.log($scope.order);
            var urgentOrder = _.filter($scope.orders, function(order) {
                return order.priority == "Urgent"
            });

            if(urgentOrder.length > 0) {
                $scope.orders = [];
                $scope.orders = urgentOrder;
            }
            else {
                $scope.orders = [];
                ngToast.create({
                    className: 'danger',
                    timeout: 2000,
                    content: "No record found"
                });
            }
        };


        $scope.$watch('search', function () {

            if ($scope.search != undefined) {
                $scope.load_study();
            }

        });

        $scope.generateISO = function(guid){
            $scope.loading = true;
            console.log(guid);
            createIsoService.create({guid: guid}, function(res){
                if(res.success != undefined){
                    $scope.orders = [];
                    $scope.load_study();
                    $scope.loading = false;
                }
                else if (res.error != undefined){
                    ngToast.create({
                        className: 'danger',
                        content: "Cannot create ISO File"
                    });
                    $scope.loading = false;
                }
            }, function (err) {
                console.log(err);
            });
        }

    })


    .controller('studyDetailCtrl', function ($scope, $timeout, ngToast, $stateParams, studyDetailService, $filter, $sce, $cookies) {

        $scope.details = {};
        $scope.user_group = JSON.parse($cookies.get('user')).groups[0].name;

        try {
            var order_guid = $stateParams.guid;

            studyDetailService.query({guid: order_guid}, function (res) {

                if (res.success != undefined) {
                    $scope.details = res.success;
                    console.log(res);
                }
                else if (res.error != undefined) {
                    ngToast.create({
                        className: 'danger',
                        content: res.error
                    });
                }

            }, function (err) {
                console.log(err);
            })

        } catch (e) {
            console.log(e)
        }

        $scope.$watch('details.report_filename', function (pad_url) {
            if(pad_url !== undefined){
                $scope.pad_url = $filter('AppStorage')(pad_url);
                $scope.secure_url = function (pad_url) {
                    return $sce.trustAsResourceUrl(pad_url);
                };
            }

        });
        $scope.$watch('details.normal_report_filename', function (pad_url) {
            if(pad_url !== undefined){
                $scope.normal_pad_url = $filter('AppStorage')(pad_url);
                $scope.normal_secure_url = function (pad_url) {
                    return $sce.trustAsResourceUrl(pad_url);
                };
            }

        });


        $scope.updateInfo = function () {
            var update_data = {
                guid: $stateParams.guid,
                study_description: $scope.details.patient_studydescription,
                additional_description: $scope.details.patient_additionalhistory
            };
            console.log(update_data);

            try{
                studyDetailService.save(update_data, function(res) {
                    if(res.success != undefined) {
                        ngToast.create({
                            className: 'success',
                            content: res.success,
                            timeout: 2000
                        });
                    }
                    else if(res.error != undefined) {
                        ngToast.create({
                            className: 'danger',
                            timeout: 2000,
                            content: res.error
                        });
                    }
                });
            }
            catch (e) {
                console.log(e);
            }
        };
    })


    .controller('reportCtrl', function ($scope, $stateParams, ngToast, reportService, reportFormatService, studyDetailService, $cookies) {
        $scope.reportDetail = {};
        $scope.report = {
            clinical_information: ""
        };
        var clinical_information = [];

        reportService.query({guid: $stateParams.guid}, function (res) {
            if (res.success != undefined) {
                $scope.reportDetail = res.success;
                $scope.reportDetail.template = 1;
            }
            else if (res.error != undefined) {
                ngToast.create({
                    className: 'danger',
                    content: res.error
                })
            }
        }, function (err) {
            console.log(err);
        });

        $scope.report_submit = function () {
            $scope.user_id = JSON.parse($cookies.get('user')).pk;
            var report_data = "";
            try {

                ngToast.create({
                    className : 'info',
                    content : 'submitting report'
                });

                if ($.trim($scope.report.procedure) != ""){
                    var procedure_data = $scope.report.procedure.replace(/<p[^>]*>/g, '<br/>').replace(/<\/p>/g, '');
                    report_data = "<div style='text-align: center;'><u><b>" + procedure_data + "</b></u></div>";

                }
                if ($.trim($scope.report.clinical_information) != ""){
                    var clinical_data = $scope.report.clinical_information.replace(/<p[^>]*>/g, '').replace(/<\/p>/g, '</br>');
                    report_data += "<div style='text-align: left;'><b>Clinical Information:</b></br>" + clinical_data + "</div>";
                }
                if ($.trim($scope.report.comparison) != ""){
                    var comparison_data = $scope.report.comparison.replace(/<p[^>]*>/g, '').replace(/<\/p>/g, '</br>');
                    report_data += "<div style='text-align: left;'><b>Comparison:</b></br>" + comparison_data + "</div>";
                }
                if ($.trim($scope.report.findings) != ""){
                    var findings_data = $scope.report.findings.replace(/<p[^>]*>/g, '').replace(/<\/p>/g, '</br>');
                    report_data += "<div style='text-align: left;'><b>Findings:</b></br>" + findings_data + "</div>";
                }
                if ($.trim($scope.report.impression) != ""){
                    var impression_data = $scope.report.impression.replace(/<p[^>]*>/g, '').replace(/<\/p>/g, '</br>');
                    report_data += "<div style='text-align: left;'><b>Impression:</b></br>" + impression_data + "</div>";
                }
                // report_data += "</table></td></tr></table>"
                console.log("report data is>>> ", report_data);
                if ($.trim(report_data) != "") {
                    reportService.save(
                        {
                            "order_guid": $scope.reportDetail.guid,
                            "report" : report_data,
                            "user_id": $scope.user_id
                        },
                        function (res) {
                            if(res.success != undefined)
                            {
                                ngToast.create({
                                    className : 'success',
                                    content : res.success
                                });
                            }
                            else if(res.error != undefined) {
                                ngToast.create({
                                    className : 'danger',
                                    content : res.error
                                });
                            }
                        },
                        function (err) {
                            console.log(err);
                        }
                    );
                }


            } catch (e) {
                console.log(e);
            }
        };

        try {
            studyDetailService.query({guid: $stateParams.guid}, function (res) {
                if (res.success != undefined) {
                    console.log("Response: ", res.success);
                    // console.log("Response: ", res.success.metas[9].value);
                    if(res.success.metas[9].value != undefined && res.success.metas[9].value != "None"){
                        $scope.report.clinical_information = res.success.metas[9].value.replace(/\n/g, "<br />");
                        clinical_information.push({clinical_information: $scope.report.clinical_information});
                        console.log("I'm here>>>", $scope.report.clinical_information);
                    }
                }
            }, function (err) {
                console.log(err.error);
            })
        } catch (e){
            console.log(e);
        }

        // $scope.submit_report = function () {
        //
        //     try {
        //         if ($scope.reportDetail.report != undefined) {
        //             if ($.trim($scope.reportDetail.report) != '') {
        //
        //                 ngToast.create({
        //                     className : 'info',
        //                     content : 'submitting report'
        //                 });
        //
        //                 reportService.save({"order_guid":$scope.reportDetail.guid,"report" : $scope.reportDetail.report}, function (res) {
        //
        //                     if(res.success != undefined)
        //                     {
        //                         ngToast.create({
        //                             className : 'success',
        //                             content : res.success
        //                         });
        //                     }
        //                     else if(res.error != undefined) {
        //                         ngToast.create({
        //                             className : 'danger',
        //                             content : res.error
        //                         });
        //                     }
        //
        //                 }, function (err) {
        //                     console.log(err);
        //                 })
        //             }
        //             else {
        //                 throw "Report Filled is Empty";
        //             }
        //         }
        //         else {
        //             throw "Report Filled is Empty";
        //         }
        //     } catch (e) {
        //         ngToast.create(
        //             {
        //                 className: 'danger',
        //                 content: e
        //             }
        //         );
        //     }
        //
        // };
        $scope.oneAtATime = true;
        $scope.first = {
          open: true
        }
          $scope.$watch('first.open', function (isOpen) {
            if (!isOpen) {
              console.log('First group was opened');
            }
          });

        //   $scope.addNew = function() {
        //     $scope.groups.push({
        //       title: "New One Created",
        //       content: "Dynamically added new one",
        //       open: false
        //     });
        //   }




        $scope.reportTemplate = [{
          'name': 'Noraml Post Nasal Space',
          'report' :  '<strong>PNS </strong> ' +
                      '<br /><br />'+
                      '<p>Normal nasopharyngeal air column. </p>'
        },{
          'name': 'NORMAL Abdominal X-ray',
          'report': '<strong>ABDOMEN</strong> '+
                    '<br /><br />'+
                    '<p>Bowel gas pattern is normal.</p>'+
                    '<p>Bowel loops are not dilated.</p>'+
                    '<p>No extraluminal air is seen.</p>'
        },{
          'name': 'NORMAL Abdominal X-ray',
          'report': '<strong>BOTH  ANKLES</strong>'+
            '<br /><br /><p>Normal ankle mortices.</p>' +
            '<p>No bone or joint lesion is seen.</p>'

        },{
          'name': 'NORMAL BARIUM ENEMA',
          'report': '<strong>BARIUM ENEMA </strong>'+
          '<br /><br />'+

          '<strong>CONTROL FILM</strong>'+
          '<p>There is fullness of the abdominal flank.</p>'+
          '<p>There is abdominal distension evidenced by the fullness of the flanks.</p>'+
          '<p>There is enlargement of both renal bed.</p>'+
          '<br /><br />'+
          '<strong>ON INJECTION OF CONTRAST</strong>'+
          '<p>There is free retrograde flow of contrast outlining the normal colon.</p>'+
          '<p>No abnormal area of narrowing is noted.</p>'+
          '<p>Presacral space is within normal limit.</p>'+
          '<br /><br />'+
          '<p><strong>Impression:</strong> Essentially normal study.  </p>'


        }];
        $scope.selectReportTemplate = function(){
          $scope.reportDetail.report = $scope.reportTemplate[$scope.reportDetail.template]['report'];
        };
        $scope.bigCurrentPage = 1;
        $scope.maxSize = 20;

        try {
            reportFormatService.query({
                page: $scope.bigCurrentPage,
                per_page: $scope.maxSize,
                provider_id: 10
            }, function(res){
                console.log(res);
                $scope.reports = res.reports;
            }, function (err) {
                console.log(err);
            });
        } catch (e) {
            console.log(e);
        }

        $scope.selectReport = function(){
            reportFormatService.query({
                report_id: $scope.reportId
            }, function(res){
                console.log(res.report);
                $scope.report = res.report
                console.log(clinical_information);
                if(clinical_information.length != 0){
                    $scope.report.clinical_information = clinical_information[0].clinical_information;
                }
            }, function(err){
                console.log(err);
            });
        };

        $scope.$watch('report.procedure', function (data) {
            if(data != undefined){
                var procedure;
                if(data.match(/<br>/g) != null){
                    procedure = data.replace(/<br>/g, '\n');
                    $scope.report.procedure = procedure;
                }
                if(data.match(/&nbsp;/g) != null){
                    procedure = data.replace(/&nbsp;/g, ' ');
                    $scope.report.procedure = procedure;
                }
            }
        });

        $scope.$watch('report.clinical_information', function (data) {
            if(data != undefined){
                var clinical;
                if(data.match(/<br>/g) != null){
                    clinical = data.replace(/<br>/g, '\n');
                    $scope.report.clinical_information = clinical;
                }
                if(data.match(/&nbsp;/g) != null){
                    clinical = data.replace(/&nbsp;/g, ' ');
                    $scope.report.clinclinical_informationical = clinical;
                }
            }
        });

        $scope.$watch('report.comparison', function (data) {
            if(data != undefined){
                var comparison;
                if(data.match(/<br>/g) != null){
                    comparison = data.replace(/<br>/g, '\n');
                    $scope.report.comparison = comparison;
                }
                if(data.match(/&nbsp;/g) != null){
                    comparison = data.replace(/&nbsp;/g, ' ');
                    $scope.report.comparison = comparison;
                }
            }
        });

        $scope.$watch('report.findings', function (data) {
            if(data != undefined){
                var findings;
                if(data.match(/<br>/g) != null){
                    findings = data.replace(/<br>/g, '\n');
                    $scope.report.findings = findings;
                }
                if(data.match(/&nbsp;/g) != null){
                    findings = data.replace(/&nbsp;/g, ' ');
                    $scope.report.findings = findings;
                }
            }
        });

        $scope.$watch('report.impression', function (data) {
            if(data != undefined){
                var impression;
                if(data.match(/<br>/g) != null){
                    impression = data.replace(/<br>/g, '\n');
                    $scope.report.impression = impression;
                }
                if(data.match(/&nbsp;/g) != null){
                    impression = data.replace(/&nbsp;/g, ' ');
                    $scope.report.impression = impression;
                }
            }
        });


    });

function n(n) {
    return n > 9 ? "" + n : "0" + n;
}

function getDateStr(dateObj) {

    if (dateObj != '') {
        return dateObj.getFullYear().toString() + n(dateObj.getMonth() + 1) + n(dateObj.getDate())
    }
}

function getTimeStr(dateObj) {
    if (dateObj != '') {
        return n(dateObj.getHours()) + n(dateObj.getMinutes()) + '00'
    }
}

function getDateObj(dateStr, chk_today) {
    if (dateStr != "") {
        var today = new Date();

        var dateFormatObj = new Date(
            dateStr.substring(0, 4) + '-' + dateStr.substring(4, 6) + '-' + dateStr.substring(6, 8)
        );

        // return dateFormatObj;

        // if its today then return null string .
        if (chk_today && today.getFullYear() == dateFormatObj.getFullYear() && today.getMonth() == dateFormatObj.getMonth() && today.getDate() == dateFormatObj.getDate())
            return '';
        else
            return dateFormatObj;
    } else {
        return '';
    }
}

function getTimeObj(timeStr) {
    var d = new Date();

    if (timeStr != '') {
        d.setHours(parseInt(timeStr.substring(0, 2)));
        d.setMinutes(parseInt(timeStr.substring(2, 4)));
    }

    return d
}
