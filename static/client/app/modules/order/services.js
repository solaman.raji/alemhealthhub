'use restrict';

angular.module('OrderApp.services')


.service('studiesService',function($resource) {

    return $resource('api/orderlist', {}, {
            query : { method: 'GET' },
            save: {method: 'PUT'}
        }
    );

})


.service('sendDicomService',function($resource) {

    return $resource('api/senddicomtolocal',{},{
        send_dicom : { method : 'GET' }
    });

})
.service('createIsoService',function($resource) {

    return $resource('api/create_iso',{},{
        create : { method : 'PUT' }
    });

})



.service('studyDetailService',function($resource) {
    return $resource('api/order/:guid',{guid:'@guid'},{
        query : {method : 'GET'},
        save : {method : 'PUT'}
    });
})

.service('hospitalStudyService',function($resource) {
    return $resource('api/hospital_study/:guid',{guid:'@guid'},{
        update : {method : 'PUT'},
        delete: {method: 'DELETE'}
    });
})


.service('reportService',function($resource) {

    return $resource('api/report/:guid/',{guid:'@guid'},{
        query : {method : 'GET'},
        save : {method : 'PUT'}
    });

})

.service('hostUpdateService', function($resource) {
    return $resource('api/order/:guid', {guid: '@guid'}, {
        save: {
            method: 'PUT'
        }
    });
})

.service('checkHostService', function($resource){
    return $resource('api/check_host/:host_id', {host_id: '@host_id'}, {
        query: {
            method: 'GET'
        }
    })
})

.service('reportFormatService', function($resource){
    return $resource('api/reporttemplate', {}, {
        query: {
            method: 'GET'
        },
        delete: {
            method: 'DELETE'
        },
        save: {
            method: 'PUT'
        },
        create: {
            method: 'POST'
        }

    })
});
