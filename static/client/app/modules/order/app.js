'use strict';

angular.module('OrderApp')

    .config(
        function ($stateProvider) {
            var templateDir = '/static/client/app/';

            $stateProvider

                .state('dashboard.studies', {
                    url: "/studies",
                    views: {
                        'dashboard-view': {
                            templateUrl: templateDir + "/templates/main_app/order/studies.html",
                            controller: 'studiesCtrl'
                        }
                    }
                })

                .state('dashboard.studydetails', {
                    url: "/studydetails/{guid}",
                    views: {
                        'dashboard-view': {
                            templateUrl: templateDir + "/templates/main_app/order/studydetails.html",
                            controller : 'studyDetailCtrl'
                        }
                    }
                })

                .state('dashboard.report', {
                    url: "/report/{guid}",
                    views: {
                        'dashboard-view': {
                            templateUrl: templateDir + "/templates/main_app/order/report.html",
                            controller : 'reportCtrl'
                        }
                    }
                })

                .state('dashboard.hospitalStudy', {
                    url: '/hospital-studies/{guid}',
                    views: {
                        'dashboard-view': {
                            templateUrl: templateDir + "/templates/main_app/order/hospital-studies.html",
                            controller : 'hospitalStudiesCtrl'
                        }
                    }
                })

                .state('dashboard.reportFormat', {
                    url: '/report-format',
                    views: {
                        'dashboard-view': {
                            templateUrl: templateDir + "/templates/main_app/order/report-format.html",
                            controller : 'reportFormatCtrl'
                        }
                    }
                })

                .state('dashboard.reportFormatUpdate', {
                    url: '/report-format-update/{id}',
                    views: {
                        'dashboard-view': {
                            templateUrl: templateDir + "/templates/main_app/order/report-format-update.html",
                            controller : 'reportFormatUpdateCtrl'
                        }
                    }
                })

                .state('dashboard.addReport', {
                    url: '/add-report',
                    views: {
                        'dashboard-view': {
                            templateUrl: templateDir + "/templates/main_app/order/add-report.html",
                            controller : 'addReportCtrl'
                        }
                    }
                })

                .state('dashboard.newHome', {
                    url: '/home',
                    views: {
                        'dashboard-view': {
                            templateUrl: templateDir + "/templates/main_app/user/home.html",
                            controller : 'homeCtrl'
                        }
                    }
                })

        }
    );


angular.module('OrderApp.services', []);
angular.module('OrderApp.controllers', []);
angular.module('OrderApp.directives', []);
