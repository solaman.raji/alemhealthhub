angular.module('ProviderSettingApp.controllers')


    .controller('providerSettingCtrl', function ($scope, $state, $rootScope, ngToast, SetBoxService) {
        $scope.config = {};


        $scope.set_box = function (config) {
            try {

                // simple form validation
                if (Object.keys(config).length != 5) {
                    throw 'Please fill up the form fileds properly'
                }
                else {
                    angular.forEach(config, function (value, key) {
                        if ($.trim(value) == '') {
                            throw 'Form Field is Empty';
                        }
                    })
                }

                SetBoxService.create({'config': config}, function success(res) {
                    console.log("Response is ", res);
                        if (res.error != undefined) {
                            ngToast.create({
                                className : 'danger',
                                content : res.error
                            })
                        }
                        else {
                            ngToast.create({
                                className: 'success',
                                content: res.success,
                                dismissOnTimeout: true,
                                timeout: 1500,
                            });


                            $rootScope.$emit('direct_login', res);
                        }


                    },
                    function error(err) {
                        ngToast.create({
                            className: 'warning',
                            dismissOnTimeout: true,
                            timeout: 1500,
                            content: err
                        });
                    })
            }
            catch (e) {
                ngToast.create({
                    className: 'warning',
                    dismissOnTimeout: true,
                    timeout: 1500,
                    content: e
                });
            }
        }

    });