'use strict';

angular.module('ProviderSettingApp.services', []);
angular.module('ProviderSettingApp.controllers', []);


angular.module('ProviderSettingApp')

    .config(function ($stateProvider) {
        var templateDir = '/static/client/app/';

        $stateProvider

            .state('provider_settings', {
                url: '/provider_set',
                templateUrl: templateDir + 'templates/hub_setting/provider-settings.html',
                controller: 'providerSettingCtrl'
            })

    })


    .run(function ($rootScope, $window, $state) {

         console.log("provider");

    })

;
