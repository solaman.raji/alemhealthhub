/**
 *    Core Script
 */

'use strict';

//Common App
angular.module('CommonApp.directives', []);
angular.module('CommonApp.services', []);

angular.module('CommonApp', [
    'CommonApp.directives',
    'CommonApp.filters',
    'CommonApp.controllers',

    // common directives
    'ui.router',
    'ngResource',
    'ui.bootstrap',
    'googlechart',
    'textAngular',
    'ngCookies',
    'ngToast',
    'xeditable',
    'ui.select'

]);


// provider settings
angular.module('ProviderSettingApp', [
    'CommonApp',

    'ProviderSettingApp.services',
    'ProviderSettingApp.controllers'
]);


//User app
angular.module('UserApp', [
    'CommonApp',
    'angularFileUpload',
    'UserApp.services',
    'UserApp.controllers',
    'UserApp.directives'
]);


// Order app
angular.module('OrderApp', [
    'CommonApp',
    'OrderApp.services',
    'OrderApp.controllers',
    'OrderApp.directives'
]);


// adding all the app to main app
angular.module('alemhealth', [
    'CommonApp',
    'CommonApp.services',

    'UserApp',
    'OrderApp',
    'ProviderSettingApp'

]);


// alemhealthHub app config
angular.module('alemhealth')

    .config(['$stateProvider', '$urlRouterProvider', '$httpProvider', function ($stateProvider, $urlRouterProvider, $httpProvider) {

        var templateDir = "/static/client/app/";

        $stateProvider

            .state('dashboard', {
                url: "",
                abstract: true,
                templateUrl: templateDir + "templates/main_app/base.html",
                controller : 'DashboardCtrl'
            })

        ;


        // default provider
        $urlRouterProvider.otherwise('/studies');


        // http intercepter
        $httpProvider.defaults.xsrfCookieName = 'csrftoken';
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
        $httpProvider.interceptors.push('httpRequestInterceptor');
        $httpProvider.interceptors.push('httpResponseInspector');


    }])


    .run(function ($rootScope, $cookies, $state, $timeout, CheckDBService) {
        // checking login or not
        $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {
            // db created or not
            $timeout(function () {
                CheckDBService.query({}, function success(res) {
                    if (res.status) {
                        $rootScope.$emit('logged_in', {})
                    }
                    else {
                        $state.transitionTo('provider_settings');
                    }

                    event.preventDefault();
                }, function error(err) {
                    event.preventDefault();
                })
            }, 0);

        });

        // logged in or not
        $rootScope.$on('logged_in', function (event, args) {
            // not logged in
            if ($cookies.get('user') == undefined) {
                $state.transitionTo('login');
            }
            // logged in
            else{
                if(($state.current.url == '/provider_set') || ($state.current.url == '/login'))
                {
                    $state.transitionTo('dashboard.studies');
                }
            }
        });

        // direct login
        $rootScope.$on('direct_login', function (event, res) {
            $cookies.putObject('user', res.user);

            $state.transitionTo('dashboard.studies');
        });

    })
;

// alemhealthHub factories
angular.module("alemhealth").factory('httpRequestInterceptor', function ($cookies) {
    return {
        request: function (config) {

            if (config.url.match('^\/static.+html$') != null) {
                config.url = config.url + '?' + $("#static_guid").val()
            }

            config.headers = {
                'Authentication': $cookies.getObject('user') ? $cookies.getObject('user').access_key : '',
                'X-CSRFToken': $('input[name=csrfmiddlewaretoken]').val()
            };


            return config;
        }
    };
});

angular.module("alemhealth").factory('httpResponseInspector', function ($q, $cookies) {
    return {
        response: function (response) {
            return response;
        },
        responseError: function (response) {
            // do something on error
            if (response.data.code == '403')
                $cookies.remove('user');
            return $q.reject(response);
        }
    };
});
