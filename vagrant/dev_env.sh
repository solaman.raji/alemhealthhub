#!/bin/bash 

export WORKON_HOME=$HOME/.virtualenvs
source /usr/local/bin/virtualenvwrapper.sh

export PROJECT=alemhealthhub
export PROJECT_HOME=/$PROJECT
cd $PROJECT_HOME

ENV_NAME=virtualenv-$PROJECT
mkvirtualenv $ENV_NAME
workon $ENV_NAME

cd $PROJECT_HOME
pip install -r alemhealthhub/requirement.txt
#python setup.py develop

mkdir -p /var/lib/alemhealthhub/{data,media} /var/cache/alemhealthhub/{dicoms,tmp}
mkdir -vp /opt/alemhealthhub/{dcmtk,conf}

cp -rv $PROJECT_HOME/dev-ops/opt /
# override with vagrant settings
cp -rv $PROJECT_HOME/vagrant/opt /

cd $PROJECT_HOME; python manage.py migrate
cd $PROJECT_HOME; python manage.py runscript create_schedule_task
cd $PROJECT_HOME; python manage.py collectstatic --noinput -i bower_components
cp -v $PROJECT_HOME/scripts/hub_access_keys.json /tmp/hub_access_keys.json
# cd $PROJECT_HOME; python manage.py runscript data_populate

export BASHRC_PATH=~/.bashrc
if ! grep -Fq "WORKON_HOME" $BASHRC_PATH; then
    echo 'export WORKON_HOME=$HOME/.virtualenvs' >> $BASHRC_PATH
    echo 'source /usr/local/bin/virtualenvwrapper.sh' >> $BASHRC_PATH
    echo ". $WORKON_HOME/$ENV_NAME/bin/activate" >> $BASHRC_PATH
    echo "export LANGUAGE=en_US.UTF-8" >> $BASHRC_PATH
    echo "export LC_ALL=en_US.UTF-8" >> $BASHRC_PATH
    echo "export RABBITMQ_HOME=/usr/lib/rabbitmq/bin" >> $BASHRC_PATH
fi

# Frontend packages
echo "Install node packages"

echo "Run npm install"
npm install

echo "Run bower install"
cd $PROJECT_HOME/static
bower install
cd $PROJECT_HOME

echo "Install grunt dev"
grunt dev
