#!/bin/sh

SESSION_NAME="v_hub"
cd /alemhealthhub
tmux new -s $SESSION_NAME -d

tmux rename-window -t $SESSION_NAME:0 "htop"
tmux send-keys -t $SESSION_NAME:0 "htop" C-m

tmux new-window -t $SESSION_NAME:1 -n "django-server"
tmux send-keys -t $SESSION_NAME:1 "python manage.py runserver 192.168.11.11:8000" C-m

tmux new-window -t $SESSION_NAME:2 -n "celeryd"
tmux send-keys -t $SESSION_NAME:2 "python manage.py celeryd" C-m

tmux new-window -t $SESSION_NAME:3 -n "celeryd_study"
tmux send-keys -t $SESSION_NAME:3 "python manage.py celeryd -l INFO -Q add.dicom.from.listener,study.download" C-m

tmux new-window -t $SESSION_NAME:4 -n "celeryd_reporting"
tmux send-keys -t $SESSION_NAME:4 "python manage.py celeryd -l INFO -Q report.upload,report.download,report.mirth" C-m

tmux new-window -t $SESSION_NAME:5 -n "celerybeat"
tmux send-keys -t $SESSION_NAME:5 "python manage.py celerybeat" C-m

tmux new-window -t $SESSION_NAME:6 -n "alemhealthhub.log"
tmux send-keys -t $SESSION_NAME:6 "tail -f /var/log/alemhealthhub/hub.log" C-m

tmux new-window -t $SESSION_NAME:7 -n "storescu"
tmux send-keys -t $SESSION_NAME:7 "cd /var/cache/alemhealthhub/test_dicoms" C-m

tmux new-window -t $SESSION_NAME:8 -n "media"
tmux send-keys -t $SESSION_NAME:8 "cd /var/lib/alemhealthhub/media" C-m

tmux new-window -t $SESSION_NAME:9 -n "dicoms"
tmux send-keys -t $SESSION_NAME:9 "cd /var/cache/alemhealthhub/dicoms" C-m

tmux new-window -t $SESSION_NAME:10 -n "worklist db"
tmux send-keys -t $SESSION_NAME:10 "cd /var/lib/alemhealthhub/worklist" C-m

tmux new-window -t $SESSION_NAME:11 -n "worklist cache"
tmux send-keys -t $SESSION_NAME:11 "cd /var/cache/alemhealthhub/worklist" C-m

tmux new-window -t $SESSION_NAME:12 -n "storescp"
tmux send-keys -t $SESSION_NAME:12 "sh storescp.sh" C-m

tmux new-window -t $SESSION_NAME:13 -n "dbshell"
tmux send-keys -t $SESSION_NAME:13 "python manage.py dbshell" C-m

tmux new-window -t $SESSION_NAME:14 -n "shell"
tmux send-keys -t $SESSION_NAME:14 "python manage.py shell" C-m

tmux new-window -t $SESSION_NAME:15 -n "bash"

tmux attach -t $SESSION_NAME
