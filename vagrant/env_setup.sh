
# RabbitMQ Plugins
echo "Enable RabbitMQ Plugins"
service rabbitmq-server stop
/usr/lib/rabbitmq/bin/rabbitmq-plugins enable rabbitmq_management
/usr/lib/rabbitmq/bin/rabbitmq-plugins enable rabbitmq_jsonrpc
service rabbitmq-server start

/usr/lib/rabbitmq/bin/rabbitmq-plugins list

# Set up nginx

cp /alemhealthhub/nginx.conf /etc/nginx/sites-available/alemhealthhub
rm -vf /etc/nginx/sites-enabled/default
ln -fs /etc/nginx/sites-available/alemhealthhub /etc/nginx/sites-enabled/alemhealthhub
service nginx restart