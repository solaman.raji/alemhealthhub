#!/bin/sh

cd /alemhealthhub

tmux new-session -s servers -d
tmux rename-window -t servers "htop"

tmux new-window -t servers:1 -n "django-servers"
tmux send-keys -t servers:django "python manage.py runserver 0.0.0.0:8000" C-m

tmux new-window -t servers:2 -n "celery"
tmux split-window -h
tmux select-pane -t 0
tmux send-keys  "python manage.py celeryd" C-m
tmux select-pane -t 1
tmux send-keys "python manage.py celerybeat" C-m

tmux new-window -t servers:3 -n "storescp"
tmux send-keys ". storescp.sh" C-m
