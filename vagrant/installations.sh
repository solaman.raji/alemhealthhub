#!/bin/bash
echo "Update aptitude packages"
apt-get update
echo "Install python and other packages"
apt-get install -y python2.7-dev python-pip git nginx rabbitmq-server tmux curl dcmtk zpaq htop iftop sqlite3

pip install -U pip
echo "Install python virtual environment"
pip install virtualenv virtualenvwrapper

ln -s /usr/lib/rabbitmq/bin/rabbitmq-plugins /usr/local/bin/rabbitmq-plugins

mkdir -p /var/{log,cache,lib,backups,www}/alemhealthhub /opt/alemhealthhub
chown -R ubuntu /var/{log,cache,lib,backups,www}/alemhealthhub /opt/alemhealthhub

# Frontend packages
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
apt-get install -y nodejs
npm install -g bower
npm install -g grunt
