import uuid

from django.core.management import call_command
from django.test import TestCase
from django.utils.six import StringIO


class PrintStudyTest(TestCase):
    def test_command_output(self):
        out = StringIO()
        study_guid = uuid.uuid4()
        call_command('print_study', study_guid.hex, stdout=out)
        self.assertIn('Successfully printed study', out.getvalue())