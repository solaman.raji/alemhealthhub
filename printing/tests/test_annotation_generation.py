from django.test import TestCase
from printing.tasks.config_generator.annotation import Annotation, Section, annotation_template



class TestAnnotationGenration(TestCase):

    def test_generate_annotation_header(self):
        annotation = Annotation()
        print repr(annotation)
        assert repr(annotation) == '[[[ANNOTATIONS]]]\n'

    def test_generate_annotation_config(self):
        annotation_instance = '''[[[ANNOTATIONS]]]

[[ANNOTATION-1]]

[General]
Description  = Annotations for CUSTOM-1 layout

CanonicalName = 1

[Line-1]
defaulttext  = DICOM Sample Image

[Line-2]
defaulttext  = Patient: (Hawa Gul)

'''

        annotation_dict = dict(number=1,
                               line_one_text='DICOM Sample Image',
                               patient_name='Hawa Gul')
        annotation_1 = annotation_template.format(**annotation_dict)
        annotation = Annotation([annotation_1])
        print annotation
        self.assertEqual(repr(annotation), annotation_instance)

    def test_generate_multiple_annotation_config(self):
        instance = '''[[[ANNOTATIONS]]]

[[ANNOTATION-1]]

[General]
Description  = Annotations for CUSTOM-1 layout

CanonicalName = 1

[Line-1]
defaulttext  = DICOM Sample Image

[Line-2]
defaulttext  = Patient: (Hawa Gul)


[[ANNOTATION-2]]

[General]
Description  = Annotations for CUSTOM-2 layout

CanonicalName = 2

[Line-1]
defaulttext  = DICOM Multiple Image

[Line-2]
defaulttext  = Patient: (Fata De Gul)

'''
        annotation_dict = dict(number=1,
                               line_one_text='DICOM Sample Image',
                               patient_name='Hawa Gul')
        annotation_1 = annotation_template.format(**annotation_dict)
        print '<', annotation_1, '>'

        annotation_dict = dict(number=2,
                               line_one_text='DICOM Multiple Image',
                               patient_name='Fata De Gul')
        annotation_2 = annotation_template.format(**annotation_dict)
        print '<', annotation_2, '>'

        annotation = Annotation([annotation_1, annotation_2])
        print annotation
        self.assertEqual(repr(annotation), instance)
