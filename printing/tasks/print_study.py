import os
import tempfile
import uuid

import printing.settings as settings
from printing.tasks.printscu import execute_dicom_print_client
from .config_generator.client_config import generate_client_config
from .config_generator.config_writer import ConfigWriter


class StudyPrinter(object):
    _config_file_suffix = '.cfg'
    _config_file_prefix = 'tcpprt'

    def __init__(self, study_guid):
        super(StudyPrinter, self).__init__()
        self.study_guid = study_guid

    def generate_print_config(self, session_options, page_options, annotations):
        fp, config_filepath = tempfile.mkstemp(self._config_file_suffix, self._config_file_prefix)
        client_config = generate_client_config(session_options, page_options)
        ConfigWriter(client_config=client_config).write_config(config_filepath)
        return config_filepath

    def exec_printscu(self, config_path, ):
        assert os.path.exists(config_path), 'Config file {} does not exists'.format(config_path)
        try:
            execute_dicom_print_client(config_path, self.get_dcmfile_path())
        except Exception as ex:
            print 'Error in printing {}'.format(ex.message)
            raise ex


    def get_dcmfile_path(self):
        MEDIA_DIR = settings.MEDIA_DIR
        dicom_path = os.path.join(MEDIA_DIR, str(self.study_guid.hex), '', '*.dcm')
        print 'Print dicoms {}'.format(dicom_path)
        return dicom_path
        # if os.path.exists(dicom_path):
        #     return os.path.join(dicom_path, '*.dcm')
        # else:
        #     raise Exception('Study directory does not found for {}'.format(dicom_path))


def print_study(study_guid, session_options, page_options, annotations, **kwargs):
    printer = StudyPrinter(study_guid)
    config_filepath = printer.generate_print_config(session_options, page_options, annotations)
    print 'file path', config_filepath
    printer.exec_printscu(config_filepath)


if __name__ == '__main__':
    study_guid = uuid.uuid4()
    session_options = {}
    page_options = {}

    print_study(study_guid, session_options=session_options, page_options=page_options, annotations=[])
    print 'Printing Done!'
