"""
Execute the tcpprt command with configuration

Sample command:
    tcpprt -v -d -c /etc/dcmtk/tcpprt.cfg --auto 1 $DCM_FILE

Sample command with logging

  tcpprt_e  -t 192.168.0.60:104 -aet AE_REPORTING -aec DRYPIX4000 \
    --log-config /var/cache/alemhealthhub/printing/logger.cfg \
    --auto 1 --save --config /var/cache/alemhealthhub/printing/tcpprt.cfg \
    /var/lib/alemhealthhub/media/dicoms/e6c84e0bd249456882464f48f52322ee/*.dcm

Sample command without logging

  tcpprt_e -v -d -t 192.168.0.60:104 -aet AE_REPORTING -aec DRYPIX4000 \
    --auto 1 --save --config /var/cache/alemhealthhub/printing/tcpprt.cfg \
    /var/lib/alemhealthhub/media/dicoms/e6c84e0bd249456882464f48f52322ee/*.dcm

"""
import subprocess
import sys

from ..settings import PRINT_CLIENT_COMMAND


# FIXME: Add checking to find out if tcpprt exists

def execute_dicom_print_client(config_path, dcmfile_in, auto_config_number=1,
                               verbose=False, debug=False, enable_logging=True):
    print_command_elements = [PRINT_CLIENT_COMMAND]

    if enable_logging:
        logging = ['--log-config', '/opt/alemhealthhub/printing/logger.cfg'] if enable_logging else ''
        print_command_elements.extend(logging)
    else:
        verbose = '-v' if not enable_logging and verbose else ''
        debug = '-d' if not enable_logging and debug else ''
        print_command_elements.extend([verbose, debug])

    print_command_elements.extend(['--auto', str(auto_config_number), '--config', config_path, dcmfile_in])

    print 'Execute print command: ', ' '.join(print_command_elements)

    print print_command_elements
    # subprocess.check_output(print_command_elements, shell=True)
    subprocess.check_output(' '.join(print_command_elements), shell=True)


if __name__ == '__main__':
    default_config_path = '/tmp/tcpprt.cfg'
    if len(sys.argv) > 1:
        default_config_path = sys.argv[1]
    print 'Execute dicom print for ', sys.argv[2], ' with config file path: ', default_config_path
    execute_dicom_print_client(default_config_path, sys.argv[2], verbose=False, debug=False)
