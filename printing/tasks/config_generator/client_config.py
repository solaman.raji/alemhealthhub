import StringIO

from printing.tasks.config_generator.annotations import generate_annotations_config
from printing.tasks.config_generator.page import generate_page_config
from printing.tasks.config_generator.session import generate_session_config

_general_config_base = '''
[[[CLIENT]]]
;  Each section here should be named AUTO-number where <number> is passed
;  to tcpprt using the --auto command line parameter.
;  Each section defines one set of parameters for batch mode operation.
[[AUTO-1]]
[General]
Description  = Printserver Custom Layout, 1 image/page, collated
Collation = NO

; optional: number of images (files) per page. After each film box,
; the given number of image files will be marked as "done", independent
; from the image display format used. Normally this number is computed
; automatically from the number of [IMAGE-X] sections (see below).
; This setting may be useful if multiple frames of a multi-frame image
; should be printed. Default is 0 (automatic computation).
FilesPerPage = 0

[PresentationLUT]
;  -----------------------------------------------------------------------

; optional. Type of presentation LUT. Possible values are "IDENTITY",
; "LIN OD" and "FILE". While "IDENTITY" and "LIN OD" represent LUT shapes,
; FILE means that a Presentation LUT is read from the file specified
; in PresentationLUTFile. Default is IDENTITY.
PresentationLUT = IDENTITY

; optional. Complete path to Presentation LUT File to be activated.
; Only used if PresentationLUT is FILE.
PresentationLUTFile = gamma.lut


;  -----------------------------------------------------------------------
;  This section is mandatory. There must be as many [Image-X] sections as
;  there are image boxes on each page (film box) of the chose image display
;  format. This section defines the settings for the image box with
;  Image Position 1.
[Image-1]
;  -----------------------------------------------------------------------

; This entry is mandatory. It defines the order in which filenames passed
; to tcpprt on the command line are mapped to the image boxes. For an image
; display format with X images, numbers 1..X can be used, referring to the
; files of the file set chosen for the current page.
File = 1

; optional: frame number of the image file to be printed. Default is 1.
; Frame = 1

; optional: this parameter can be used to adjust the window level and width
; (i.e. apply a VOI LUT transformation) if and only if the parameters of
; the VOI are encoded in the image file, either as Window Center/Width or
; as a VOI LUT Sequence.
; The parameter only specifies the number of the transformation, in the case
; that multiple transformations are contained in the image. The index starts
; at 1 and counts all Window Center/Width pairs followed by all VOI LUT Sequence
; items.
; Default is not to apply a VOI transformation before printing the image.
; VOILUT = 1

; optional: Polarity. Default: don't send, use SCP default.
; Possible values are NORMAL, REVERSE.
; Polarity = NORMAL

; optional: Magnification Type. Default: don't send, use SCP default.
; Defined terms: REPLICATE, BILINEAR, CUBIC, NONE
; Magnification = CUBIC

; optional: Smoothing Type. Default: don't send, use SCP default.
; Possible values are totally printer specific, check conformance statement.
; Smoothing = 1

; optional: Requested Image Size. Default: don't send, use SCP default.
; Value is requested width of image in mm.
; RequestedSize = 200


;  -----------------------------------------------------------------------
;  This section is optional. It is required if annotation boxes of the
;  optional Annotation Box SOP Class are to be used. There must be as many
;  [Annotation-X] sections as there are annotations in use. This section
;  defines the text for the annotation box with Annotation Position 1.
; [Annotation-1]
;  -----------------------------------------------------------------------

; Text = Patient's Name: \[0010,0010,1]
;

; Add session config

; Add Page Config

'''


def generate_client_config(session_config=None, page_config=None):
    config_text = StringIO.StringIO()
    config_text.write(_general_config_base)
    # config_text.write(read_base_client_config())

    session_config = session_config or {}
    config_text.write(generate_session_config(**session_config))

    page_config = page_config or {}
    config_text.write(generate_page_config(**page_config))

    config_text.write(generate_annotations_config())

    return config_text.getvalue()


if __name__ == '__main__':
    config_output = ''
    session_config = {'mediumtype': 'CLEAR FILM'}
    page_config = {'imagedisplayformat': r'STANDARD\2,3', 'annotation': 'FORMAT3'}

    config_output = generate_client_config(session_config=session_config, page_config=page_config)

    print config_output
