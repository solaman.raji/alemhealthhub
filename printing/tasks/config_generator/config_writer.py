from printing.tasks.config_generator.client_config import generate_client_config


class ConfigWriter(object):
    base_config_path = '/opt/alemhealthhub/printing/base_tcpprt.cfg'
    # base_config_path = '/Users/issa/projects/vagrant/alemhealth-vagrants/alemhealthhub/dev-ops/opt/alemhealthhub/printing/base_tcpprt.cfg'

    def __init__(self, client_config=''):
        super(ConfigWriter, self).__init__()
        self.client_config = client_config

    def write_config(self, output_path):
        with open(output_path, 'w') as output_config_fp:
            with open(self.base_config_path) as base_config_fp:
                for line in base_config_fp.readlines():
                    output_config_fp.write(line)

                self.client_config and output_config_fp.write(self.client_config)
                output_config_fp.flush()


if __name__ == '__main__':
    output_path = '/tmp/tcpprt_1.cfg'
    print 'Generate configuration '
    print 'Write configuration to ', output_path

    _config = client_config = generate_client_config()
    print 'client config'
    print _config
    # ConfigWriter(_config).write_config(output_path)
