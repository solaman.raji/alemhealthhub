import ConfigParser
import StringIO

_section_key = 'Session'
_options = ['copies', 'priority', 'mediumtype', 'destination', 'label']

_session_config_base = '''
[Session]
;  -----------------------------------------------------------------------

; optional: Number of Copies. Default: don't send, use SCP default
;
Copies = 1

; optional: Print Priority. Default: don't send, use SCP default.
; Possible values are LOW, MED and HIGH.
Priority = MED

; optional: Medium Type. Default: don't send, use SCP default.
; Defined terms are "PAPER", "BLUE FILM", "CLEAR FILM".
; MediumType = PAPER

MediumTypE = BLUE FILM

; optional: Film Destination. Default: don't send, use SCP default.
; Defined terms are "MAGAZINE", "PROCESSOR", "BIN_1", "BIN_2"...
; Destination = MAGAZINE

Destination = BIN_2

; optional: Film Session Label. Default: don't send, use SCP default.
Label = MY_HARDCOPY

'''


def generate_session_config(**kwargs):
    for key in kwargs.keys():
        assert key in _options

    config = ConfigParser.RawConfigParser(allow_no_value=True)
    config_fp = StringIO.StringIO(_session_config_base)
    config.readfp(config_fp)
    print 'Default config defaults: {}', config.defaults()
    print 'Default config items: {}', config.items(_section_key)

    # config.remove_option('Session', 'priority')

    for option in config.options(_section_key):
        print option
        if option in ['mediumtype', 'destination'] and kwargs.get(option):
            config.set(_section_key, option, kwargs.get(option))
        else:
            config.remove_option(_section_key, option)

    fout = StringIO.StringIO()
    config.write(fout)
    print 'generated session config --'
    print fout.getvalue()
    return fout.getvalue()


if __name__ == '__main__':
    generate_session_config(mediumtype='Paper', destination='BIN_2')
