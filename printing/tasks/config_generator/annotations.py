_annotation_config_base = r'''

[Annotation-1]
Text = \[0010,0020,1] \[0010,0010,1] \[0010,1010,1] \[0010,0040,1]

# Text = \[0010,0020] \[0010,0010] \[0010,1010] \[0010,0040]

#

[Annotation-2]
Text = \[0008,0080,1]

# Text = \[0008,0080]

#

[Annotation-3]
Text = \[0008,0050,1] \[0008,0022,1] \[0008,0030,1]

# Text = \[0008,0050] \[0008,0022] \[0008,0030]
#
'''


# FIXME: add validation to parameter values

def generate_annotations_config(**kwargs):
    # assert 'imagedisplayformat' in kwargs

    return _annotation_config_base


if __name__ == '__main__':
    print generate_annotations_config()
