import ConfigParser
import StringIO

_section_key = 'Page'
_options = ['imagedisplayformat', 'pagesize', 'orientation', 'annotation']

_page_config_base = r'''
[Page]
;  -----------------------------------------------------------------------

; This mandatory setting determines the Image Display Format (page layout).
ImageDisplayFormat = STANDARD\1,1

; optional: Annotation Display Format ID. Default: don't send, use SCP default.
; Possible values are totally printer specific, check conformance statement.
; Annotation = 1

; optional: Orientation. Default: don't send, use SCP default.
; Possible values: PORTRAIT, LANDSCAPE
; Orientation = PORTRAIT

; optional: Film Size ID. Default: don't send, use SCP default.
; Defined terms: 8INX10IN, 8_5INX11IN, 10INX12IN, 10INX14IN, 11INX14IN, 11INX17IN
; 14INX14IN, 14INX17IN, 24CMX24CM, 24CMX30CM, A4, A3
; PageSize = 14INX17IN
; PageSize = 8INX10IN

; PageSize = 14INX17IN

; optional: Magnification Type. Default: don't send, use SCP default.
; Defined terms: REPLICATE, BILINEAR, CUBIC, NONE
; Magnification = CUBIC

; optional: Smoothing Type. Default: don't send, use SCP default.
; Possible values are totally printer specific, check conformance statement.
; Smoothing = 1

; optional: Border Density. Default: don't send, use SCP default.
; Defined terms: BLACK, WHITE or hundreds of OD ("320" => 3.2 OD)
; BorderDensity = BLACK

; optional: Empty Image Density. Default: don't send, use SCP default.
; Defined terms: BLACK, WHITE or hundreds of OD ("320" => 3.2 OD)
; EmptyImageDensity = BLACK

; optional: Trim. Default: don't send, use SCP default.
; Possible values: YES, NO.
; Trim = YES

; optional: Configuration Information. Default: don't send, use SCP default.
; possible values are defined in [[CONFIGURATION INFORMATION]] above.
; ConfigurationInformation =

; optional: minimum density. Default: don't send, use SCP default.
; Defined terms: hundreds of OD ("320" => 3.2 OD)
; MinDensity = 20

; optional: maximum density. Default: don't send, use SCP default.
; Defined terms: hundreds of OD ("320" => 3.2 OD)
; MaxDensity = 320

; optional: illumination. Default: don't send, use SCP default.
; Only used when Presentation LUT is negotiated.
; Illumination = 2000

; optional: reflected ambient light. Default: don't send, use SCP default.
; Only used when Presentation LUT is negotiated.
; ReflectedAmbientLight = 10

'''


# FIXME: add validation to parameter values

def generate_page_config(**kwargs):
    # assert 'imagedisplayformat' in kwargs

    for key in kwargs.keys():
        assert key in _options

    config = ConfigParser.RawConfigParser(allow_no_value=True)
    config.readfp(StringIO.StringIO(_page_config_base))
    print 'Default config defaults: {}', config.defaults()
    print 'Default config items: {}', config.items(_section_key)

    # for option in config.options(_section_key):
    #     print option
    #     if option in _options and kwargs.get(option):
    #         config.set(_section_key, option, kwargs.get(option))
            # else:
            #     config.remove_option(_section_key, option)
    for option in _options:
        if option in kwargs and kwargs.get(option):
            print 'Found ', option
            config.set(_section_key, option, kwargs.get(option))


    fout = StringIO.StringIO()
    config.write(fout)
    print 'generated Page config --'
    print fout.getvalue()
    return fout.getvalue()


if __name__ == '__main__':
    generate_page_config(imagedisplayformat=r'STANDARD\2,2', pagesize='14INX17IN', annotation='')
