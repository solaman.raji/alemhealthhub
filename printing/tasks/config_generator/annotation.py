from collections import OrderedDict, namedtuple

annotation_template = '''
[[ANNOTATION-{number}]]

[General]
Description  = Annotations for CUSTOM-{number} layout

CanonicalName = {number}

[Line-1]
defaulttext  = {line_one_text}

[Line-2]
defaulttext  = Patient: ({patient_name})

'''

# AnnotationConfig = namedtuple('AnnotationConfig', ['annotation_number', 'patient_name'])
# default_annotation_config = AnnotationConfig(1, '<Patient Name>')
#
# def generate_annotation_config(**kwargs):
#     annotation_config = default_annotation_config
#     for key, value in kwargs.iteritems():
#         if key in annotation_config._fields and value:
#             annotation_config._replace(**{key: value})
#     return annotation_config_template.format(**annotation_config._asdict())


class Section(object):
    def __init__(self, name, items=None):
        super(Section, self).__init__()
        self.name = name
        self.items = OrderedDict(items)

    def add_item(self, key, value):
        self.items[key] = value

    def remove_item(self, key):
        self.items.pop(key)

    def __str__(self):
        return "Section: {}".format(self.name)

    def __repr__(self):
        output = u'[[{}]]'.format(self.name)
        output += "\n"
        for key, value in self.items:
            output += "{0}={1}".format(key, value)
            output += "\n"
        return output


class Annotation(object):
    header = 'ANNOTATIONS'

    def __init__(self, sections=None):
        super(Annotation, self).__init__()
        self.sections = sections[:] if sections else []

    def __str__(self):
        return u'Header: {}'.format(self.header)

    def __repr__(self):
        output = u'[[[{}]]]'.format(self.header)
        output += '\n'
        for section in self.sections:
            output += section
        return output


def generate_annotation(number, line_one_text, patient_name):
    annotation = annotation_template.format(number=number, line_one_text=line_one_text, patient_name=patient_name)

    return repr(Annotation([annotation]))