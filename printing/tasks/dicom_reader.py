import os

import dicom


class DicomReader(object):
    # FIXME: Use settings to load this value
    dicom_base_dir = '/var/lib/alemhealthhub/media'

    def __init__(self, dcmfile_path, parent_directory=None):
        super(DicomReader, self).__init__()
        if parent_directory:
            self.dcmfile_path = os.path.join(parent_directory, dcmfile_path)
        else:
            self.dcmfile_path = os.path.join(self.dicom_base_dir, dcmfile_path)
        self.dicom_data = dicom.read_file(self.dcmfile_path)

    @property
    def patient_name(self):
        return self.dicom_data.PatientName

    @property
    def modality(self):
        return self.dicom_data.Modality
