from django.core.management.base import BaseCommand

from printing.tasks import execute_dicom_print_client


class Command(BaseCommand):
    help = 'Run the print scu command in dcmtk'

    def add_arguments(self, parser):
        parser.add_argument('dcmfile_in', type=str)
        parser.add_argument('--config', type=str, required=True)
        parser.add_argument('--auto', type=int)
        parser.add_argument('--verbose', action="store_true", help='Show verbose output from dcmtk command')
        parser.add_argument('--debug', action="store_true", help='Show debug output from dcmtk command')

    def handle(self, *args, **options):
        print 'args:', args
        print 'options: ', options

        dcmfile_in = options.pop('dcmfile_in')

        self.stdout.write('Print DCM file : %s with options %s' % (dcmfile_in, options))

        execute_dicom_print_client(options['config'], dcmfile_in, options['auto'], options['verbose'], options['debug'])

        self.stdout.write(self.style.SUCCESS('Successfully printed study %s' % str(dcmfile_in)))
