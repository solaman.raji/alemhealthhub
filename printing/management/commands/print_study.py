import json
import uuid

from django.core.management.base import BaseCommand

from printing.tasks.print_study import print_study


class Command(BaseCommand):
    help = 'Print the DICOM images for a study'

    def add_arguments(self, parser):
        parser.add_argument('study_guid', type=uuid.UUID)
        parser.add_argument('--session_options', type=json.loads, default={})
        parser.add_argument('--page_options', type=json.loads, default={})
        # parser.add_argument('--annotations', type=list, default={})

    def handle(self, *args, **options):
        print 'args:', args
        print 'options: ', options

        study_guid = options.pop('study_guid')
        session_options = options.pop('session_options')
        page_options = options.pop('page_options')
        annotations = []

        self.stdout.write('Print Study for order: %s' % study_guid)
        self.stdout.write('Print with page options {} and session options: {}'.format(page_options, session_options))

        print_study(study_guid, session_options, page_options, annotations)

        self.stdout.write('Print command executed')

        # load order from DB
        # get information needed for printing
        # generate config for printing
        # run print command

        self.stdout.write(self.style.SUCCESS('Successfully printed study %s' % str(study_guid)))
