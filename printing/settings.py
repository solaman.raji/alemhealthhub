
MEDIA_DIR = '/var/lib/alemhealthhub/media/dicoms/'
PRINT_CLIENT_COMMAND = 'tcpprt_e'

ORIENTATION = ['PORTRAIT', 'LANDSCAPE']

PAGE_SIZE = ['8INX10IN', '8_5INX11IN', '10INX12IN', '10INX14IN',
             '11INX14IN', '11INX17IN',
             '14INX14IN', '14INX17IN',
             '24CMX24CM', '24CMX30CM',
             'A4', 'A3']

