from distutils.core import setup

from setuptools import find_packages
packages = find_packages()

setup(
    name='alemhealthhub',
    version='0.1-dev',
    packages=['order', 'order.management', 'order.management.commands', 'order.migrations', 'alemhealthhub',
              'alemhealthhub.migrations', 'alemhealthhub.templatetags', 'concurrent_transfer'],
    url='http://alemhealth.com',
    license='',
    author='Alemhealth',
    author_email='info@alemhealth.com',
    description='AlemHealth Box project'
)
