import os
import shutil
import random
import logging
import dicom
from time import gmtime, strftime
from order.models import Order, DicomFile
from order.tasks import add_dicom_from_listener, send_dicom_to_alemhealth
from alemhealthhub import settings

logger = logging.getLogger('alemhealth.scripts')

TEST_DICOM_DIR = 'test_dicoms'
TEST_DICOM_PATH = os.path.join(settings.CACHE_DIR, TEST_DICOM_DIR)
SAMPLE_STUDY_NAME = "test_study_1"
SAMPLE_STUDY_SRC_PATH = os.path.join(TEST_DICOM_PATH, SAMPLE_STUDY_NAME)
SAMPLE_PATIENT_NAME = "Test 1"
DICOM_UID_PREFIX = 'ah'
DICOM_UID_GLUE = '_'
DICOM_UID = '1.2.392.200036.9116.2.5.1.16.1623985112.1462687084.604490'
SAMPLE_DICOM_UID = DICOM_UID_PREFIX + DICOM_UID_GLUE + DICOM_UID

if not os.path.exists(SAMPLE_STUDY_SRC_PATH):
    assert False, SAMPLE_STUDY_SRC_PATH + ' doesnt exist'


def update_item(item, replace_element, glue):
    item_split = item.split(glue)
    item_split[len(item_split) - 1] = replace_element
    return glue.join(item_split)


def copy_from_sample_study(test_dicom_path, sample_study_name, new_study_name):
    src_path = os.path.join(test_dicom_path, sample_study_name)
    dst_path = os.path.join(test_dicom_path, new_study_name)
    shutil.copytree(src_path, dst_path)
    return dst_path


def overwrite_dicom_file_meta_data(src_path, patient_name, study_instance_uid):
    for dicom_file in os.listdir(src_path):
        dicom_file_path = os.path.join(src_path, dicom_file)
        dicom_file_obj = dicom.read_file(dicom_file_path)
        dicom_file_obj.PatientName = patient_name
        dicom_file_obj.StudyInstanceUID = study_instance_uid
        dicom_file_obj.save_as(dicom_file_path)


def create_new_study_from_sample_study():
    rand_num = str(random.randint(100000, 999999))
    rand_study_num = ''.join(random.choice('0123456789ABCDEF') for i in range(4))

    dicom_uid = update_item(SAMPLE_DICOM_UID, rand_num, '.')
    study_instance_uid = update_item(DICOM_UID, rand_num, '.')
    study_name = update_item(SAMPLE_STUDY_NAME, rand_study_num, '_')
    patient_name = update_item(SAMPLE_PATIENT_NAME, rand_study_num, ' ')
    dicom_src_path = copy_from_sample_study(TEST_DICOM_PATH, SAMPLE_STUDY_NAME, study_name)
    overwrite_dicom_file_meta_data(dicom_src_path, patient_name, study_instance_uid)

    logger.info("dicom_uid: " + dicom_uid)
    logger.info("study_instance_uid: " + study_instance_uid)
    logger.info("study_name: " + study_name)
    logger.info("patient_name: " + patient_name)
    logger.info("dicom_src_path: " + dicom_src_path)

    return dicom_uid, dicom_src_path, patient_name


def test_add_dicom_from_listener_without_auto_sync():
    dicom_uid, dicom_src_path, patient_name = create_new_study_from_sample_study()
    dicom_dst_path = os.path.join(settings.CACHE_DIR, "dicoms", dicom_uid)
    shutil.copytree(dicom_src_path, dicom_dst_path)
    add_dicom_from_listener.delay(folder_path=dicom_dst_path, auto_sync=False)
    shutil.rmtree(dicom_src_path)


def test_send_dicom_to_alemhealth():
    dicom_uid, dicom_src_path, patient_name = create_new_study_from_sample_study()

    order = Order()
    order.dicom_uid = dicom_uid
    order.patient_name = patient_name
    order.patient_gender = "F"
    order.patient_id = "P-7845"
    order.patient_bodypartexamined = "HAND"
    order.accession_id = "AH-09-16-10151"
    order.modality = "CR"
    order.priority = "Routine"
    order.meta_status = 5
    order.created_at = strftime("%Y-%m-%d %H:%M:%S", gmtime())
    order.save()

    dicom_dst_path = os.path.join(settings.MEDIA_ROOT, "dicoms", str(order.guid))
    shutil.copytree(dicom_src_path, dicom_dst_path)

    for dicom in os.listdir(dicom_dst_path):
        dicom_file = DicomFile()
        dicom_file.file = os.path.join("dicoms", str(order.guid), os.path.basename(dicom))
        dicom_file.order = order
        dicom_file.edit_meta = 1
        dicom_file.save()

    send_dicom_to_alemhealth(order.guid)
    shutil.rmtree(dicom_src_path)


def test_add_dicom_from_listener_with_auto_sync():
    dicom_uid, dicom_src_path, patient_name = create_new_study_from_sample_study()
    dicom_dst_path = os.path.join(settings.CACHE_DIR, "dicoms", dicom_uid)
    shutil.copytree(dicom_src_path, dicom_dst_path)
    add_dicom_from_listener.delay(folder_path=dicom_dst_path)
    shutil.rmtree(dicom_src_path)
