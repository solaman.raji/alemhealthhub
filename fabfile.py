"""
This fabfile is to install and deploy Alemhealth Hub.

Use the following command to install and deploy a new Hub.
    fab hub:host[:port] install deploy [--sudo-password=PASSWORD] [-i key_filename]

Fabric will use 'ubuntu' as default user name to access the host. Provide sudo password if
ubuntu user need password for sudo. Pass the location of private keyfile name if key is
used to access the host.

To install a hub that will connect to production platform and Middleware, pass 'env_type' in hub command

    fab hub:host=host[:port],env_type=prod install deploy

"""
from fabric.api import *
from fabric.operations import *
from fabric.contrib.project import rsync_project
from fabric.contrib.files import exists

import os

abspath = lambda filename: os.path.join(
    os.path.abspath(os.path.dirname(__file__)),
    filename
)


# --------------------------------------------
# Alemhealthhub platform cofiguration
# --------------------------------------------

class FabricException(Exception):
    pass


# edit /etc/security/limit.conf
# According to https://github.com/crs4/ome_seadragon/wiki/Enable-Django-CORS-(Cross-Origin-Resource-Sharing)-Headers-configuration

def hub(host=None, env_type='dev'):
    """host parameter will accept format hostname[:port]"""
    assert env_type in ['dev', 'prod']
    print "Connecting to %s" % host

    env.setup = True
    env.user = 'ubuntu'
    env.ubuntu_version = '16.04'
    env.warn_only = False
    env.abort_exception = FabricException
    # env.shell = "/bin/bash -l -i -c"

    env.hosts = [host]
    env.graceful = False
    env.is_grunt = True
    env.home = '/home/%s' % (env.user)
    env.project = 'alemhealthhub'

    # local config file path
    env.serverpath = '%s/%s' % (env.home, env.project)
    env.virtualenvpath = '%s/virtual-env/%s' % (env.home, env.project)
    env.static_file_path = '%s/static' % (env.serverpath)
    env.config_relative_dir = 'opt/alemhealthhub/conf'

    env.apt_policy_path = 'dev-ops/etc/apt'
    env.systemd_config_path = 'dev-ops/etc/systemd'
    env.supervisor_file_path = 'dev-ops/supervisor'
    env.supervisor_config_path = '/etc/supervisor/conf.d'
    env.dcmtk_listener = '%s/dcmtk_listener.conf' % (env.supervisor_file_path)
    env.uwsgi_config = '%s/uwsgi.conf' % (env.supervisor_file_path)
    env.flower_config = '%s/flower.conf' % (env.supervisor_file_path)
    env.celery_config = '%s/celeryd.conf' % (env.supervisor_file_path)
    env.celerybeat_config = '%s/celerybeat.conf' % (env.supervisor_file_path)
    env.dcmqrscp_config = '%s/dcmqrscp.conf' % (env.supervisor_file_path)
    env.config_src_path = os.path.join('dev-ops', env.config_relative_dir, env_type + '-config.json')
    env.config_dest_path = os.path.join('/', env.config_relative_dir, 'config.json')
    env.db_path = '/var/lib/alemhealthhub/data/hub.dat'

    env.nginx_config = 'nginx.conf'
    env.dcmqrscp_cfg_file = 'dev-ops/supervisor/dcmqrscp.cfg'

    env.rsync_exclude = [
        '*/.DS_Store*',
        '*/.readme.md.*',
        'node_modules',
        'static/bower_components',
        'alemhealthhub/local_settings.py',
        '*.pyc',
        'log',
        'celerybeat-schedule.db',
        'celerybeat.pid',
        'celeryev.pid',
        'db.sqlite3',
        '*.pem',
        'dicoms/*',
        'virtual-env',
        '*/migrations',
        '*.pem',
        'fab*',
        'sample_dcmqrscp.cfg',
        'dev-ops',
        'vagrant',
    ]

    return


def create_app_directories():
    directories = '/var/{log,cache,lib,backups,www}/alemhealthhub /opt/alemhealthhub'
    sudo('mkdir -vp %s' % directories)
    sudo('chown -R %s %s' % (env.user, directories))

    nested_directories = [
        '/var/lib/alemhealthhub/{data,media,worklist}',
        '/var/cache/alemhealthhub/{dicoms,tmp,worklist}',
        '/opt/alemhealthhub/{dcmtk,conf,viewer}',
        '/var/log/alemhealthhub/{dcmtk,wlmscpfs,celery}',
    ]
    for directory in nested_directories:
        run('mkdir -vp %s' % directory)

    sudo('chown -R %s:www-data /var/www/alemhealthhub' % (env.user))
    sudo('usermod -a -G www-data %s' % (env.user))
    sudo('chown %s:www-data /var/www' % (env.user))
    sudo('chmod -R 2770 /var/www')

    run('mkdir -p /tmp/%s' % env.project)


def install():
    update()
    install_python_dependency()
    install_pip()
    create_app_directories()
    install_virtualenv()
    install_rabbitmq()
    install_dcmtk()
    install_zpaq()
    install_supervisor()
    create_log_file()
    install_nodejs()
    copy_dev_ops_files()
    install_nginx()
    config_nginx()
    install_mkisofs()
    return


def update():
    print 'Start updating the system'
    sudo('apt-get update')


def install_python_dependency():
    print "Installing python dependency packages"
    sudo(
        'apt-get -y install '
        'build-essential python-setuptools '
        'python-dev make automake gcc '
        'libxml2 libxml2-dev libxslt-dev libtidy-dev python-lxml '
        'htop iftop sqlite3'
    )

    sudo(
        'apt-get install -y '
        'libjpeg62 zlib1g-dev libjpeg8-dev '
        'libjpeg-dev libfreetype6-dev '
        'libpng-dev libgif-dev'
    )
    sudo('apt-get -y install libcurl4-gnutls-dev librtmp-dev')
    sudo('apt-get install -y libjpeg-turbo8-dev')
    sudo('apt-get install -y libjpeg62-dev')


def install_pip():
    print "Installing python pip in the system"
    sudo('curl -O https://bootstrap.pypa.io/get-pip.py')
    sudo('python get-pip.py')
    sudo('pip install -U pip')


def install_virtualenv():
    print "Installing python virtualenv"
    sudo('pip install virtualenv virtualenvwrapper')
    run('mkdir -p %s/virtual-env' % env.home)
    print "Creating alemhealthhub virtualenv"
    run('virtualenv %s' % env.virtualenvpath)
    print "Virtualenv installed"


def install_pip_requirements():
    print "Installing pip requirements"
    with cd(env.serverpath), prefix('source %s/bin/activate' % env.virtualenvpath):
        run('pip install -r %s/requirement.txt' % env.project)


def install_rabbitmq():
    print 'Installing rabbitMQ'

    print 'Add repository key and apt source list for erlang'
    sudo('echo "deb https://packages.erlang-solutions.com/ubuntu xenial contrib" | tee /etc/apt/sources.list.d/erlang-solutions.erlang.list')
    sudo('wget --no-check-certificate -O- https://packages.erlang-solutions.com/ubuntu/erlang_solutions.asc | sudo apt-key add -')

    print 'Add repository key and apt source for RabbitMQ'
    sudo('echo "deb https://dl.bintray.com/rabbitmq/debian xenial main" | tee /etc/apt/sources.list.d/bintray.rabbitmq.list')
    sudo('wget --no-check-certificate -O- https://www.rabbitmq.com/rabbitmq-release-signing-key.asc | sudo apt-key add -')

    print 'Erlang Version Pinning: Copy apt preference file'
    put('%s' % (env.apt_policy_path), '/etc/', use_sudo=True)

    update()

    sudo('apt-get install -y rabbitmq-server')

    print 'Add systemd files for rabbitMQ'
    put('%s' % (env.systemd_config_path), '/etc/systemd', use_sudo=True)
    sudo('systemctl daemon-reload')

    sudo('systemctl enable rabbitmq-server.service')
    sudo('systemctl start rabbitmq-server.service')


def install_dcmtk():
    print 'Installing dcmtk'
    sudo('apt-get install -y dcmtk')


def install_zpaq():
    print 'Installing  zpaq'
    sudo('apt-get -y install zpaq')


def install_supervisor():
    print 'Installing Supervisor'
    sudo('apt-get -y install supervisor')
    print 'Configuring supervisor config files'

    print 'Copy supervisor conf files'
    put('%s/*.conf' % (env.supervisor_file_path), env.supervisor_config_path, use_sudo=True)


def install_nodejs():
    print 'Installing latest nodejs '
    run('cd %s;curl -sL https://deb.nodesource.com/setup_6.x -o nodesource_setup.sh' % (env.home))
    sudo('cd %s; bash nodesource_setup.sh' % (env.home))
    sudo('apt-get -y install nodejs build-essential')
    sudo('npm -g install bower')
    sudo('npm -g install grunt-cli')
    sudo('rm %s/nodesource_setup.sh' % (env.home))


def install_nginx():
    print 'Installing NGINX'
    sudo("apt-get install -y nginx")
    sudo('systemctl enable nginx')
    default_config = '/etc/nginx/sites-enabled/default'
    if exists(default_config):
        sudo('rm %s' % (default_config))
        print 'Deleted NGINX default config'
    sudo('systemctl enable nginx.service')
    sudo('systemctl enable nginx.service')


def create_log_file():
    print "Creating log files for project"
    privileged_log_files = '/var/log/{uwsgi,dcmtk_listener,dcmqrscp,dcmnet,storescp,flower}.log'
    celery_log_files = '/var/log/alemhealthhub/celery/{celeryd,celerybeat,celeryd_reporting,celeryd_study}.log'

    sudo('touch %s' % privileged_log_files)
    sudo('chown ubuntu:root %s' % privileged_log_files)

    run('touch %s' % celery_log_files)


# --------------------------------------------
# Deploying Alemhealth hub
# --------------------------------------------

def deploy():
    print 'Starting to deploy Alemhealthhub'
    sync_code_base()
    install_pip_requirements()
    if not exists(env.config_dest_path, verbose=True):
        print 'Copying %s as config to %s' % (env.config_src_path, env.config_dest_path)
        put(env.config_src_path, env.config_dest_path)
    migrate()
    install_npm_packages()
    collect_static_files()

    print 'Restarting supervisor'
    restart_supervisor()
    print 'Finished the deployment of Alemhealthhub: '
    return


def sync_code_base():
    print 'Syncing Alemhealthhub code base'
    rsync_project(env.serverpath, abspath('') + '*', exclude=env.rsync_exclude, delete=True, default_opts='-rvz')
    put('%s' % (env.dcmqrscp_cfg_file), env.serverpath)

    # copy viewer folder
    print 'Copy viewer folder'
    rsync_project("/opt/alemhealthhub/viewer/",
                  abspath('dev-ops/opt/alemhealthhub/viewer/'),
                  delete=True,
                  default_opts='-rvz')

    # sudo('usermod -a -G www-data %s' %(env.user))
    # sudo('chown -R %s:%s %s' %(env.user, env.user, env.serverpath))
    # sudo('chown %s:www-data /var/www' %(env.user))
    # sudo('chmod -R 2770 /var/www')
    print 'code sync is complete'


def install_npm_packages():
    print 'Installing npm packages'
    run('cd %s;npm install' % (env.serverpath))
    run('cd %s; bower install' % (env.static_file_path))
    run('cd %s; grunt dev' % (env.serverpath))


def migrate():
    print 'Migrate database'
    with cd(env.serverpath), prefix('source %s/bin/activate' % env.virtualenvpath):
        run('python manage.py makemigrations')
        run('python manage.py makemigrations alemhealthhub order mwl_broker')
        run('python manage.py migrate')
        run('python manage.py runscript create_schedule_task --traceback')


def collect_static_files():
    print 'Collect static files to web directory'
    with cd(env.serverpath), prefix('source %s/bin/activate' % env.virtualenvpath):
        run('python manage.py collectstatic --noinput -i bower_components')


def copy_dev_ops_files():
    print 'copy dev-ops files'
    put('dev-ops/opt', '/')


def config_nginx():
    print 'Configuring NGINX'
    default_config = '/etc/nginx/sites-enabled/nginx.conf'
    if exists(default_config):
        sudo('rm /etc/nginx/sites-enabled/nginx.conf')
        print 'Deleted NGINX default config'

    print 'Install NGINX config'
    put('%s' % (env.nginx_config), '/etc/nginx/sites-enabled/', use_sudo=True)
    sudo('nginx -t')
    sudo('systemctl restart nginx')


def restart_supervisor():
    print 'Restarting supervisor services'
    sudo('supervisorctl stop all', shell=False)
    sudo('supervisorctl reread all', shell=False)
    sudo('supervisorctl update all', shell=False)
    sudo('supervisorctl start all', shell=False)
    sudo('supervisorctl status')


def install_mkisofs():
    print "Installing mkisofs"
    sudo("apt-get install -y mkisofs")


def update_report_header(header_image):
    """Replace report header image for customer. Run this after a deployment"""
    assert '.png' in header_image, "png image needed"
    with cd('%s/assets/images' % env.static_file_path):
        if not exists(header_image):
            print 'Header image %s not found in static directory' % header_image
            return 1
        run('cp -v %s header.png' % header_image)
    collect_static_files()
