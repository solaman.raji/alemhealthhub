# README #

### What is this repository for? ###

* Alemhealthhub

### Setup ###

* install pip
* install virtualenv via pip
* activate the virtualenv
* pip install -r alemhealthhub/requirement.txt
* then npm install,bower install
* grunt dev,python manage.py runserver

### Acceptance Testing Setup ##

Run the following steps in your shell to download demo studies need to run tests

    cd /alemhealthhub ; To location where you have alemhealthhub code in vagrant/hub instance
    source scripts/create_test_dicoms.sh

### Run Acceptance Testing ##

    cd /alemhealthhub
    DJANGO_SETTINGS_MODULE=alemhealthhub.settings pytest acceptance_test
