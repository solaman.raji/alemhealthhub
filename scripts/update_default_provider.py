"""
Run this script from your console using runscript extension
python manage.py runscript update_default_provider --script-args provider_guid --traceback
"""

import json
import logging
from uuid import UUID

from django.core.exceptions import ObjectDoesNotExist

from alemhealthhub.models import Config

logger = logging.getLogger("alemhealth.scripts")

SETTINGS_KEY = "settings"
HOSPITAL_KEY = "hospital"


def is_box():
    try:
        Config.objects.get(key=HOSPITAL_KEY)
    except ObjectDoesNotExist:
        return True
    except Exception:
        return True
    return False


def is_setting_available():
    try:
        return json.loads(Config.objects.get(key=SETTINGS_KEY).value)
    except ObjectDoesNotExist:
        return False
    except Exception :
        return False


def validate_uuid4(uuid_string, guid_type):
    try:
        guid = UUID(uuid_string, version=4).hex
    except ValueError as e:
        err_msg = "{0} {1}, {2}".format(guid_type, uuid_string, e)
        logger.error(err_msg)
        raise ValueError(err_msg)
    except Exception as e:
        err_msg = "Unexpected error: {0}".format(e)
        logger.error(err_msg)
        raise Exception(err_msg)
    return guid


def update_default_provider(data, provider_guid):
    try:
        data["provider"]["default"] = provider_guid

        try:
            config_settings = Config.objects.get(key=SETTINGS_KEY)
            config_settings.value = json.dumps(data)
            config_settings.save()
        except ObjectDoesNotExist:
            err_msg = "{0} key does not exist".format(SETTINGS_KEY)
            logger.error(err_msg)
            raise ObjectDoesNotExist(err_msg)
        except Exception as e:
            err_msg = "Unexpected error: {0}".format(e)
            logger.error(err_msg)
            raise Exception(err_msg)
    except KeyError as e:
        err_msg = "{0} not found".format(e)
        logger.error(err_msg)
        raise KeyError(err_msg)
    except Exception as e:
        err_msg = "Unexpected error: {0}".format(e)
        logger.error(err_msg)
        raise Exception(err_msg)


def run(*args):
    logger.info("args: {0}".format(args))

    if not len(args):
        logger.error("provider_guid is not given. please pass valid provider_guid as first argument")
        raise Exception("provider_guid is not given. please pass valid provider_guid as first argument")

    provider_guid = args[0]
    logger.info("provider_guid: {0}".format(provider_guid))

    default_provider_guid = validate_uuid4(provider_guid, "provider_guid")
    logger.info("default_provider_guid: {0}".format(default_provider_guid))

    if is_box():
        settings_data = is_setting_available()

        if settings_data:
            update_default_provider(settings_data, provider_guid)
        else:
            logger.error("Cannot update default provider. Device settings data not available")
    else:
        logger.error("Cannot update default provider. This device is not registered as box.")

