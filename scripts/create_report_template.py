import json
from django.test import RequestFactory
from alemhealthhub.api import ReportView as ReportTemplateView


# Create Report Template
def create_report_template():
    # Default Data
    url = "api/reporttemplate/"
    data = {
      "report_name": "Report 4",
      "procedure": "Precedure 1",
      "clinical_information": "<p>Clinical Information 1</p>",
      "comparison": "<p>Comparison 1</p>",
      "findings": "<p>Findings 1</p>",
      "impression": "<p>Impression 1</p>"
    }

    # Create Report Template Using Request Factory
    request_factory_object = RequestFactory()
    request = request_factory_object.post(url, content_type="application/json")
    request._body = json.dumps(data)
    ReportTemplateView.as_view()(request)


def run():
    create_report_template()
