#!/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/home/ubuntu
service=rabbitmq
host=`hostname -f`

if  (( $(ps -ef | grep -v grep | grep $service | wc -l) < 4 ))
then
subject="RabbitMQ at $host is down"
echo "RabbitMq is Down!" | mail -s "$subject" sajjad@alemcloud.com, sc@inov.io, solaman@alemcloud.com > /dev/null 2>&1
fi