"""
Run this script from your console using runscript extension
python manage.py runscript registration --script-args box/hub --traceback
"""

import json
import logging
from uuid import UUID

import os
from django.test import RequestFactory

from alemhealthhub.api import BoxSetup, ConfigView
from alemhealthhub.models import Config
from alemhealthhub.settings import OPT_DIR

logger = logging.getLogger("alemhealth.scripts")


def register_provider(name, provider_access_key, username, password):
    url = "/api/boxsetup"
    data = {
        "config": {
            "first_name": name,
            "guid": provider_access_key,
            "username": username,
            "password": password,
            "retype_password": password
        }
    }

    request_factory_object = RequestFactory()
    request = request_factory_object.post(url, content_type="application/json")
    request._body = json.dumps(data)
    BoxSetup.as_view()(request)


def register_facility(facility_access_key):
    url = "/api/config"
    data = {
        "config": {
            "facility_access_key": facility_access_key
        }
    }

    request_factory_object = RequestFactory()
    request = request_factory_object.put(url, content_type="application/json")
    request._body = json.dumps(data)
    ConfigView.as_view()(request)


def remove_registration_data(device_config):
    if "provider" in device_config and "registration" in device_config["provider"]:
        device_config["provider"].pop("registration", None)

    if "facility" in device_config:
        device_config.pop("facility", None)


def save_settings_data(data):
    try:
        config, created = Config.objects.get_or_create(key="settings")
        config.value = json.dumps(data)
        config.save()
    except Exception as e:
        err_msg = "Unexpected error: {0}".format(e)
        logger.error(err_msg)
        raise Exception(err_msg)


def validate_uuid4(uuid_string, guid_type):
    try:
        guid = UUID(uuid_string, version=4).hex
    except ValueError as e:
        err_msg = "{0} {1}, {2}".format(guid_type, uuid_string, e)
        logger.error(err_msg)
        raise ValueError(err_msg)
    except Exception as e:
        err_msg = "Unexpected error: {0}".format(e)
        logger.error(err_msg)
        raise Exception(err_msg)
    return guid


def run(*args):
    logger.info("args: {0}".format(args))

    if not len(args):
        logger.error("device type is not given. please pass either box or hub as argument")
        raise Exception("device type is not given. please pass either box or hub as argument")

    device_type = args[0].lower()
    logger.info("device_type: {0}".format(args[0]))

    if device_type != "box" and device_type != "hub":
        logger.error("invalid device type. please pass either box or hub as argument")
        raise Exception("invalid device type. please pass either box or hub as argument")

    filename = device_type + "-config.json"
    logger.info("filename: {0}".format(filename))

    file_path = os.path.join(OPT_DIR, "conf", filename)
    logger.info("filepath: {0}".format(file_path))

    try:
        with open(file_path) as config_file:
            device_config = json.load(config_file)
            logger.info("device_config: {0}".format(device_config))
    except IOError as e:
        err_msg = "I/O error({0}): {1} {2}".format(e.errno, e.strerror, e.filename)
        logger.error(err_msg)
        raise Exception(err_msg)
    except Exception as e:
        err_msg = "Unexpected error: {0}".format(e)
        logger.error(err_msg)
        raise Exception(err_msg)

    try:
        provider_name = device_config["provider"]["registration"]["name"]
        logger.info("provider_name: {0}".format(provider_name))

        provider_guid = device_config["provider"]["registration"]["guid"]
        logger.info("provider_guid: {0}".format(provider_guid))

        provider_username = device_config["provider"]["registration"]["username"]
        logger.info("provider_username: {0}".format(provider_username))

        provider_password = device_config["provider"]["registration"]["password"]
        logger.info("provider_password: {0}".format(provider_password))

        facility_guid = device_config["facility"]["registration"]["guid"]
        logger.info("facility_guid: {0}".format(facility_guid))
    except KeyError as e:
        err_msg = "{0} not found".format(e)
        logger.error(err_msg)
        raise KeyError(err_msg)
    except Exception as e:
        err_msg = "Unexpected error: {0}".format(e)
        logger.error(err_msg)
        raise Exception(err_msg)

    provider_access_key = validate_uuid4(provider_guid, "provider_guid")
    logger.info("provider_access_key: {0}".format(provider_access_key))

    facility_access_key = validate_uuid4(facility_guid, "facility_guid")
    logger.info("facility_access_key: {0}".format(facility_access_key))

    register_provider(provider_name, provider_access_key, provider_username, provider_password)
    register_facility(facility_access_key)
    remove_registration_data(device_config)
    save_settings_data(device_config)
