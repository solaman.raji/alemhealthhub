"""
Run this script from your console using runscript extension
python manage.py runscript sync_study_to_hub  --script-args dicom_folder --traceback
"""

import os
import logging
import sys
import django
from alemhealthhub.settings import CACHE_DIR
from order.tasks import add_dicom_from_listener

django.setup()
logger = logging.getLogger('alemhealth.scripts')


def sync_study_to_hub(folder_path):
    add_dicom_from_listener.delay(folder_path=folder_path)


def run(*args):
    if not len(args):
        logger.error('Dicom folder name is not given.')
        raise Exception('Dicom folder name is not given.')

    _dicom_folder = args[0]
    _dicom_folder_path = os.path.join(CACHE_DIR, "dicoms", _dicom_folder)
    logger.info('Dicom folder: {} and path: {}'.format(_dicom_folder, _dicom_folder_path))

    sync_study_to_hub(_dicom_folder_path)


if __name__ == '__main__':
    run(sys.argv)
