import os, shutil, sys
from datetime import datetime


def rmtree_op(parent_dir_path, dir_name):
    dir_path = os.path.join(parent_dir_path, dir_name)

    if os.path.isdir(dir_path):
        try:
            shutil.rmtree(dir_path)
            print "Directory %s removed successfully from %s" % (dir_name, parent_dir_path)
        except Exception as e:
            print "Exception occurs. Directory %s cannot be removed from: %s. Reason: %s" % (dir_name, parent_dir_path, e)


def copytree_op(src_path, dst_path, dir_name):
    src_dir_path = os.path.join(src_path, dir_name)
    dst_dir_path = os.path.join(dst_path, dir_name)

    try:
        shutil.copytree(src_dir_path, dst_dir_path)
        print "Directory %s copied to: %s" % (dir_name, dst_path)
    except Exception as e:
        print "Exception occurs. Directory %s cannot be moved to: %s. Reason: %s" % (dir_name, dst_path, e)


def backup_op(date_search, src_path, dst_path):
    date_search = datetime.strptime(date_search, "%Y-%m-%d").date()
    dir_list = [d for d in os.listdir(src_path) if os.path.isdir(os.path.join(src_path, d))]
    print "\n"
    print "Total directory count: %s" % (len(dir_list))
    print 'Directory list: %s' % (dir_list)

    i = 0;
    for dir_name in dir_list:
        print "\n"
        print "Working with directory: %s" % (dir_name)
        dir_mdate = datetime.fromtimestamp(os.path.getmtime(src_path+'/'+dir_name)).date()
        print "Directory %s last modified date: %s" % (dir_name, dir_mdate)

        if dir_mdate <= date_search:
            print "Directory %s is in the date range." % (dir_name)
            i += 1

            # If exists, remove directory from destination path
            rmtree_op(dst_path, dir_name)

            # Copy directory from source path to destination path
            copytree_op(src_path, dst_path, dir_name)

            # If exists, remove directory from source path
            rmtree_op(src_path, dir_name)
        else:
            print "Directory %s is not in the date range. It will not be moved." % (dir_name)

    print "\n"
    print "Total directory moved: %s" % (i)


if __name__ == '__main__':
    argv_indexs = range(0, len(sys.argv))

    if 1 in argv_indexs:
        date_search = sys.argv[1]
        print "Search date: %s. Taken from arguments." % (date_search)
    else:
        date_search = "2017-07-27"
        print "Search date: %s. Taken from defaults." % (date_search)

    if 2 in argv_indexs:
        src_path = sys.argv[2]
        print "Source path: %s. Taken from arguments." % (src_path)
    else:
        src_path = "/Users/inovio/projects/python_test/directory_operation/dir_op_src"
        print "Source path: %s. Taken from defaults." % (src_path)

    if 3 in argv_indexs:
        dst_path = sys.argv[3]
        print "Destination path: %s. Taken from arguments." % (dst_path)
    else:
        dst_path = "/Users/inovio/projects/python_test/directory_operation/dir_op_dst"
        print "Destination path: %s. Taken from defaults." % (dst_path)

    backup_op(date_search, src_path, dst_path)
