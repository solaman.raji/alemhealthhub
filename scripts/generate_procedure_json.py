"""
This script will parse radiopaedia playbook csv and will generate json objects for procedure list.

Run the script using the following command

    $ python generate_procedure_json.py procedure-list.csv
"""
import csv
import sys


def read_csv(filename):
    with open(filename) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            yield row


def generate_procedures(input_file, output_file):
    """
    Generate a json array like below
    [
       { value: 'RPID123', modality: 'CT', etc},
       { value: 'RPID153', modality: 'CT', etc},
    ]
    """
    modality_list = ['CR', 'CF', 'CT', 'NM', 'MR', 'DS', 'DR', 'US', 'HSG', "OP", "MG"]
    json_output_template = "  {{ value: '{RPID}', modality: '{MODALITY}', bodypart: '{BODY_REGION}', label: '{AUTOMATED_SHORT_NAME}' }},"
    with open(output_file, 'wb') as fout:
        fout.write('[\n')
        for row in read_csv(input_file):
            if row['MODALITY'] in modality_list:
                fout.write(json_output_template.format(**row))
                fout.write('\n')
        fout.write(']')


if __name__ == '__main__':
    if len(sys.argv) == 1:
        print 'Filename parameter is missing'
        exit(1)
    input_filename = sys.argv[1]
    output_file = 'procedures.json'
    generate_procedures(input_filename, output_file)
    print 'Done!'
