import os
from django.test import RequestFactory
from django.core.files.uploadedfile import SimpleUploadedFile
from django.core.files import File
from alemhealthhub import settings
from order.api import ReportHeaderView


# Upload Header
def upload_header(url, file_path):
    filename = os.path.basename(file_path)
    file_obj = File(open(file_path, 'rb'))
    uploaded_file = SimpleUploadedFile(filename, file_obj.read(), content_type='multipart/form-data')
    request_factory_object = RequestFactory()
    request = request_factory_object.post(url, data={"file": uploaded_file}, format="multipart")
    return ReportHeaderView.as_view()(request)


def run():
    # Header Upload URL
    url = "/api/reportheader/"

    # Logo File Path
    filename = "dummy_alemhealth_logo.png"
    file_path = os.path.join(settings.BASE_DIR, "static", "assets", "images", filename)

    upload_header(url, file_path)
