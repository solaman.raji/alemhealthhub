import json
from django.test import RequestFactory
from order.api import ReportView


# Submit Report
def submit_report(order_guid):
    # Default Data
    url = "api/report/" + order_guid
    data = {
        "order_guid": order_guid,
        "report": "<div style='text-align: center;'><u><b>Procedure 1</b></u></div><div style='text-align: left;'><b>Clinical Information:</b></br>Clinical Information 1</br></div><div style='text-align: left;'><b>Comparison:</b></br>Comparison 1</br></div><div style='text-align: left;'><b>Findings:</b></br>Findings 1</br></div><div style='text-align: left;'><b>Impression:</b></br>Impression 1</br></div>"
    }

    # Submit Report Using Request Factory
    request_factory_object = RequestFactory()
    request = request_factory_object.put(url, content_type="application/json")
    request._body = json.dumps(data)
    ReportView.as_view()(request)


def run():
    # Order GUID
    _order_guid = "234f624702a9408b9d1bad0658573102"
    submit_report(_order_guid)
