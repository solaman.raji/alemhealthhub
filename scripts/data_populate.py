import json
from django.test import RequestFactory
from alemhealthhub.api import BoxSetup, ConfigView
from alemhealthhub.utils import load_settings_from_config_file

'''
Provide facility and provider guid in settings file like the command below

cat > /tmp/hub_access_keys.json << EOL
{
    "provider_guid": "65491ecc5a1f4543a82f9df38953b899",
    "facility_guid": "7e8d4b4094034453a41d0b595e2cd402"
}
EOL

'''

# Register Provider
def register_provider(provider_access_key):
    # Default Data
    url = "/api/boxsetup"
    data = {
        "config": {
            "first_name": "Provider1",
            "guid": provider_access_key,
            "username": "admin",
            "password": "qweqwe",
            "retype_password": "qweqwe"
        }
    }

    # Register Provider Using Request Factory
    request_factory_object = RequestFactory()
    request = request_factory_object.post(url, content_type="application/json")
    request._body = json.dumps(data)
    print 'Request provider setup'
    response = BoxSetup.as_view()(request)
    print 'Response status: %s %s' % (response.status_code, response.status_text)


# Register Facility
def register_facility(facility_access_key):
    # Default Data
    url = "/api/config"
    data = {
        "config": {
            "facility_access_key": facility_access_key
        }
    }
    # Register Facility Using Request Factory
    request_factory_object = RequestFactory()
    request = request_factory_object.put(url, content_type="application/json")
    request._body = json.dumps(data)
    print 'Request facility setup'
    response = ConfigView.as_view()(request)
    print 'Response status: %s %s' % (response.status_code, response.status_text)


def run():
    # Run the following command to use Test Hub and Test access key available in produciton
    # cp -v alemhealthhub/scripts/hub_access_keys.json /tmp/hub_access_keys.json

    config = {'provider_guid': None, 'facility_guid': None}
    config.update(load_settings_from_config_file('/tmp/hub_access_keys.json', silent=False))
    print 'Hub Access keys: ', config
    register_provider(config['provider_guid'])
    register_facility(config['facility_guid'])
