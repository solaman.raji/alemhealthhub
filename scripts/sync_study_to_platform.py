"""
Run this script from your console using runscript extension

    python manage.py runscript sync_study_to_platform  --script-args 2017-11-17 --traceback
"""

import logging
import os
import sys
import subprocess

import django

from order.models import Order
from order.tasks import send_dicom_to_alemhealth

django.setup()
logger = logging.getLogger('alemhealth.scripts')

MEDIA_DIR = '/var/lib/alemhealthhub/media/dicoms'


def get_orders_with_status(_meta_status, _date_prefix):
    query = Order.objects.filter(meta_status=_meta_status).filter(created_at__startswith=_date_prefix).filter(
        ah_order_guid=None)

    guids = []
    for study in query:
        logger.info('{} {} {} {}'.format(study.guid, study.created_on, study.created_at, study.ah_order_guid))
        guids.append(study.guid.hex)

    return guids


def get_study_directory(study_guid):
    study_dir = os.path.join(MEDIA_DIR, study_guid, '')
    logger.info('Show study directory: {}'.format('du -hs {}'.format(study_dir)))
    output = subprocess.check_output('du -hs {}'.format(study_dir), shell=True)
    print output
    return study_dir


def run(*args):
    if not len(args):
        logger.error('Date prefix is not given.')
        raise Exception('Set the date to use')
    _meta_status = 5
    _date_prefix = args[0]
    logger.info('Get orders with meta status: {} and from date: {}'.format(_meta_status, _date_prefix))
    guids = get_orders_with_status(_meta_status, _date_prefix)
    for guid in guids:
        try:
            logger.info('Send order to platform for guid: {}'.format(guid))
            get_study_directory(guid)
            send_dicom_to_alemhealth(guid)
            logger.info('Order {} is sent to platform'.format(guid))
        except Exception as ex:
            logger.exception(ex)


if __name__ == '__main__':
    run(sys.argv)
