import os
import shutil
from alemhealthhub.settings import BASE_DIR, OPT_DIR


def copy_config_file(device_type):
    filename = device_type.lower() + '-config.json'
    src_path = os.path.join(BASE_DIR, 'dev-ops', 'opt', 'alemhealthhub', 'conf', filename)
    dst_path = os.path.join(OPT_DIR, 'conf', filename)

    if os.path.isfile(src_path):
        shutil.copy(src_path, dst_path)


def run():
    copy_config_file('box')
    copy_config_file('hub')
    copy_config_file('facility')
    copy_config_file('provider')
