"""
Run this script from your console using runscript extension
python manage.py runscript register_provider_new --script-args provider_guid --traceback
"""

import json
import logging
from uuid import UUID

from django.test import RequestFactory
from alemhealthhub.api import BoxSetup

logger = logging.getLogger("alemhealth.scripts")


def register_provider(name, provider_access_key, username, password):
    url = "/api/boxsetup"
    data = {
        "config": {
            "first_name": name,
            "guid": provider_access_key,
            "username": username,
            "password": password,
            "retype_password": password
        }
    }

    request_factory_object = RequestFactory()
    request = request_factory_object.post(url, content_type="application/json")
    request._body = json.dumps(data)
    BoxSetup.as_view()(request)


def validate_uuid4(uuid_string, guid_type):
    try:
        guid = UUID(uuid_string, version=4).hex
    except ValueError as e:
        err_msg = "{0} {1}, {2}".format(guid_type, uuid_string, e)
        logger.error(err_msg)
        raise ValueError(err_msg)
    except Exception as e:
        err_msg = "Unexpected error: {0}".format(e)
        logger.error(err_msg)
        raise Exception(err_msg)
    return guid


def run(*args):
    logger.info("args: {0}".format(args))

    if not len(args):
        logger.error("provider_guid is not given. please pass valid provider_guid as first argument")
        raise Exception("provider_guid is not given. please pass valid provider_guid as first argument")

    provider_name = ""
    provider_username = "admin"
    provider_password = "qweqwe"

    logger.info("provider_name: {0}".format(provider_name))
    logger.info("provider_username: {0}".format(provider_username))
    logger.info("provider_password: {0}".format(provider_password))

    provider_guid = args[0]
    logger.info("provider_guid: {0}".format(provider_guid))

    provider_access_key = validate_uuid4(provider_guid, "provider_guid")
    logger.info("provider_access_key: {0}".format(provider_access_key))

    register_provider(provider_name, provider_access_key, provider_username, provider_password)
