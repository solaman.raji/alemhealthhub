"""
Run this script from your console using runscript extension
python manage.py runscript register_facility_new --script-args facility_guid --traceback
"""

import json
import logging
from uuid import UUID

from django.db import IntegrityError
from django.test import RequestFactory
from alemhealthhub.api import ConfigView
from alemhealthhub.models import User

logger = logging.getLogger("alemhealth.scripts")


def register_facility(facility_access_key):
    url = "/api/config"
    data = {
        "config": {
            "facility_access_key": facility_access_key
        }
    }

    request_factory_object = RequestFactory()
    request = request_factory_object.put(url, content_type="application/json")
    request._body = json.dumps(data)
    ConfigView.as_view()(request)


def create_user(name, username, password):
    try:
        user = User()
        user.first_name = name
        user.username = username
        user.set_password(password)
        user.is_staff = True
        user.is_superuser = True
        user.save()
    except IntegrityError:
        logger.error("Username '{0}' already exists".format(username))
    except Exception as e:
        logger.error("Unexpected error: {0}".format(e))
        raise Exception("Unexpected error: {0}".format(e))


def validate_uuid4(uuid_string, guid_type):
    try:
        guid = UUID(uuid_string, version=4).hex
    except ValueError as e:
        err_msg = "{0} {1}, {2}".format(guid_type, uuid_string, e)
        logger.error(err_msg)
        raise ValueError(err_msg)
    except Exception as e:
        err_msg = "Unexpected error: {0}".format(e)
        logger.error(err_msg)
        raise Exception(err_msg)
    return guid


def run(*args):
    logger.info("args: {0}".format(args))

    if not len(args):
        logger.error("facility_guid is not given. please pass valid facility_guid as first argument")
        raise Exception("facility_guid is not given. please pass valid facility_guid as first argument")

    facility_name = ""
    facility_username = "admin"
    facility_password = "qweqwe"

    logger.info("facility_name: {0}".format(facility_name))
    logger.info("facility_username: {0}".format(facility_username))
    logger.info("facility_password: {0}".format(facility_password))

    facility_guid = args[0]
    logger.info("facility_guid: {0}".format(facility_guid))

    facility_access_key = validate_uuid4(facility_guid, "facility_guid")
    logger.info("facility_access_key: {0}".format(facility_access_key))

    register_facility(facility_access_key)
    create_user(facility_name, facility_username, facility_password)

