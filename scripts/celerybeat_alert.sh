#!/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/home/ubuntu
status=$(sudo supervisorctl status celerybeat | grep -o FATAL)
if [ "$status" == "FATAL" ]
 then
    echo "Crestview celerybeat is Down!" | mail -s "CrestView Down" sajjad@alemcloud.com, sc@inov.io, solaman@alemcloud.com > /dev/null 2>&1
    sudo supervisorctl restart all > /dev/null 2>&1
    status=$(sudo supervisorctl status celerybeat | grep -o RUNNING)
    if [ "$status" == "RUNNING" ]
     then
      echo "Crestview celerybeat is up!" | mail -s "CrestView Up" sajjad@alemcloud.com, sc@inov.io, solaman@alemcloud.com > /dev/null 2>&1
    fi
fi