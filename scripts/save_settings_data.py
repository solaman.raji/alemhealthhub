"""
Run this script from your console using runscript extension
python manage.py runscript save_settings_data --script-args provider/facility --traceback
"""

import json
import logging
import os
from alemhealthhub.models import Config
from alemhealthhub.settings import OPT_DIR

logger = logging.getLogger("alemhealth.scripts")


def save_settings_data(data):
    try:
        config, created = Config.objects.get_or_create(key="settings")
        config.value = json.dumps(data)
        config.save()
    except Exception as e:
        err_msg = "Unexpected error: {0}".format(e)
        logger.error(err_msg)
        raise Exception(err_msg)


def run(*args):
    logger.info("args: {0}".format(args))

    if not len(args):
        logger.error("device type is not given. please pass either provider or facility as argument")
        raise Exception("device type is not given. please pass either provider or facility as argument")

    device_type = args[0].lower()
    logger.info("device_type: {0}".format(device_type))

    if device_type != "provider" and device_type != "facility":
        logger.error("invalid device type. please pass either provider or facility as argument")
        raise Exception("invalid device type. please pass either provider or facility as argument")

    filename = device_type + "-config.json"
    logger.info("filename: {0}".format(filename))

    file_path = os.path.join(OPT_DIR, "conf", filename)
    logger.info("filepath: {0}".format(file_path))

    try:
        with open(file_path) as config_file:
            device_config = json.load(config_file)
            logger.info("device_config: {0}".format(device_config))
    except IOError as e:
        err_msg = "I/O error({0}): {1} {2}".format(e.errno, e.strerror, e.filename)
        logger.error(err_msg)
        raise Exception(err_msg)
    except Exception as e:
        err_msg = "Unexpected error: {0}".format(e)
        logger.error(err_msg)
        raise Exception(err_msg)

    save_settings_data(device_config)
