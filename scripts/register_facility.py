import json
import uuid
from django.test import RequestFactory
from alemhealthhub.api import ConfigView


# Register Facility
def register_facility(facility_access_key):
    # Default Data
    url = "/api/config"
    data = {
        "config": {
            "facility_access_key": uuid.UUID(facility_access_key).hex
        }
    }

    request_factory_object = RequestFactory()
    request = request_factory_object.put(url, content_type="application/json")
    request._body = json.dumps(data)
    ConfigView.as_view()(request)


def run():
    # Facility Access Key
    facility_access_key = "7e8d4b40-9403-4453-a41d-0b595e2cd402"
    register_facility(facility_access_key)
