from alemhealthhub.models import ScheduleTask


# Update Schedule Task
def update_schedule_task(task_type):
    schedule_task = ScheduleTask.objects.get(task_type=task_type)
    schedule_task.last_download_time = None
    schedule_task.save()


def run():
    task_type = "report_download"
    update_schedule_task(task_type)
