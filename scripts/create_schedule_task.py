from alemhealthhub.models import ScheduleTask


# Create Schedule Task for report_download if not exist
def create_schedule_task(task_type):
    ScheduleTask.objects.get_or_create(task_type=task_type)


def run():
    task_type = "report_download"
    create_schedule_task(task_type)
