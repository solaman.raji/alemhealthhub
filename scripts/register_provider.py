import json
import uuid
from django.test import RequestFactory
from alemhealthhub.api import BoxSetup


# Register Provider
def register_provider(provider_access_key):
    # Default Data
    url = "/api/boxsetup"
    data = {
        "config": {
            "first_name": "Provider1",
            "guid": uuid.UUID(provider_access_key).hex,
            "username": "admin",
            "password": "qweqwe",
            "retype_password": "qweqwe"
        }
    }

    request_factory_object = RequestFactory()
    request = request_factory_object.post(url, content_type="application/json")
    request._body = json.dumps(data)
    BoxSetup.as_view()(request)


def run():
    # Provider Access Key
    provider_access_key = "65491ecc-5a1f-4543-a82f-9df38953b899"
    register_provider(provider_access_key)
