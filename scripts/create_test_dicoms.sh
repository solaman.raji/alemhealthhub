#!/bin/bash
mkdir -p /var/cache/alemhealthhub/test_dicoms
pushd /var/cache/alemhealthhub/test_dicoms
wget --continue https://s3-ap-southeast-1.amazonaws.com/orderticket-images/test_study_{1..3}.tar.gz
for file in *.tar.gz; do tar -xvzf $file; done
popd